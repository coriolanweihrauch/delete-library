-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 10, 2014 at 11:42 AM
-- Server version: 5.5.38-0+wheezy1
-- PHP Version: 5.4.4-14+deb7u14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `avlib_blank`
--

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE IF NOT EXISTS `authors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_fuller_form` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `lifetime` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `authors_books`
--

CREATE TABLE IF NOT EXISTS `authors_books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serial_number` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `original_title` text COLLATE utf8_unicode_ci,
  `remainder_of_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parallel_title` text COLLATE utf8_unicode_ci,
  `part_x` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `part_of` int(11) DEFAULT NULL,
  `statement_of_responsability` text COLLATE utf8_unicode_ci,
  `dewey_decimal_classification` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `book_number` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `publication_publisher` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `publication_place` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `publication_year` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `edition_statement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `collation` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `ISBN` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ISSN` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `series_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `series_part_no` int(16) DEFAULT NULL,
  `series_part_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8_unicode_ci,
  `date_created` date DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_removed` date DEFAULT NULL,
  `delete_flag` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `serial_number` (`serial_number`),
  KEY `autocomplete` (`publication_publisher`,`publication_place`,`series_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `books_clients`
--

CREATE TABLE IF NOT EXISTS `books_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `date_borrow` date NOT NULL,
  `date_due` date NOT NULL,
  `date_return` date NOT NULL,
  `date_reminder` date NOT NULL,
  `reminder_level` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `books_features`
--

CREATE TABLE IF NOT EXISTS `books_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `books_keywords`
--

CREATE TABLE IF NOT EXISTS `books_keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `keyword_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `book_id` (`book_id`,`keyword_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `books_languages`
--

CREATE TABLE IF NOT EXISTS `books_languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `books_physicaldetails`
--

CREATE TABLE IF NOT EXISTS `books_physicaldetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `physicaldetail_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `books_topicalterms`
--

CREATE TABLE IF NOT EXISTS `books_topicalterms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `topicalterm_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asynctoid` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `serial_number` varchar(8) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `aurovillename` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `workplace` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `telephone` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `group` int(11) NOT NULL,
  `number_of_books` int(11) NOT NULL,
  `number_of_days` int(11) NOT NULL,
  `date_registration` date NOT NULL,
  `date_termination` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `clients_groups`
--

CREATE TABLE IF NOT EXISTS `clients_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `clients_overdues`
--

CREATE TABLE IF NOT EXISTS `clients_overdues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `overdue_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE IF NOT EXISTS `features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `notes`) VALUES
(1, 'aurovilian', ''),
(2, 'guest', ''),
(3, 'worker', '');

-- --------------------------------------------------------

--
-- Table structure for table `keywords`
--

CREATE TABLE IF NOT EXISTS `keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `notes`) VALUES
(1, 'BEN', ''),
(2, 'BUL', ''),
(3, 'DUM', ''),
(4, 'DUT', ''),
(5, 'ENG', ''),
(6, 'FRE', ''),
(7, 'GER', ''),
(8, 'GRC', ''),
(9, 'GRE', ''),
(10, 'GUJ', ''),
(11, 'HEB', ''),
(12, 'HIN', ''),
(13, 'ITA', ''),
(14, 'JPN', ''),
(15, 'KAN', ''),
(16, 'KOR', ''),
(17, 'LAT', ''),
(18, 'MAL', ''),
(19, 'MAY', ''),
(20, 'MUL', ''),
(21, 'NEW', ''),
(22, 'NON', ''),
(23, 'NOR', ''),
(24, 'PAN', ''),
(25, 'PER', ''),
(26, 'PLI', ''),
(27, 'POL', ''),
(28, 'RUS', ''),
(29, 'SPA', ''),
(30, 'SWE', ''),
(31, 'TAM', ''),
(32, 'TEL', ''),
(33, 'TIB', ''),
(34, 'TML', ''),
(35, 'WEL', '');

-- --------------------------------------------------------

--
-- Table structure for table `overdues`
--

CREATE TABLE IF NOT EXISTS `overdues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `overdues`
--

INSERT INTO `overdues` (`id`, `name`, `notes`) VALUES
(1, 'none', ''),
(2, 'long overdue', ''),
(3, 'administratively disabled', '');

-- --------------------------------------------------------

--
-- Table structure for table `physicaldetails`
--

CREATE TABLE IF NOT EXISTS `physicaldetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `physicaldetails`
--

INSERT INTO `physicaldetails` (`id`, `name`, `notes`) VALUES
(1, 'hardcover', ''),
(2, 'softcover', ''),
(3, 'paperback', ''),
(4, 'clamped', '');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'author'),
(2, 'compiler'),
(3, 'contributor'),
(4, 'editor'),
(5, 'illustrator'),
(6, 'photographer'),
(7, 'translator');

-- --------------------------------------------------------

--
-- Table structure for table `settings_blacklist`
--

CREATE TABLE IF NOT EXISTS `settings_blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `settings_blacklist`
--

INSERT INTO `settings_blacklist` (`id`, `name`) VALUES
(12, '10.10.10.150');

-- --------------------------------------------------------

--
-- Table structure for table `settings_reminders`
--

CREATE TABLE IF NOT EXISTS `settings_reminders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `settings_reminders`
--

INSERT INTO `settings_reminders` (`id`, `title`, `content`) VALUES
(1, 'first reminder', 'Hi {name} {surname}\n\nThis is the Library Database software. I''ve been told you borrowed the book "{title}" by {author_name} on {date_borrow}. I''m told you should have returned this book by {date_due}.\n\nPlease bring it back to the library any day from Mo-Fr 0800-1200 and 1300-1730 (8AM to 12AM, 1PM to 5:30PM) so my human counterparts can make it available to other readers.\n\nI''m sending you this based on information I''ve received from humans. They work long, hard hours and sometimes they make mistakes. If they did, please let us know ;)\n\nSincerely,\nThe Library Database software'),
(2, 'second reminder', 'Hi again {name} {surname} !\n\nSorry to keep bothering you, but my human counterparts are very insistent.\n\nThey say you''ve been keeping the book "{title}" by {author_name} for too long and you should already have already returned it on {date_due}.\n\nPlease hurry to bring it back to the library as others are anxiously waiting for this book.\n\nYou can drop it off any day from Mo-Fr 0800-1200 and 1300-1730 (that''s 8AM to 12AM and 1PM to 5:30PM)\n\nPlease hurry ; the air is getting so thick here my fan blades are clogging!\n\nAs usual, I''m sending you this based on information I''ve received from humans. They work long, hard hours and sometimes they make mistakes. If they did, please let us know ;)\n\nSincerely,\nThe Library Database software'),
(3, 'third reminder', 'Hi {name} {surname} !\r\n\r\nThis is the Library Database software :)\r\n\r\n3rd+ reminder (manual for "{title}" by {author_name} and due by {date_due}.\r\n\r\nYou can return it any day from Mo-Fr 0800-1200 and 1300-1730 (that''s 8AM to 12AM and 1PM to 5:30PM)\r\n\r\nI''m sending you this based on information I''ve received from humans. They work long, hard hours and sometimes they make mistakes. If they did, please let us know ;)\r\n\r\nSincerely,\r\nThe Library Database software'),
(4, 'fourth reminder', 'pl retunr book'),
(5, 'manual reminder', 'i''m gonna get you');

-- --------------------------------------------------------

--
-- Table structure for table `topicalterms`
--

CREATE TABLE IF NOT EXISTS `topicalterms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `password`) VALUES
(1, 'admin', '*23AE809DDACAF96AF0FD78ED04B6A265E05AA257'),
(2, 'staff', '*23AE809DDACAF96AF0FD78ED04B6A265E05AA257');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
