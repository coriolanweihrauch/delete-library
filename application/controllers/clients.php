<?php
class Clients extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('clients_model');
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('avlib');
		$this->fields = $this->clients_model->fields;
		$this->ldb = $this->clients_model->ldb;
	}

	public function index()
	{
		$this->view();
	}

	public function view($id = FALSE, $username = FALSE, $limit = FALSE)
	{
		## Admin/staff => edit
		if(is_admin() || is_staff())
		{
			$this->edit($id);
			return;
		}

		$validdata = $this->clients_model->validate_client($id, $username);
		$data['helpLink'] = "/library/help#myhistory";

//print("valid: ".$validdata." - ".$id." - ".$username);

		if($validdata)
		{
			$data['title'] = 'User History';
			$data['selectedMenu'] = 'Users';
			$data['elements']['fields'] = $this->fields;
	
			$data['loaded'] = $this->clients_model->get($id);
			//$data['elements']['id_next'] = $this->clients_model->get_id_next($id);
			//$data['elements']['id_prev'] = $this->clients_model->get_id_prev($id);
			// Load values from Database
			foreach($data['elements']['fields'] as $i => $e)
			{
				if(array_key_exists($e['name'], $data['loaded']))
					$data['elements']['fields'][$i]['value'] = $data['loaded'][$e['name']];
			}



			// Get borrowing history
			$data['elements']['tabletitle'] = "Borrow history for this user";
			$data['elements']['items'] = $this->clients_model->get_client_borrowhistory($id);

			$data['elements']['labels'] = array("serial_number" => "Barcode", "title" => "Title", "language" => "Language", "author_name" => "Author", "date_borrow" => "Borrowed", "date_due" => "Due by", "date_return" => "Returned");
			$data['elements']['resultsize'] = count($data['elements']['items']);
	
			$this->load->view('templates/avlibheader', $data);
			//$this->load->view('clients/asyncto', $data);
			$this->load->view('books/menubar_clients', $data);
			$this->load->view('clients/view', $data);
			$this->load->view('templates/tableview', $data);
			$this->load->view('templates/avlibfooter', $data);
		}
		else
		{
			$this->load->view('templates/avlibheader', $data);
			//$this->load->view('clients/asyncto', $data);
			$this->load->view('books/menubar_clients', $data);
			$this->load->view('clients/view_login', $data);
			$this->load->view('templates/avlibfooter', $data);
		}

	}


	public function edit($id = FALSE, $limit = FALSE)
	{
		## Admin/staff => edit
		if(!is_admin() && !is_staff())
		{
			$this->edit($id);
			return;
		}
		
		if($limit == "true")
			$limit = TRUE;
		else
			$limit = 100;
		
		$this->load->library('form_validation');
		
		$data['title'] = 'Edit user';
		$data['selectedMenu'] = 'Users';
		$data['elements']['fields'] = $this->fields;
		// Focus on client ID  field
		$data['focusfield'] = "client_aurovillename";
		$data['helpLink'] = "/library/help/staff#client_edit";
		
		if($id !== FALSE)
		{
			$data['loaded'] = $this->clients_model->get($id);
			$data['elements']['id_next'] = $this->clients_model->get_id_next($id);
			$data['elements']['id_prev'] = $this->clients_model->get_id_prev($id);
			// Load values from Database
			foreach($data['elements']['fields'] as $i => $e)
			{
				if(array_key_exists($e['name'], $data['loaded']))
					$data['elements']['fields'][$i]['value'] = $data['loaded'][$e['name']];
			}

			// Get borrowing history
			$data['elements']['tabletitle'] = "Borrow history for this user";
			$data['elements']['items'] = $this->clients_model->get_client_borrowhistory($id, $limit);

//print_r($data['elements']);
//die();
			$data['elements']['labels'] = array("serial_number" => "Barcode", "title" => "Title", "language" => "Language", "author_name" => "Author", "date_borrow" => "Borrowed", "date_due" => "Due by", "date_return" => "Returned");
			$data['elements']['resultsize'] = count($data['elements']['items']);


		}
		// New client, remove ID from list of fields
		else{
			if(array_key_exists("id", $data['elements']['fields']))
			{
				// Remove ID from list of fields
				//array_shift($data['elements']['fields']);
				//unset($data['elements']['fields']['id']);
				// Propose next serial number
//				$data['elements']['serial_number']['value'] = $this->clients_model->get_book_nextserialnumber();
//print("<hr>".$this->ldb->last_query()."<hr><br/>");
//print("Serial: ");
//print_r($sn);
			}
			else
print("Error - no ID in this set of elements");
		}

//print_r($data['elements']);

		$this->form_validation->set_rules('name', 'Name', 'required');
		//$this->form_validation->set_rules('address', 'Address', 'required');
		//$this->form_validation->set_rules('summary_note', 'Summary Note', 'required');

		
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/avlibheader', $data);
			//$this->load->view('clients/asyncto', $data);
			$this->load->view('clients/menubar', $data);
			$this->load->view('clients/edit', $data);
			$this->load->view('templates/tableview', $data);
			if($limit !== TRUE)
				$this->load->view('clients/showfullborrowhistory', $data);
			$this->load->view('templates/avlibfooter', $data);
		}
		else
		{
			// Import submitted values
			foreach($this->fields as $f)
			{
				if(!array_key_exists('ljtable', $f))
				{
					if($this->input->post($f['name']) !== FALSE)
						$data['main'][$f['name']] = $this->input->post($f['name']);
				}
				else
				{
					if($this->input->post($f['name']) !== FALSE)
					{
						$data['ljtable'][$f['name']] = $f;
						$data['ljtable'][$f['name']]['value_submitted'] = $this->input->post($f['name']);
					}
				}
			}

//print("data:");
//print_r($data);

			//echo $this->clients_model->set_book();
			$newid = $this->clients_model->set($data, $id);
			if($id === FALSE)
				$data['redirect'] = "clients/edit/".$newid;

			$this->load->view('templates/avlibheader', $data);
			$this->load->view('templates/saved', $data);
			$this->load->view('templates/avlibfooter', $data);
		}
	}



	function search()
	{
		$this->load->library('form_validation');

		$data['title'] = "Search user";
		$data['selectedMenu'] = 'Users';
		// Focus on client ID  field
		$data['focusfield'] = "client_name";
		$data['helpLink'] = "/library/help/staff#client_search";

		foreach($this->fields as $i => $f)
		{
			if($f['show_search'] == "1")
			{
//print("number: ".$this->input->post($i));
				$this->fields[$i]['value'] = $this->input->post($i);
				$data['elements']['fields'][$i] = $this->fields[$i];
				if($data['elements']['fields'][$i]['type'] == "hidden")
					$data['elements']['fields'][$i]['type'] = "text";
			}
			if($f['show_result'] == "1")
			{
				$data['elements']['labels'][$i] = $f['label'];
			}
		}
		//
		//$data['elements']['labels']['clientid'] = $data['elements']['labels']['id'];
		//unset($data['elements']['labels']['id']);
		//$data['elements']['labels']['client'] = "Borrowed by";
		//$data['elements']['labels']['date_borrow'] = "On";
//print_r();

//print_r($data['elements']);
//die();

		$data['elements']['fields']['search_deleted'] = array(
              'name'        => 'search_deleted',
							'type'				=> 'checkbox',
              'id'          => '',
              'value'       => $this->input->post('search_deleted'),
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Show terminated',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_result'		=> '0',
            );


		$data['elements']['items'] = $this->clients_model->search($data['elements']['fields']);
		$data['elements']['resultsize'] = $this->ldb->affected_rows();
//print("<pre>");
//print_r($data['elements']['items']);
//print("</pre>");

		$this->load->view('templates/avlibheader', $data);
		$this->load->view('clients/menubar', $data);
		$this->load->view('clients/search', $data);
		$this->load->view('templates/tableview', $data);
		$this->load->view('templates/avlibfooter', $data);
	}

	function delete($clientid)
	{
			$this->clients_model->delete($clientid);
  		$data['redirect'] = "clients/search";

			$this->load->view('templates/avlibheader', $data);
			$this->load->view('templates/saved', $data);
			$this->load->view('templates/avlibfooter', $data);
	}


	function checkin()
	{
		# Config
		$searchfields = array("id"=>1, "name"=>1, "surname"=>1);
		# Config

		$this->load->library('form_validation');

		foreach($this->fields as $i => $f)
		{
			if(array_key_exists($f['name'], $searchfields))
			{
//print("number: ".$this->input->post($i));
				$this->fields[$i]['value'] = $this->input->post($i);
				if($this->fields[$i]['type'] == "hidden")
					$this->fields[$i]['type'] = "text";
				$data['elements'][$i] = $this->fields[$i];
			}
			if($f['show_result'] == "1")
			{
				$data['label'][$i] = $f['label'];
			}
		}

//print_r($data['titles']);


		$data['results'] = $this->clients_model->search($data['elements']);

//print_r($this->fields);
//print_r($data['searchresults']);


		$this->load->view('templates/avlibheader', $data);
		$this->load->view('clients/search', $data);
		$this->load->view('clients/view', $data);
		$this->load->view('templates/avlibfooter', $data);
	}


	// Search client by name
	function ajaxsearchclient($term = FALSE)
	{
		$needle = $this->input->post('term').$term;
		if($needle == FALSE)
			return;

		$res = $this->clients_model->ajaxsearchclient($needle);
		if(isset($res) && is_array($res) && count($res) > 0)
			$this->output->set_content_type('application/json')->set_output(json_encode($res));
	}

	// Search for "term" in table "field"
	// Used for autocomplete
	function ajaxsearch($term = FALSE, $field = FALSE)
	{
		$needle = $this->input->post('term').$term;
		$haystack = $this->input->post('field').$field;
		if($needle == FALSE)
			return;

		$res = $this->clients_model->ajaxsearch($needle, $haystack);
		if(isset($res) && is_array($res) && count($res) > 0)
			$this->output->set_content_type('application/json')->set_output(json_encode($res));
	}

}
?>