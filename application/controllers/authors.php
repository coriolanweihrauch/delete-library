<?php
class Authors extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('authors_model');
		$this->load->helper('form');
		$this->load->helper('avlib');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->fields = $this->authors_model->fields;
		$this->ldb = $this->authors_model->ldb;
	}

	public function index()
	{
		$this->search();
	}

/*	public function view($id = FALSE)
	{
		$data['loaded'] = $this->authors_model->get($id);

		if (empty($data['loaded']))
		{
			show_404();
		}

		$data['title'] = $data['loaded']['title'];

    $this->load->view('templates/avlibheader', $data);
    $this->load->view('authors/view', $data);
    $this->load->view('templates/avlibfooter', $data);
	}*/

	public function edit($id = FALSE)
	{
		if( ! is_admin())
		{
			if(!$id)
			{
				login();
				return;
			}
			else{
				$this->view($id);
				return;
			}
		}

		$data['title'] = 'Edit author';
		$data['selectedMenu'] = 'Books';
		$data['elements']['fields'] = $this->fields;
		// Focus on author Name field
		$data['focusfield'] = "author_name_first";

		if($id !== FALSE)
		{
			$data['loaded'] = $this->authors_model->get($id);
			$data['elements']['id_next'] = $this->authors_model->get_id_next($id);
			$data['elements']['id_prev'] = $this->authors_model->get_id_prev($id);
			// Load values from Database
			foreach($data['elements']['fields'] as $i => $e)
			{
				if(array_key_exists($e['name'], $data['loaded']))
					$data['elements']['fields'][$i]['value'] = $data['loaded'][$e['name']];
			}

			// Get borrowing history
			$data['elements']['tabletitle'] = "Books related to this person";
			$data['elements']['items'] = $this->authors_model->get_author_books($id);

//itle & Remainder o.title / Language / Role / Book Nr. / Barcode

			$data['elements']['labels'] = array("serial_number" => "Barcode", "title" => "Title", "role" => "Role", "language" => "Language", "dewey_decimal_classification" => "DDC", "book_number" => "Book Nr", "client" => "Borrowed by", "date_due" => "Due by");

//print_r($data['elements']);

		}
		// New client, remove ID from list of fields
		else
		{
			if(array_key_exists("id", $data['elements']['fields']))
			{
				// Remove ID from list of fields
				array_shift($data['elements']['fields']);
				// Propose next serial number
//				$data['elements']['serial_number']['value'] = $this->clients_model->get_book_nextserialnumber();
//print("<hr>".$this->ldb->last_query()."<hr><br/>");
//print("Serial: ");
//print_r($sn);
			}
			else
print("Error - no ID in this set of elements");
		}

//print('hello');
//print_r($data['elements']);

		$this->form_validation->set_rules('name', 'Name', 'required');
		//$this->form_validation->set_rules('name_last', 'Last Name', 'required');

		if ($this->form_validation->run() === FALSE)
		{
//print('errors');
			$this->load->view('templates/avlibheader', $data);
			$this->load->view('books/menubar', $data);
			$this->load->view('authors/edit', $data);
			$this->load->view('templates/tableview', $data);
			$this->load->view('templates/avlibfooter', $data);
		}
		else
		{
//print('saving');
			// Import submitted values
			foreach($this->fields as $f)
			{
				if(!array_key_exists('ljtable', $f))
				{
					if($this->input->post($f['name']) !== NULL)
						$data['main'][$f['name']] = $this->input->post($f['name']);
				}
				else
				{
					if($this->input->post($f['name']) !== NULL)
					{
						$data['ljtable'][$f['name']] = $f;
						$data['ljtable'][$f['name']]['value_submitted'] = $this->input->post($f['name']);
					}
				}
			}


//print("<pre>");
//print_r($this->input->post());
//print_r($data);
//print("</pre>");
//die();
			//echo $this->clients_model->set_book();
			$newid = $this->authors_model->set($data, $id);
			if($id === FALSE)
				$data['redirect'] = "authors/edit/".$newid;

			$this->load->view('templates/avlibheader', $data);
			$this->load->view('templates/saved', $data);
			$this->load->view('templates/avlibfooter', $data);
		}
	}

	public function view($id)
	{
		if(is_admin())
		{
			$this->edit($id);
			return;
		}
		//elseif(!is_staff())
		//{
		//		login();
		//		return;
		//}

		$data['title'] = 'Author';
		$data['selectedMenu'] = 'Books';
		$data['elements']['fields'] = $this->fields;
		// Focus on author Name field
		$data['focusfield'] = "author_name_first";


		$data['loaded'] = $this->authors_model->get($id);
		$data['elements']['id_next'] = $this->authors_model->get_id_next($id);
		$data['elements']['id_prev'] = $this->authors_model->get_id_prev($id);
		// Load values from Database
		foreach($data['elements']['fields'] as $i => $e)
		{
			if(array_key_exists($e['name'], $data['loaded']))
				$data['elements']['fields'][$i]['value'] = $data['loaded'][$e['name']];
		}

		// Get borrowing history
		$data['elements']['tabletitle'] = "Books related to this person";
		$data['elements']['items'] = $this->authors_model->get_author_books($id);

//itle & Remainder o.title / Language / Role / Book Nr. / Barcode

		if(is_staff())
			$data['elements']['labels'] = array("serial_number" => "Barcode", "title" => "Title", "role" => "Role", "language" => "Language", "dewey_decimal_classification" => "DDC", "book_number" => "Book Nr", "client" => "Borrowed by", "date_due" => "Due by");
		else
			$data['elements']['labels'] = array("title" => "Title", "role" => "Role", "language" => "Language", "dewey_decimal_classification" => "DDC", "book_number" => "Book Nr", "book_status" => "Status", "date_due" => "Due by");

		$this->load->view('templates/avlibheader', $data);
		if( ! is_client())
			$this->load->view('books/menubar', $data);
		else
			$this->load->view('books/menubar_clients', $data);
		$this->load->view('authors/view', $data);
		$this->load->view('templates/tableview', $data);
		$this->load->view('templates/avlibfooter', $data);
	}

	function search()
	{
		$data['title'] = "Search author";
		// Focus on author Name field
		$data['focusfield'] = "author_name_first";

		$data['elements']['labels']['author_name'] = "Author";
		foreach($this->fields as $i => $f)
		{
			if($f['show_search'] == "1")
			{
//print("number: ".$this->input->post($i));
				$this->fields[$i]['value'] = $this->input->post($i);
				$data['elements']['fields'][$i] = $this->fields[$i];
				if($data['elements']['fields'][$i]['type'] == "hidden")
					$data['elements']['fields'][$i]['type'] = "text";
			}
			if($f['show_view'] == "1")
			{
				$data['elements']['labels'][$i] = $f['label'];
			}
		}
		unset($data['elements']['labels']['id']);
		//
		//$data['elements']['labels']['clientid'] = $data['elements']['labels']['id'];
		//unset($data['elements']['labels']['id']);
		//$data['elements']['labels']['client'] = "Borrowed by";
		//$data['elements']['labels']['date_borrow'] = "On";

//print_r();

//print_r($data['elements']);
//die();


		$data['elements']['items'] = $this->authors_model->search($data['elements']['fields']);
//print("<pre>");
//print_r($data['elements']['items']);
//print("</pre>");

		$this->load->view('templates/avlibheader', $data);
		$this->load->view('books/menubar', $data);
		$this->load->view('authors/search', $data);
		$this->load->view('templates/tableview', $data);
		$this->load->view('templates/avlibfooter', $data);
	}


	function ajaxsearchauthor($term = FALSE)
	{
		$needle = $this->input->post('term').urldecode($term);
		if($needle == FALSE)
			return;

		$res = $this->authors_model->ajaxsearchauthor($needle);
		if(isset($res) && is_array($res) && count($res) > 0)
			$this->output->set_content_type('application/json')->set_output(json_encode($res));
	}

	function ajaxsearch($term = FALSE, $field = FALSE)
	{
		$needle = $this->input->post('term').urldecode($term);
		$haystack = $this->input->post('field').urldecode($field);
		if($needle == FALSE)
			return;

			$res = $this->authors_model->ajaxsearch($needle, $haystack);
		if(isset($res) && is_array($res) && count($res) > 0)
			$this->output->set_content_type('application/json')->set_output(json_encode($res));
	}

	// Add author to book
	function ajaxaddrole($bookid = FALSE, $personid = FALSE, $roleid = FALSE)
	{
//print_r($this->input->post());
		$bookid = $this->input->post('bookid').urldecode($bookid);
		$personid = intval($this->input->post('personid').urldecode($personid));
		$roleid = $this->input->post('roleid').urldecode($roleid);
//print("book ".$bookid."      person ".$personid."      role ".$roleid));
		if($personid == FALSE || $personid == 0  || $roleid == FALSE || $bookid == FALSE)
			$this->output->set_content_type('application/json')->set_output(json_encode(array("result"=>"Invalid entry      book ".$bookid."      person ".$personid."      role ".$roleid)));
		elseif($this->authors_model->ajaxaddrole($bookid, $personid, $roleid))
			$this->output->set_content_type('application/json')->set_output(json_encode(array("result"=>"Success")));
		else
			$this->output->set_content_type('application/json')->set_output(json_encode(array("result"=>"Error while saving")));
	}


	// Delete author from book
	function ajaxremoverole($bookid = FALSE, $personid = FALSE, $roleid = FALSE)
	{
//print_r($this->input->post());
		$bookid = $this->input->post('bookid').urldecode($bookid);
		$personid = intval($this->input->post('personid').urldecode($personid));
		$roleid = $this->input->post('roleid').urldecode($roleid);
//print("book ".$bookid."      person ".$personid."      role ".$roleid));
		if($personid == FALSE || $personid == 0 || $roleid == FALSE || $bookid == FALSE)
			$this->output->set_content_type('application/json')->set_output(json_encode(array("result"=>"Invalid entry      book ".$bookid."      person ".$personid."      role ".$roleid)));
		elseif($this->authors_model->ajaxremoverole($bookid, $personid, $roleid))
			$this->output->set_content_type('application/json')->set_output(json_encode(array("result"=>"Success")));
		else
			$this->output->set_content_type('application/json')->set_output(json_encode(array("result"=>"Error while saving")));
	}

	function ajaxlistroles($bookid = FALSE)
	{
		$bookid = $this->input->post('bookid').urldecode($bookid);
		if($bookid == FALSE)
			$this->output->set_content_type('application/json')->set_output(json_encode(array("result"=>"Invalid entry      book ".$bookid)));
		elseif($res = $this->authors_model->ajaxlistroles($bookid))
			$this->output->set_content_type('application/json')->set_output(json_encode($res));
		else
			$this->output->set_content_type('application/json')->set_output(json_encode(array("result"=>"Error while loading books")));
	}

}
?>