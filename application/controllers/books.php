<?php
class Books extends CI_Controller {

	private $ldb;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('books_model');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->helper('avlib');
		$this->fields = $this->books_model->fields;
		$this->ldb = $this->books_model->ldb;
	}

	public function index()
	{
		if(is_staff())
			$this->checkin();
		elseif(is_admin())
			$this->search();
		else
			$this->search();
	}

	//public function test()
	//{
	//	$this->load->view("books/test");
	//}

	private function login($redirect = null)
	{
		$data['title'] = "Please authenticate";
		$data['selectedMenu'] = 'Books';
		if($redirect)
			$data['redirect'] = $redirect;
		// Focus on password field
		$data['focusfield'] = "password";
		$this->load->view('templates/avlibheader', $data);
		//$this->load->view('books/menubar', $data);
		$this->load->view('templates/loginform', $data);
		$this->load->view('templates/avlibfooter', $data);
	}

	public function switchtoedit()
	{
		if( ! is_admin())
		{
			$this->login($_SERVER["HTTP_REFERER"]);
			return;
		}
		else
			header('Location: '.$_SERVER["HTTP_REFERER"]);
	}

	public function view($id)
	{
		## Admin => edit
		if(is_admin())
		{
			$this->edit($id);
			return;
		}

		$this->load->model('Authors_model');

		$data['title'] = 'View book';
		$data['selectedMenu'] = 'Books';
		$data['elements']['fields'] = $this->fields;
		$data['loaded'] = $this->books_model->get($id);

		$cd = $this->books_model->get_checkin_details($data['loaded']['serial_number']);
		if(is_array($cd) && array_key_exists("clientid", $cd))
		{
		$data['elements']['fields']['client'] = array(
              'name'        => 'client',
							'type'				=> 'text',
							'ljtable'			=> '',
              'id'          => '',
              'value'       => $cd['clientid'],
              'maxlength'   => '',
              'size'        => '15',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Author',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );
		}

		$data['elements']['fields']['author'] = array(
              'name'        => 'author',
							'type'				=> 'lookup',
							'ljtable'			=> 'authors_books',
              'id'          => 'lookup_authors',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '15',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Author',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );


//print_r($data['elements']);
		if($data['loaded'])
		{
			// Load values from Database
			foreach($data['elements']['fields'] as $i => $e)
			{
				if(array_key_exists($e['name'], $data['loaded']))
					$data['elements']['fields'][$i]['value'] = $data['loaded'][$e['name']];

				if(array_key_exists("ljtable", $data['elements']['fields'][$i]) && $data['elements']['fields'][$i]['ljtable'] != "")
				{
					$arrVal = array();
					// get LJ tables
					if($data['elements']['fields'][$i]['value'] != "" && is_array($data['elements']['fields'][$i]['value']))
					{
						foreach($data['elements']['fields'][$i]['value'] as $j => $v)
						{
							if(array_key_exists("options", $data['elements']['fields'][$i]))
								if(array_key_exists($v, $data['elements']['fields'][$i]['options']))
									$arrVal[] = $data['elements']['fields'][$i]['options'][$v];
						}
						$data['elements']['fields'][$i]['value'] = implode(", ", $arrVal);
						//$data['elements']['fields'][$i]['value'] = $arrVal;
					}
					if($data['elements']['fields'][$i]['type'] == "lookup")
					{
						$r = $this->books_model->lookup($data['elements']['fields'][$i]['name'], $data['elements']['fields'][$i]['ljtable'], $id);
						//print('hello');
						//print_r($r);
						foreach($r as $j => $v)
							$arrVal[] = array("id" => $v['id'], "value" => $v['name']);
						//$data['elements']['fields'][$i]['value'] = implode(", ", $arrVal);
						$data['elements']['fields'][$i]['value'] = $arrVal;

						// Get author roles
						if($data['elements']['fields'][$i]['name'] == 'author')
						{
							foreach($data['elements']['fields'][$i]['value'] as $a => $author)
								$data['elements']['fields'][$i]['value'][$a]['role'] = $this->books_model->lookup_author_role($data['elements']['fields'][$i]['value'][$a]['id'], $id);
						}
						//print_r($data['elements']['fields'][$i]['value']);
					}
				}
			}

//print_r($data['elements']['fields']['author']);

			// Add leading zeros to serial_number
			for($i = strlen($data['elements']['fields']['serial_number']['value']); $i < 6 ; $i++)
				$data['elements']['fields']['serial_number']['value'] = "0".$data['elements']['fields']['serial_number']['value'];

			// Get borrowing history
			if(is_staff())
			{
				$data['elements']['tabletitle'] = "Borrow history for this book";
				$data['elements']['items'] = $this->books_model->get_book_borrowhistory($id);
				$data['elements']['labels'] = array("date_borrow" => "Borrowed", "date_return" => "Returned", "client" => "Client");
			}
/*
			// Get authors
			$data['authors']['quickaddform']['fields'] = $this->Authors_model->get_quickadd_fields($id);
			$data['authors']['quickaddform']['fields'] = $this->Authors_model->load_ljtables($data['authors']['quickaddform']['fields']);
*/
		}
		else
		{
			$data['messages'][] = array("error" => "This Book was not found");
		}

		$this->load->view('templates/avlibheader', $data);

		if( ! is_client())
			$this->load->view('books/menubar', $data);
		else
			$this->load->view('books/menubar_clients', $data);

		$this->load->view('books/view', $data);
		if(is_staff)
			$this->load->view('templates/tableview', $data);
		//$this->load->view('authors/quickadd', $data);
		$this->load->view('templates/avlibfooter', $data);
	}


	public function edit($id = FALSE)
	{
		if( ! is_admin())
		{
			if(!$id)
			{
				$this->login();
				return;
			}
			else{
				$this->view($id);
				return;
			}
		}

		$this->load->model('Authors_model');

		$data['title'] = 'Edit book';
		$data['selectedMenu'] = 'Books';
		$data['elements']['fields'] = $this->fields;
		// Focus on password field
		$data['focusfield'] = "serial_number";

/*
print("id: ");
var_dump($id);
print("sn: ".$this->books_model->get_book_nextid());
print_r($data);
die();
*/

		// Creating new book
		if($id == FALSE)
		{

			if(array_key_exists("id", $data['elements']['fields']))
			{
				$data['loaded']['serial_number'] = $this->books_model->get_book_nextserialnumber();
// Seems to fix the intial book creation bug
//				$id = $this->books_model->get_book_nextid();
				$data['loaded']['id'] = $id;
			}
		}
		else
		{
			$data['loaded'] = $this->books_model->get($id);
			$data['elements']['id_next'] = $this->books_model->get_id_next($id);
			$data['elements']['id_prev'] = $this->books_model->get_id_prev($id);
		}


//print("<pre>");
//print_r($data['elements']['fields']);
//print("</pre>");

//print("<pre>");
//print_r($data['loaded']);
//print("</pre>");

		if($data['loaded'])
		{
			// Load values from Database
			foreach($data['elements']['fields'] as $i => $e)
			{
				if(array_key_exists($e['name'], $data['loaded']))
					$data['elements']['fields'][$i]['value'] = $data['loaded'][$e['name']];
			}

			// Add leading zeros to serial_number
			for($i = strlen($data['elements']['fields']['serial_number']['value']); $i < 6 ; $i++)
				$data['elements']['fields']['serial_number']['value'] = "0".$data['elements']['fields']['serial_number']['value'];

			// Get borrowing history
			$data['elements']['tabletitle'] = "Borrow history for this book";
			$data['elements']['items'] = $this->books_model->get_book_borrowhistory($id);
			$data['elements']['labels'] = array("date_borrow" => "Borrowed", "date_return" => "Returned", "client" => "Client");

			// Get authors
			//$data['authors']['quickaddform']['fields'] = $this->Authors_model->get_quickadd_fields($id);
			//$data['authors']['quickaddform']['fields'] = $this->Authors_model->load_ljtables($data['authors']['quickaddform']['fields']);

			$data['elements']['fields']['author']['quickaddform']['bookid'] = $id;
			$data['elements']['fields']['author']['quickaddform']['fields'] = $this->Authors_model->get_quickadd_fields($id);
			$data['elements']['fields']['author']['quickaddform']['fields'] = $this->Authors_model->load_ljtables($data['elements']['fields']['author']['quickaddform']['fields']);
		}
		else
		{
			$data['messages'][] = array("error" => "This Book was not found");
		}

		$data['elements']['fields']['serial_number_display'] = array(
              'name'        => 'serial_number_display',
							'type'				=> '',
              'id'          => 'serial_number_display',
              'value'       => $data['elements']['fields']['serial_number']['value'],
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'readonly'	  => 'readonly',
							'js'					=> '',
							'label'				=> 'Barcode',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_view'		=> '1',
							'script'			=> "$(\"#serial_number\").change(function(){
																	$(\"#serial_number_display\").val($(\"#serial_number\").val());
																}
															  );"
            );


//print("<pre>");
//print_r($data['elements']['fields']);
//print("</pre>");


		$this->form_validation->set_error_delimiters('<br/><div class="message">', '</div>');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('serial_number', 'Serial Number', 'required');
		$this->form_validation->set_rules('serial_number', 'Serial Number', 'callback_validate_serial_number');
		//$this->form_validation->set_rules('summary_note', 'Summary Note', 'required');

		// Error validating
		if ($this->form_validation->run() === FALSE)
		{
			if($this->input->post())
			{
				foreach($this->input->post() as $i => $v)
				{
					if(array_key_exists($i, $data['elements']['fields']))
					{
	//print($i.":".$v."<br/>");
						$data['elements']['fields'][$i]['value'] = $v;
					}
				}
			}
//				print($i.":".$v."<br/>\n");
//print_r($data['elements']['fields']);
			//$data['elements']['fields'][$i]['value'] = $v;

//print_r($data['elements']['fields']);

//print("<pre>");
//print_r($data['elements']);
//print("</pre>");

			$this->load->view('templates/avlibheader', $data);
			$this->load->view('books/menubar', $data);
			$this->load->view('books/edit', $data);
			$this->load->view('templates/tableview', $data);
			//$this->load->view('authors/quickadd', $data);
			$this->load->view('templates/avlibfooter', $data);
		}
		// Validation ok => save
		else
		{
/*
			print($this->books_model->set($id, $this->input->post()));
			print_r($this->input->post());
			die();
*/
			$newid = $this->books_model->set($id, $this->input->post());
			$data['redirect'] = "books/edit/".$newid;

			$this->load->view('templates/avlibheader', $data);
			$this->load->view('books/menubar', $data);
			$this->load->view('templates/saved', $data);
			$this->load->view('templates/avlibfooter', $data);
		}
	}

	public function validate_serial_number($sn)
	{
		// Verify if serial_no exists in DB
		$b = $this->books_model->get_by_sn($sn);
		// AND has been used by ANOTHER book (i.e. different ID)
		if(count($b) && $this->input->post('id')!= $b['id'])
		{
			$this->form_validation->set_message('validate_serial_number', 'This %s has already been used');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	function delete($bookid)
	{
		if( ! is_admin())
		{
			$this->login();
			return;
		}
		$this->books_model->delete($bookid);
		$data['redirect'] = "books/search";

		$this->load->view('templates/avlibheader', $data);
		$this->load->view('templates/saved', $data);
		$this->load->view('templates/avlibfooter', $data);
	}

	public function duplicate($id)
	{
		if( ! is_admin())
		{
			$this->login();
			return;
		}

		$data['elements']['fields'] = $this->fields;

		$this->load->model('Authors_model');

		$data['loaded'] = $this->books_model->get($id);

// Duplicate AUthor !!!
// Duplicate AUthor !!!
// Duplicate AUthor !!!


		// Load values from Database
		foreach($data['elements']['fields'] as $i => $e)
		{
			if(array_key_exists($e['name'], $data['loaded']))
				$data['elements']['fields'][$i]['value'] = $data['loaded'][$e['name']];
		}

		// Add leading zeros to serial_number
		for($i = strlen($data['elements']['fields']['serial_number']['value']); $i < 6 ; $i++)
			$data['elements']['fields']['serial_number']['value'] = "0".$data['elements']['fields']['serial_number']['value'];

		// Get authors
		$data['authors']['quickaddform']['fields'] = $this->Authors_model->get_quickadd_fields($id);
		$data['authors']['quickaddform']['fields'] = $this->Authors_model->load_ljtables($data['authors']['quickaddform']['fields']);

		// Update s/n
		$data['loaded']['serial_number'] = $this->books_model->get_book_nextserialnumber();
		// Update id
		$id = $this->books_model->get_book_nextid();
		$data['loaded']['id'] = $id;

		// Save
		$newid = $this->books_model->set($id, $data['loaded']);
		$data['redirect'] = "books/edit/".$newid;

		$this->load->view('templates/avlibheader', $data);
		$this->load->view('books/menubar', $data);
		$this->load->view('templates/saved', $data);
		$this->load->view('templates/avlibfooter', $data);
	}


	function checkout()
	{
		if(is_client())
		{
			$this->login();
			return;
		}


		$data['elements'] = array();

		$this->form_validation->set_rules('clientid', 'User ID', 'numeric');
		$this->form_validation->set_error_delimiters('<div class="message">', '</div>');

		$data['title'] = "Check out";
		$data['selectedMenu'] = 'Check-out';
		$data['helpLink'] = "/library/help/staff#checkout";

		// Focus on client ID field
		$data['focusfield'] = "clientidfield";

		// Get client information //
		// Error parsing form //
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/avlibheader', $data);
			$this->load->view('clients/checkout', $data);
			$this->load->view('templates/avlibfooter', $data);
		}
		// Client ID submitted //
		elseif($this->input->post('clientid'))
		{
			// Load client data //
			$data['elements']['clientid'] = $this->input->post('clientid');
			$this->load->model('Clients_model');
			$data['elements']['client'] = $this->Clients_model->get($data['elements']['clientid']);

			// Valid client id //
			if(isset($data['elements']['client']) && is_array($data['elements']['client']) && count($data['elements']['client']) > 0)
			{
				// Client registration terminated
				if($data['elements']['client']['date_termination'] > 0)
				{
					$data['messages'][] = array("notice" => "User registration was terminated on ".$data['elements']['client']['date_termination']);
					$this->load->view('templates/avlibheader', $data);
					//$this->load->view('templates/messages', $data);
					$this->load->view('clients/checkout', $data);
					$this->load->view('clients/clientsummary', $data);
					$this->load->view('templates/avlibfooter', $data);
					return;
				}

				// Client administratively disabled
				if($data['elements']['client']['overdue'][0] == 3)
				{
					$data['messages'][] = array("overdue" => "This user has been administratively disabled and cannot borrow books at the moment");
					//$this->load->view('templates/avlibheader', $data);
					////$this->load->view('templates/messages', $data);
					//$this->load->view('clients/checkout', $data);
					//$this->load->view('clients/clientsummary', $data);
					//$this->load->view('templates/avlibfooter', $data);
					//return;
				}

				// Borrow book, if any //
				if($data['elements']['bookid'] = $this->input->post('bookid'))
					$data['messages'][] = $this->books_model->checkout($data['elements']['bookid'], $data['elements']['clientid']);

				// Load currently borrowed books
				$data['elements']['labels'] = $this->books_model->get_labels_view();
				$data['elements']['labels']['author_name'] = "Author";
				$data['elements']['labels']['date_borrow'] = "Borrowed on";
				$data['elements']['labels']['date_due'] = "Due by";
				$data['elements']['labels']['date_reminder'] = "Last reminder";
				$data['elements']['items'] = $this->Clients_model->get_books($data['elements']['clientid']);
				$data['elements']['tabletitle'] = "Books currently with ".$data['elements']['client']['name']." ".$data['elements']['client']['surname'];
				unset($data['elements']['labels']['id']);
				unset($data['elements']['labels']['part_x']);
				// Overdue warning messages

				if($data['elements']['client']['overdue'][0] == 2)
					$data['messages'][] = array("overdue" => "This user has long overdue books. Please go through the list with him and clear each book's status before releasing any new books");
				foreach($data['elements']['items'] as $i => $item)
				{
				//print_r($item);

				if(array_key_exists("date_due", $item) && $item['date_due'] != "0000-00-00")
					if($this->books_model->overdue_category($item['date_due']) >= 2)
						$data['messages'][] = array("message" => "Long overdue ! The user has has the book \"".$item['title']."\" by ".$item['author_name']." which was due by ".$item['date_due']);
					elseif($this->books_model->overdue_category($item['date_due']) == 1)
						$data['messages'][] = array("notice" => "Overdue ! The user has has the book \"".$item['title']."\" by ".$item['author_name']." which was due by ".$item['date_due']);
					elseif($this->books_model->overdue_category($item['date_due']) == 0)
					{
						// not overdue => no error message
					}
					else
					{
						$data['messages'][] = array("success" => "There should be an error message here ... hm ?");
					}
				}

				// Mark overdue dates
				foreach($data['elements']['items'] as $i => $item)
					$data['elements']['items'][$i]['date_due'] = $this->mark_overdue($item['date_due']);

				if($data['elements']['client']['overdue'][0] != 3 && $data['elements']['client']['overdue'][0] != 2)
				{
					// Check quota of user
					$data['elements']['client']['number_of_books_remaining'] = $data['elements']['client']['number_of_books']-count($data['elements']['items']);
					$q = $data['elements']['client']['number_of_books_remaining'];
					if($q < 0)
						$data['messages'][] = array("message" => "The user has exceeded his borrowing quota by ".(-$q)." ".($q < -1 ? "books" : "book"));
					elseif($q > 0)
						$data['messages'][] = array("success" => "The user can borrow ".$q." more ".($q < -1 ? "books" : "book"));
					else
						$data['messages'][] = array("notice" => "The user should not borrow any more books");
				}

				// Place messages BELOW form - add template view //
				$data['messagescustomlocation'] = 1;

				// Focus on book ID  (to be borrowed) field
				$data['focusfield'] = "bookid";

//print_r($data['elements']['labels']);
//print("<hr>\n");
//print_r($data['elements']['items']);
				$this->load->view('templates/avlibheader', $data);
				$this->load->view('clients/checkout', $data);
				if($data['elements']['client']['overdue'][0] != 3)
					$this->load->view('books/checkout', $data);
				$this->load->view('templates/messages', $data);
				$this->load->view('clients/clientsummary', $data);
				$this->load->view('templates/tableview', $data);
				$this->load->view('templates/avlibfooter', $data);
			}
			// Invalid client id //
			else
			{
				$data['messages'][] = array("message" => "This user ID does not exist");
				$this->load->view('templates/avlibheader', $data);
				$this->load->view('clients/checkout', $data);
				$this->load->view('templates/messages', $data);
				$this->load->view('templates/avlibfooter', $data);
			}
		}
		// NO Client ID submitted //
		else
		{
			$this->load->view('templates/avlibheader', $data);
			$this->load->view('clients/checkout', $data);
			$this->load->view('templates/messages', $data);
			$this->load->view('templates/avlibfooter', $data);
		}
	}


	function checkin($sn = FALSE)
	{
		if(is_client())
		{
			$this->login();
			return;
		}

		$this->form_validation->set_error_delimiters('<div class="message">', '</div>');
		$this->form_validation->set_rules('serial_number', 'Serial Number', 'numeric');

		$data['title'] = "Check in";
		$data['selectedMenu'] = 'Check-in';
		$data['elements']['id'] = $this->fields['id'];
		$data['elements']['serial_number'] = $this->fields['serial_number'];
		$data['helpLink'] = "/library/help/staff#checkin";

		// Focus on book ID  (to be returned) field
		$data['focusfield'] = "serial_number";

		if($sn === FALSE)
			$sn = $this->input->post('serial_number');
		$data['elements']['serial_number']['value'] = $sn;

//print("details:<br/><pre>\n");
//print_r($data['elements']);
//print("</pre><br/>\n");
		if ($this->form_validation->run() === FALSE)
		{
//			$data['messages'][] = "Please verify your data and try again";
		}
		else
		{
			$data['details'] = $this->books_model->get_checkin_details($sn);
			// Book is currently borrowed
			if(array_key_exists('id', $data['details']))
			{
//print("details:<br/>\n");
//print_r($data['details']);
//print("<br/>\n");
				// Return book
				$r = $this->books_model->checkin($data['details']['id']);
				if(isset($data['details']))
				{
					// Display message: returned successfully
					$data['messages'][] = array("success" => "Returned #".$data['details']['serial_number']." - <a href=\"/library/books/edit/".$data['details']['bookid']."\">".$data['details']['title']."</a> borrowed by <a href=\"/library/clients/edit/".$data['details']['clientid']."\">".$data['details']['name']." ".$data['details']['surname']."</a> (".$data['details']['address'].") on ".$data['details']['date_borrow']);
				}
			}
			// Book is currently not borrowed
			else
			{
				// Load book details
				$data['details'] = $this->books_model->get_by_sn($sn);
				if(array_key_exists('title', $data['details']))
					$data['messages'][] = array("notice" => "#".$sn." - <a href=\"/library/books/edit/".$data['details']['id']."\">".$data['details']['title']."</a> is currently not borrowed by anybody");
				else
					// Display message: book currently in
					$data['messages'][] = array("message" => "#".$sn." does not exist");
			}

		// Remove s/n from field
		$data['elements']['serial_number']['value'] = '';
//print_r($data['details']);

		}

		// Place messages BELOW form - add template view //
		$data['messagescustomlocation'] = 1;

		$this->load->view('templates/avlibheader', $data);
		$this->load->view('books/checkin', $data);
		$this->load->view('templates/messages', $data);
		$this->load->view('templates/avlibfooter', $data);
	}

	function search()
	{
		$data['title'] = "Search book";
		$data['selectedMenu'] = 'Books';
		if( ! is_client())
			$data['helpLink'] = "/library/help/staff#search";
		else
			$data['helpLink'] = "/library/help#search";

		// Focus on book ID  (to be returned) field
		$data['focusfield'] = "serial_number";

		foreach($this->fields as $i => $f)
		{
			if($f['show_search'] == "1")
			{
				$this->fields[$i]['value'] = $this->input->post($i);
				$data['elements']['fields'][$i] = $this->fields[$i];
				if($data['elements']['fields'][$i]['type'] == "hidden")
					$data['elements']['fields'][$i]['type'] = "text";
			}
			if($f['show_result'] == "1")
			{
				$data['elements']['labels'][$i] = $f['label'];
			}
		}

		//$data['elements']['fields']['author'] =
		//$data['elements']['fields']['general'] =

		// Add additional search fields
		$data['elements']['fields']['author'] = array(
              'name'        => 'author',
							'type'				=> '',
              'id'          => 'search_author',
              'value'       => $this->input->post('author'),
              'maxlength'   => '',
              'size'        => '40',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Author',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_view'		=> '0',
            );
//		if( ! is_client())
//		{
//			$data['elements']['fields']['search_deleted'] = array(
//              'name'        => 'search_deleted',
//							'type'				=> 'checkbox',
//              'id'          => 'search_checkbox',
//              'value'       => $this->input->post('search_deleted'),
//              'maxlength'   => '',
//              'size'        => '',
//              'style'       => '',
//							'js'					=> '',
//							'label'				=> 'Show deleted',
//							'show_edit'		=> '0',
//							'show_search' => '1',
//							'show_view'		=> '0',
//            );
//		}

		if( ! is_client())
		{
			$data['elements']['labels']['author_name'] = "Author(s)";
			$data['elements']['labels']['client'] = "Borrowed by";
			//$data['elements']['labels']['date_borrow'] = "On";
			$data['elements']['labels']['date_due'] = "Due by";
			unset($data['elements']['labels']['id']);
			unset($data['elements']['labels']['part_x']);
			if($data['elements']['fields']['serial_number']['value'])
			{
				$data['elements']['fields']['serial_number']['value'] = intval($data['elements']['fields']['serial_number']['value']);
				// Add leading zeros to serial_number
				for($i = strlen($data['elements']['fields']['serial_number']['value']); $i < 6 ; $i++)
					$data['elements']['fields']['serial_number']['value'] = "0".$data['elements']['fields']['serial_number']['value'];
			}
		}
		else{
			$data['elements']['labels']['author_name'] = "Author(s)";
			$data['elements']['labels']['book_status'] = "Status";
			//$data['elements']['labels']['client'] = "Borrowed by";
			//$data['elements']['labels']['date_borrow'] = "On";
			$data['elements']['labels']['date_due'] = "Due by";

			unset($data['elements']['fields']['serial_number']);
			unset($data['elements']['labels']['id']);
			unset($data['elements']['labels']['part_x']);
			if(is_client())
				unset($data['elements']['labels']['serial_number']);
		}

		// Not needed ; integrated into tableview
		//$data['elements']['labels'] = sort_labels($data['elements']['labels']);



		$data['elements']['items'] = $this->books_model->search($data['elements']['fields']);
		$data['elements']['resultsize'] = $this->ldb->affected_rows();
//print("<pre>");
//print_r($data['elements']['items']);
//print("</pre>");

		foreach($data['elements']['items'] as $i => $v)
		{
			$data['elements']['items'][$i]['book_status'] = empty($v['client']) ? "IN" : "OUT";
		}


		$this->load->view('templates/avlibheader', $data);
		if( ! is_client())
			$this->load->view('books/menubar', $data);
		else
			$this->load->view('books/menubar_clients', $data);
		$this->load->view('books/search', $data);
		$this->load->view('templates/tableview', $data);
		$this->load->view('templates/avlibfooter', $data);
	}

	function search_advanced()
	{
		if( ! is_client())
			$data['helpLink'] = "/library/help/staff#search_advanced";
		else
			$data['helpLink'] = "/library/help#search_advanced";
		//$data['helpLink'] = "/library/help#search_advanced";

		$arrFields = array();
		foreach($this->fields as $f)
		{
			if($f['name'] != "id")
				$arrFields[$f['name']] = $f['label'];
		}

		//print_r($arrFields);

//		$data['elements']['fields']['bf1'] = array(
//              'name'        => 'bf1',
//							'type'				=> 'select',
//							'ljtable'		  => 'virtualdub',
//              'id'          => 'bf1',
//              'value'       => $this->input->post('bf1'),
//              'maxlength'   => '',
//              'size'        => '',
//              'style'       => '',
//							'js'					=> '',
//							'options'			=> array("" => "---", "not" => "NOT"),
//							'label'				=> 'Operator',
//							'show_edit'		=> '0',
//							'show_search' => '1',
//							'show_view'		=> '0',
//          );

		$arrCriteria = array("%like%" => "Contains", "like" => "Equals (soft)", "equals" => "Equals (strict)", "like%" => "Begins with", "%like" => "Ends with");

		$data['elements']['fields']['f1'] = array(
              'name'        => 'f1',
							'type'				=> 'select',
							'ljtable'		  => 'virtualdub',
              'id'          => 'f1',
              'value'       => $this->input->post('f1') ? $this->input->post('f1') : "title",
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'options'			=> $arrFields,
							'label'				=> 'Field',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_view'		=> '0',
          );

		$data['elements']['fields']['fv1'] = array(
              'name'        => 'fv1',
							'type'				=> '',
              'id'          => 'fv1',
              'value'       => strval($this->input->post('fv1')),
              'maxlength'   => '',
              'size'        => '40',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Value',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_view'		=> '0',
          );

		$data['elements']['fields']['fc1'] = array(
              'name'        => 'fc1',
							'type'				=> 'select',
							'ljtable'			=> 'virtualdub',
              'id'          => 'fc1',
              'value'       => strval($this->input->post('fc1')),
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'options'			=> $arrCriteria,
							'label'				=> 'Criteria',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_view'		=> '0',
          );

		$data['elements']['fields']['bf1'] = array(
              'name'        => 'bf2',
							'type'				=> 'select',
							'ljtable'		  => 'virtualdub',
              'id'          => 'bf2',
              'value'       => $this->input->post('bf2'),
              'maxlength'   => '',
              'size'        => '40',
              'style'       => '',
							'js'					=> '',
							//'options'			=> array("and" => "And", "or" => "Or", "and not" => "And NOT", "or not" => "Or NOT"),
							'options'			=> array("and" => "And", "or" => "Or"),
							'label'				=> 'Operator',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_view'		=> '0',
          );

		$data['elements']['fields']['f2'] = array(
              'name'        => 'f2',
							'type'				=> 'select',
							'ljtable'		  => 'virtualdub',
              'id'          => 'f2',
              'value'       => $this->input->post('f2') ? $this->input->post('f2') : "author",
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'options'			=> $arrFields,
							'label'				=> 'Field',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_view'		=> '0',
          );

		$data['elements']['fields']['fc2'] = array(
              'name'        => 'fc2',
							'type'				=> 'select',
							'ljtable'			=> 'virtualdub',
              'id'          => 'fc2',
              'value'       => strval($this->input->post('fc2')),
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'options'			=> $arrCriteria,
							'label'				=> 'Criteria',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_view'		=> '0',
          );

		$data['elements']['fields']['fv2'] = array(
              'name'        => 'fv2',
							'type'				=> '',
              'id'          => 'fv2',
              'value'       => strval($this->input->post('fv2')),
              'maxlength'   => '',
              'size'        => '40',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Value',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_view'		=> '0',
          );

		$data['elements']['fields']['bf2'] = array(
              'name'        => 'bf3',
							'type'				=> 'select',
							'ljtable'		  => 'virtualdub',
              'id'          => 'bf3',
              'value'       => $this->input->post('bf3'),
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							//'options'			=> array("and" => "And", "or" => "Or", "and not" => "And NOT", "or not" => "Or NOT"),
							'options'			=> array("and" => "And", "or" => "Or"),
							'label'				=> 'Operator',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_view'		=> '0',
          );

		$data['elements']['fields']['f3'] = array(
              'name'        => 'f3',
							'type'				=> 'select',
							'ljtable'		  => 'virtualdub',
              'id'          => 'f3',
              'value'       => $this->input->post('f3') ? $this->input->post('f3') : "topicalterm",
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'options'			=> $arrFields,
							'label'				=> 'Field',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_view'		=> '0',
          );

		$data['elements']['fields']['fc3'] = array(
              'name'        => 'fc3',
							'type'				=> 'select',
							'ljtable'			=> 'virtualdub',
              'id'          => 'fc3',
              'value'       => strval($this->input->post('fc3')),
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'options'			=> $arrCriteria,
							'label'				=> 'Criteria',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_view'		=> '0',
          );

		$data['elements']['fields']['fv3'] = array(
              'name'        => 'fv3',
							'type'				=> '',
              'id'          => 'fv3',
              'value'       => strval($this->input->post('fv3')),
              'maxlength'   => '',
              'size'        => '40',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Value',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_view'		=> '0',
          );

		foreach($this->fields as $i => $f)
		{
			if($f['show_result'] == "1")
			{
				$data['elements']['labels'][$i] = $f['label'];
			}
		}


		if( ! is_client())
		{
			$data['elements']['labels']['author_name'] = "Author(s)";
			$data['elements']['labels']['client'] = "Borrowed by";
			//$data['elements']['labels']['date_borrow'] = "On";
			$data['elements']['labels']['date_due'] = "Due by";
			unset($data['elements']['labels']['id']);
			unset($data['elements']['labels']['part_x']);
			//if($data['elements']['fields']['serial_number']['value'])
			//{
			//	$data['elements']['fields']['serial_number']['value'] = intval($data['elements']['fields']['serial_number']['value']);
			//	// Add leading zeros to serial_number
			//	for($i = strlen($data['elements']['fields']['serial_number']['value']); $i < 6 ; $i++)
			//		$data['elements']['fields']['serial_number']['value'] = "0".$data['elements']['fields']['serial_number']['value'];
			//}
		}
		else{
			$data['elements']['labels']['author_name'] = "Author(s)";
			$data['elements']['labels']['book_status'] = "Status";
			//$data['elements']['labels']['client'] = "Borrowed by";
			//$data['elements']['labels']['date_borrow'] = "On";
			$data['elements']['labels']['date_due'] = "Due by";
			unset($data['elements']['labels']['id']);
			unset($data['elements']['labels']['part_x']);
			if(is_client())
				unset($data['elements']['labels']['serial_number']);
		}

		$data['title'] = "Advanced search";
		$data['selectedMenu'] = 'Books';


		if($this->input->post())
		{
			$data['elements']['items'] = $this->books_model->search_advanced($data['elements']['fields']);


//print("result");
//print_r($data['elements']['items']);

			if(count($data['elements']['items']) > 0)
			{
				$data['elements']['resultsize'] = $this->ldb->affected_rows();
				foreach($data['elements']['items'] as $i => $v){
					$data['elements']['items'][$i]['book_status'] = empty($v['client']) ? "IN" : "OUT";
				}
			}
		}
//print("<pre>");
//print_r($data['elements']['items']);
//print("</pre>");

		$this->load->view('templates/avlibheader', $data);
		if( ! is_client())
			$this->load->view('books/menubar', $data);
		else
			$this->load->view('books/menubar_clients', $data);
		$this->load->view('books/search_advanced', $data);
		$this->load->view('templates/tableview', $data);
		$this->load->view('templates/avlibfooter', $data);
	}

	function overdue($strAction = null)
	{
		if( ! is_admin())
		{
			$this->login();
			return;
		}

    $data['title'] = "Overdue books";
		$data['selectedMenu'] = 'Books';

		foreach($this->fields as $i => $f)
		{
			if($f['show_result'] == "1")
			{
				$data['elements']['labels'][$i] = $f['label'];
			}
		}

		$data['elements']['labels']['client'] = "Borrowed by";
		$data['elements']['labels']['date_borrow'] = "On";
		$data['elements']['labels']['date_due'] = "Due by";
		$data['elements']['labels']['date_reminder'] = "Last reminder";
		$data['elements']['labels']['overdueid'] = "Action";
		unset($data['elements']['labels']['id']);

		$data['elements']['tabletitle'] = $data['title'];

		$data['elements']['items'] = $this->books_model->overdue();
		$data['elements']['resultsize'] = count($data['elements']['items']);

		$data['reminderemail']['count_email'] = 0;
		$data['reminderemail']['count_noemail'] = 0;
		//print("<pre>");
		$data['sendlist'] = array();
		foreach($data['elements']['items'] as $i => $item)
		{
			$t = $this->books_model->overdue_additem($item);
			//print($t);
			if($t != 0)
				$data['sendlist'][] = "\"".$t."\"";


			// Color-coding elements to indicate delay
			$data['elements']['items'][$i]['date_due'] = $this->mark_overdue($item['date_due']);
			// Make message of overdue delay
			$data['elements']['items'][$i]['overdueid'] = $this->books_model->overdue_label($data['elements']['items'][$i]['overdueid'], $data['elements']['items'][$i]['reminder_level'], $data['elements']['items'][$i]['date_due'], $data['elements']['items'][$i]['date_reminder']);

			if($data['elements']['items'][$i]['email'] != "")
				$data['reminderemail']['count_email']++;
			else
				$data['reminderemail']['count_noemail']++;
			//$this->mark_overdue($item['date_due']);
		}
		//print_r($data['sendlist']);
		//print("</pre>");
//print("<pre>");
//print_r($data['elements']);
//print("</pre>");

		$this->load->view('templates/avlibheader', $data);
		$this->load->view('books/menubar', $data);
		$this->load->view('books/overdue', $data);
		$this->load->view('templates/tableview', $data);
		$this->load->view('templates/avlibfooter', $data);
	}

	function overduesendreminder($transactionid, $trialmode = FALSE)
	{

		if( ! is_admin())
		{
			$this->login();
			return;
		}
		// Todo: check for validity of claim (book returned etc...)
		// Check email address exists
		$transactiondata = $this->books_model->overdue_getdata($transactionid);

		if(array_key_exists("date_reminder", $transactiondata))
			$emailtemplate = $this->books_model->overdue_getemailtemplate($transactiondata);

//print("trial: ".$trialmode);
//return;

//print("template:");
//print_r($emailtemplate);
//return;
		if(!$emailtemplate || !isset($emailtemplate) || $emailtemplate == "")
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array("error" => "Error loading email tempalte")));
			return;
		}

		$this->load->library('parser');
		$email = $this->parser->parse_string($emailtemplate, $transactiondata);
		if(!isset($email) || $email == "")
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array("error" => "Error applying email tempalte")));
			return;
		}

//print('hello');
//return;

		if($trialmode)
		{
//print('sent trial');
//return;
			sleep(1);
			$ret = array("success" => "TRIAL\n=====\n".$email);
		}
		else
		{
			if($this->books_model->send_reminder_email($transactiondata['email'], "AV Library: Reminder", $email))
			{
//print('sent');
//return;
				$ret = array("success" => $email);
				$this->books_model->overdue_updatereminderdate($transactionid);

				// Set overdue_status to 2 after 3rd reminder
				if($transactiondata['reminder_level'] >= 3)
				{
					$this->load->model('clients_model');
					$this->clients_model->setlongoverdue($transactiondata['clientid']);
				}
			}
			else
				$ret = array("error" => "Error sending email");
		}


//print_r($transactiondata);
//print("<br/>");
//print_r($emailtemplate);
//print("<br/>");
//print_r($email)

		$this->output->set_content_type('application/json')->set_output(json_encode($ret));
	}

	private function mark_overdue($date)
	{
		$level = $this->books_model->overdue_category($date);
		if($level == 1)
			return "<span class=\"overdueone\">".$date."</span>";
		elseif($level >= 2)
			return "<span class=\"overduetwo\">".$date."</span>";
		else
			return $date;

		}
	//
	//function overdue_mark($date)
	//{
	//	$iLevelOne = strtotime($date."+10 days", time());
	//	$iLevelTwo = strtotime($date."+20 days", time());
	//	if(time() > $iLevelTwo)
	//		return 2;
	//	if(time() > $iLevelOne)
	//		return 1;
	//	return 0;
	//}

	function catalogue($sKeyword = NULL, $direction=NULL)
	{
		if($sKeyword != "entrydate" && is_client())
		{
			$this->login();
			return;
		}

		$data['title'] = "Catalogue";
		$data['selectedMenu'] = 'Books';
		$data['helpLink'] = "/library/help/staff#catalogue";


		foreach($this->fields as $i => $f)
		{
//			if($f['show_search'] == "1")
//			{
////print("number: ".$this->input->post($i));
//				$this->fields[$i]['value'] = $this->input->post($i);
//				$data['elements']['fields'][$i] = $this->fields[$i];
//				if($data['elements']['fields'][$i]['type'] == "hidden")
//					$data['elements']['fields'][$i]['type'] = "text";
//			}
			if($f['show_result'] == "1")
			{
				$data['elements']['labels'][$i] = $f['label'];
			}
		}

		// Entry Date search fields (for both entry date and borrow frequency searches)
		$edf['from'] = array(
						'name'        => 'from',
						'type'				=> 'date',
						'id'          => 'date_from',
						'value'       => '',
						'maxlength'   => '',
						'size'        => '',
						'style'       => '',
						'js'					=> '',
						'label'				=> 'From',
						'show_edit'		=> '0',
						'show_search' => '1',
						'show_result'	=> '0',
						'script'			=> "$(\"#date_from\").datepicker();"
					);

		$edf['to'] = array(
						'name'        => 'to',
						'type'				=> 'date',
						'id'          => 'date_to',
						'value'       => '',
						'maxlength'   => '',
						'size'        => '',
						'style'       => '',
						'js'					=> '',
						'label'				=> 'To',
						'show_edit'		=> '0',
						'show_search' => '1',
						'show_result'	=> '0',
						'script'			=> "$(\"#date_to\").datepicker();"
					);

		$edf['direction'] = array(
						'name'        => 'direction',
						'type'				=> 'select',
						'id'          => '',
						'value'				=> '0',
						'options'     => array(0=>'ascending', 1=>'descending'),
						'ljtable'			=> 'nill',
						'maxlength'   => '',
						'size'        => '',
						'style'       => '',
						'js'					=> '',
						'label'				=> 'Direction',
						'show_edit'		=> '0',
						'show_search' => '1',
						'show_result'	=> '0'
					);

		foreach($edf as $i => $f)
		{
			if($this->input->post($i))
			{
				$f['value'] = $this->input->post($i);
				$edf[$i] = $f;
			}
		}

		// Fields / search specific to entry date search
		if($sKeyword == "entrydate")
		{
			if(!is_client())
				$data['helpLink'] = "/library/help/staff#over";
			else
				$data['helpLink'] = "/library/help#listnew";
			$data['title'] = "Catalogue by entry date";
			// Focus on start date
			if(!$this->input->post("from") && !$this->input->post("to"))
				$data['focusfield'] = "date_from";
			$data['keyword'] = $sKeyword;
			$data['elements']['labels']['author_name'] = "Author(s)";
			if(is_admin())
			{
				$data['elements']['labels']['client'] = "Borrowed by";
			}
			else
			{
				$data['elements']['labels']['book_status'] = "Status";
				//unset($data['elements']['labels']['client']);
				//unset($data['elements']['labels']['client_id']);
			}
			$data['elements']['labels']['date_due'] = "Due by";
			unset($data['elements']['labels']['id']);
			unset($data['elements']['labels']['part_x']);
			$data['elements']['tabletitle'] = $data['title'];
			$data['elements']['labels']['date_created'] = "Date Created";

			foreach($edf as $i => $f)
			{
				if($this->input->post($i))
				{
					$f['value'] = $this->input->post($i);
					$edf[$i] = $f;
				}
			}

			$data['elements']['fields'] = $edf;
			$data['elements']['items'] = $this->books_model->searchentrydate($data['elements']['fields']['from']['value'], $data['elements']['fields']['to']['value'], $data['elements']['fields']['direction']['value']);
			$data['elements']['resultsize'] = count($data['elements']['items']);

			foreach($data['elements']['items'] as $i => $v){
				$data['elements']['items'][$i]['book_status'] = empty($v['client']) ? "IN" : "OUT";
			}
		}
		// Fields / search specific to borrow frequency search
		elseif($sKeyword == "borrowfrequency")
		{
			$edf = array();
					// DDC Entry search fields (for both entry date and borrow frequency searches)
			$edf['ddc_from'] = array(
							'name'        => 'ddc_from',
							'type'				=> 'text',
							'id'          => 'ddc_from',
							'value'       => '',
							'maxlength'   => '',
							'size'        => '',
							'style'       => '',
							'js'					=> '',
							'label'				=> 'DDC From',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_result'	=> '0',
							'script'			=> ""
						);

			$edf['ddc_to'] = array(
							'name'        => 'ddc_to',
							'type'				=> 'date',
							'id'          => 'ddc_to',
							'value'       => '',
							'maxlength'   => '',
							'size'        => '',
							'style'       => '',
							'js'					=> '',
							'label'				=> 'DDC To',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_result'	=> '0',
							'script'			=> ""
						);

//print_r($this->input->post());

			$data['title'] = "Catalogue by borrowing frequency";
			// Focus on start date
			$data['focusfield'] = "ddc_from";
			$data['keyword'] = $sKeyword;
			$data['elements']['tabletitle'] = $data['title'];
			$data['elements']['fields'] = $edf;
			if($this->input->post("ddc_from") && $this->input->post("ddc_to"))
			{
				$data['elements']['fields']['ddc_from']['value'] = $this->input->post("ddc_from");
				$data['elements']['fields']['ddc_to']['value'] = $this->input->post("ddc_to");
				$data['elements']['items'] = $this->books_model->searchborrowfrequency($data['elements']['fields']['ddc_from']['value'], $data['elements']['fields']['ddc_to']['value']);
				$data['elements']['resultsize'] = count($data['elements']['items']);
			}

			unset($data['elements']['labels']['id']);
			unset($data['elements']['labels']['part_x']);
			unset($data['elements']['labels']['language']);
			$data['elements']['labels']['author_name'] = "Author(s)";
			$data['elements']['labels']['borrow_qty'] = "Check-outs";
			$data['elements']['labels']['created'] = "Created";
			$data['elements']['labels']['last_borrowed'] = "Last borrowed";

//print("<pre>\n");
//print_r($data);
//print("</pre>\n");

		}
		// Fields / search specific to borrow frequency search
		elseif($sKeyword == "notborrowed")
		{
			$data['title'] = "Catalogue of NOT borrowed books";
			// Focus on start date
			if(!$this->input->post("from") && !$this->input->post("to"))
				$data['focusfield'] = "date_from";
			$data['keyword'] = $sKeyword;
			unset($data['elements']['labels']['id']);
			unset($data['elements']['labels']['part_x']);
			unset($data['elements']['labels']['language']);
			$data['elements']['labels']['author_name'] = "Author(s)";
			$data['elements']['tabletitle'] = $data['title'];

			$data['elements']['fields'] = $edf;
			$data['elements']['items'] = $this->books_model->searchnotborrowed($data['elements']['fields']['from']['value'], $data['elements']['fields']['to']['value'], $data['elements']['fields']['direction']['value']);
			$data['elements']['resultsize'] = count($data['elements']['items']);
		}
		// Fields / search specific to default catalogue display
		else
		{
			$data['elements']['items'] = $this->books_model->searchall();
			$data['elements']['labels']['client'] = "Borrowed by";
			$data['elements']['labels']['date_borrow'] = "On";
			$data['elements']['labels']['date_due'] = "Due by";
			unset($data['elements']['labels']['id']);
			unset($data['elements']['labels']['part_x ']);
			$data['elements']['tabletitle'] = $data['title'];
		}

//print("<pre>");
//print_r($data['elements']['items']);
//print("</pre>");

		$this->load->view('templates/avlibheader', $data);
		//$this->load->view('templates/deauthenticate', $data);
		if(is_client())
			$this->load->view('books/menubar_clients', $data);
		else
			$this->load->view('books/menubar', $data);
		if(isset($data['elements']['fields']) && is_array($data['elements']['fields']) && count($data['elements']['fields']) > 0)
		{
			$data['elements']['tabletitle'] = "";
			$this->load->view('books/cataloguesearch', $data);
		}
		$this->load->view('templates/tableview', $data);
		$this->load->view('templates/avlibfooter', $data);

	}


	function checkoutcount()
	{
		if(is_client())
		{
			$this->login();
			return;
		}

//		$data['title'] = "Number of books borrowed";
		$data['title'] = "Checkout count";
		$data['selectedMenu'] = 'Books';

		if (!$this->session->userdata('loggedin'))
		{
			$data['title'] .= " - please authenticate";
			// Focus on password field
			$data['focusfield'] = "password";
			$this->load->view('templates/avlibheader', $data);
			$this->load->view('books/menubar', $data);
			$this->load->view('templates/loginform', $data);
			$this->load->view('templates/avlibfooter', $data);
			return;
		}

//		if($this->input->post("interval"))
		//{
			switch($this->input->post("interval"))
			{
				case "day":
					$data['elements']['labels'] = array("date_borrow" => "Date", "books_borrowed" => "Check-outs");
					$data['elements']['items'] = $this->books_model->get_checkoutcount_day();
				break;
				case "week":
					$data['elements']['labels'] = array("Y" => "Year", "W" => "Week", "books_borrowed" => "Check-outs");
					$data['elements']['items'] = $this->books_model->get_checkoutcount_week();
				break;
				case "month":
					$data['elements']['labels'] = array("Y" => "Year", "M" => "Month", "books_borrowed" => "Check-outs");
					$data['elements']['items'] = $this->books_model->get_checkoutcount_month();
				break;
				case "year": default:
					$data['elements']['labels'] = array("Y" => "Year", "books_borrowed" => "Check-outs");
					$data['elements']['items'] = $this->books_model->get_checkoutcount_year();
				break;
			}
		//}

		$this->load->view('templates/avlibheader', $data);
		//$this->load->view('templates/deauthenticate', $data);
		$this->load->view('books/menubar', $data);
		$this->load->view('books/checkoutcount', $data);
		//if($this->input->post("interval"))
			$this->load->view('templates/tableview', $data);
		$this->load->view('templates/avlibfooter', $data);

	}

	function ajaxgeneratebooknumber($bookid = null)
	{
		if(is_client())
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array("Result" => "login first")));
			return;
		}

		$bookid = $this->input->post('bookid').$bookid;
		if($bookid == FALSE)
			return;

		$booknumber = $this->books_model->ajaxgeneratebooknumber($bookid);



		//print($booknumber[0]['name']."\n");
		//print(substr(mb_convert_encoding($booknumber[0]['name'], "UTF-8"),0,4)."\n");
		//print_r($booknumber);
		//die();

		if(isset($booknumber) && is_array($booknumber) && count($booknumber) > 0)
		{
			$alphamap=array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
			$alphamap = array_flip($alphamap);
//			$ret[] = $booknumber[0]['dewey_decimal_classification'];
			// Had substr($booknumber[0]['name'],0,4)) which doesn't work for authors with accents (m�alouf) - so changed it to $booknumber[0]['name']
			$ret[] = ($booknumber[0]['name'] == "" ? "!no_author!" : $booknumber[0]['name'])." ".(array_key_exists(strtolower($booknumber[0]['title']), $alphamap) ? $alphamap[strtolower($booknumber[0]['title'])]+1 : 0).(($booknumber[0]['part_x'] > 0) ? ":".$booknumber[0]['part_x'] : "");
//			$ret[] = str_pad($booknumber[0]['serial_number'], 5, "0", STR_PAD_LEFT);
//			print("<pre>".implode("\n", $ret)."</pre>");
			//$this->output->set_content_type('application/json')->set_output(json_encode($ret));
			$this->output->set_output(implode("\n", $ret));
		}
	}

	function ajaxlookupload($name = null, $bookid = null)
	{
		if(is_client())
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array("Result" => "login first")));
			return;
		}

		$name = $this->input->post('name').urldecode($name);
		$bookid = $this->input->post('bookid').urldecode($bookid);
		if($name == FALSE || $bookid == FALSE)
			$this->output->set_content_type('application/json')->set_output(json_encode(array("result"=>"Invalid entry book ".$bookid." name ".$name)));
		elseif($res = $this->books_model->ajaxlookupload($name, $bookid))
			$this->output->set_content_type('application/json')->set_output(json_encode($res));
		else
			$this->output->set_content_type('application/json')->set_output(json_encode(array("result"=>"Error while loading lookup values")));
	}

	// Add author to book
	function ajaxlookupadd($name = null, $bookid = null, $value = null)
	{
		if(is_client())
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array("Result" => "login first")));
			return;
		}
//print_r($this->input->post());
		$name = $this->input->post('name').urldecode($name);
		$bookid = $this->input->post('bookid').urldecode($bookid);
		$value = $this->input->post('value').urldecode($value);
//print("book ".$bookid."      person ".$personid."      role ".$roleid));
		if($name == FALSE || $bookid == FALSE || $value == FALSE)
			$this->output->set_content_type('application/json')->set_output(json_encode(array("result"=>"Invalid entry      name ".$name."      book ".$bookid."      value ".$value)));
		elseif($this->books_model->ajaxlookupadd($name, $bookid, $value))
			$this->output->set_content_type('application/json')->set_output(json_encode(array("result"=>"Success")));
		else
			$this->output->set_content_type('application/json')->set_output(json_encode(array("result"=>"Error while saving")));
	}

	function ajaxlookupdelete($name = null, $lookupid = null)
	{
		if(is_client())
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array("Result" => "login first")));
			return;
		}
		$name = $this->input->post('name').urldecode($name);
		$lookupid = $this->input->post('lookupid').urldecode($lookupid);
		if($name == FALSE || $lookupid == FALSE)
			$this->output->set_content_type('application/json')->set_output(json_encode(array("result"=>"Invalid entry name ".$name." lookupid ".$lookupid)));
		elseif($this->books_model->ajaxlookupdelete($name, $lookupid))
			$this->output->set_content_type('application/json')->set_output(json_encode(array("result"=>"Success")));
		else
			$this->output->set_content_type('application/json')->set_output(json_encode(array("result"=>"Error while saving")));
	}

	function ajaxlookupcomplete($name = null, $term = null)
	{
		if(is_client())
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array("Result" => "login first")));
			return;
		}
		$name = $this->input->post('name').urldecode($name);
		$term = $this->input->post('term').urldecode($term);
		if($name == FALSE || $term == FALSE)
			return;

		$res = $this->books_model->ajaxlookupcomplete($name, $term);

		if(isset($res) && is_array($res) && count($res) > 0)
		$this->output->set_content_type('application/json')->set_output(json_encode($res));
	}

	function ajaxsearch($term = FALSE, $field = FALSE)
	{
		if(is_client())
		{
			$this->output->set_content_type('application/json')->set_output(json_encode(array("Result" => "login first")));
			return;
		}
		$needle = $this->input->post('term').$term;
		$haystack = $this->input->post('field').$field;
		if($needle == FALSE)
			return;

		$res = $this->books_model->ajaxsearch($needle, $haystack);
		if(isset($res) && is_array($res) && count($res) > 0)
			$this->output->set_content_type('application/json')->set_output(json_encode($res));
	}
}
?>