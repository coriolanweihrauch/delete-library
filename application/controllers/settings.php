<?php
class Settings extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('settings_model');
		$this->load->helper('form');
		$this->load->helper('avlib');
		$this->load->library('session');
		$this->lists = $this->settings_model->lists;
		$this->mergelists = $this->settings_model->mergelists;
		$this->ldb = $this->settings_model->ldb;
	}

	public function info()
	{
		phpinfo();
	}

	private function login()
	{
		$data['title'] = "Please authenticate";
		$data['selectedMenu'] = 'Settings';
		// Focus on password field
		$data['focusfield'] = "password";
		$this->load->view('templates/avlibheader', $data);
		//$this->load->view('books/menubar', $data);
		$this->load->view('templates/loginform', $data);
		$this->load->view('templates/avlibfooter', $data);
	}

	public function authenticate()
	{
		$password = $this->input->post('password');
		$username = $this->input->post('username');
		$redirect = $this->input->post("redirect");

		// Verify password and log in
		$q = $this->ldb->query("SELECT name FROM user WHERE name='".$username."' AND password = PASSWORD('".$password."')");
		$r = $q->result_array();
		if(isset($r) && array_key_exists('0', $r) && array_key_exists('name', $r[0]) && $r[0]['name'] == $username)
		{
			$this->session->set_userdata('loggedin', true);
			$this->session->set_userdata('credentials', $username);
		}
	
		// Redirect back
		if($redirect)
			header('Location: '.$redirect);
		else
			header('Location: '.$_SERVER["HTTP_REFERER"]);
	}

	public function deauthenticate()
	{
		if($this->session->userdata('credentials') == "admin")
			$this->session->set_userdata('credentials', "staff");
		else
		{
			// Complete logout
			$this->session->sess_destroy();
		}
		// Redirect back
		if(array_key_exists("HTTP_REFERER", $_SERVER))
			header('Location: '.$_SERVER["HTTP_REFERER"]);
		else
			header("Location: /library ");
	}

	public function index()
	{
		if( ! is_admin())
		{
			$this->login();
			return;
		}

    $data['title'] = "Settings index";
		$data['selectedMenu'] = 'Settings';
		$data['helpLink'] = "/library/help/staff#settings";

		$this->load->view('templates/avlibheader', $data);
		//$this->load->view('templates/deauthenticate', $data);
		$this->load->view('settings/menubar', $data);
		$this->load->view('templates/avlibfooter', $data);
	}

  public function lists($listname = NULL, $action = NULL, $id = NULL)
  {
		if( ! is_admin())
		{
			$this->login();
			return;
		}
		$this->load->library('form_validation');

		$data['title'] = 'Edit lists';
		$data['selectedMenu'] = 'Settings';
		$data['lists'] = $this->lists;
		$data['helpLink'] = "/library/help/staff#lists";

		if($action)
		{
			switch($action)
			{
				case "delete":
					if($id)
					{
						$this->settings_model->delete_list_item($listname, $id);
						$data['redirect'] = "settings/lists/".$listname;		
						$this->load->view('templates/avlibheader', $data);
						//$this->load->view('templates/deauthenticate', $data);
						$this->load->view('settings/menubar', $data);
						$this->load->view('settings/lists', $data);
						$this->load->view('templates/messages', $data);
						$this->load->view('templates/saved', $data);
						$this->load->view('templates/avlibfooter', $data);
						return;
					}
				break;
			}
		}
		// List selected
		elseif($listname)
		{
			// Save submitted date
			if($data['submitted'] = $this->input->post())
			{
//print_r($this->input->post());
				if($this->settings_model->save_list($listname, $this->input->post()))
					$data['messages'][] = array("success" => "Successfully updated list");
				else
					$data['messages'][] = array("notice" => "Error updating list ; please try again");
				$data['messagescustomlocation'] = 1;
			}

			$data['listname'] = $listname;
			$data['list'] = $this->settings_model->load_list($data['listname']);
		}

		$this->load->view('templates/avlibheader', $data);
		//$this->load->view('templates/deauthenticate', $data);
		$this->load->view('settings/menubar', $data);
		$this->load->view('settings/lists', $data);
		$this->load->view('templates/messages', $data);
		$this->load->view('settings/listseditor', $data);
		$this->load->view('templates/avlibfooter', $data);
  }


	public function printlabels()
	{
		if( ! is_admin())
		{
			$this->login();
			return;
		}

    $data['title'] = "Print labels";
		$data['selectedMenu'] = 'Settings';
		$data['helpLink'] = "/library/help/staff#print";

		// Add additional search fields
		$data['elements']['fields']['output'] = array(
              'name'        => 'output',
							'type'				=> 'hidden',
              'id'          => 'labeloutput',
              'value'				=> 'view',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> '',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_view'		=> '0',
            );
		
		$data['elements']['fields']['format'] = array(
              'name'        => 'format',
							'type'				=> 'select',
							'ljtable'			=> NULL,
              'id'          => '',
              'value'				=> '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Format',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_view'		=> '0',
							'options'       => array(1=>"Spine Label", 2=>"Barcode label"),
            );

		$data['elements']['fields']['date_from'] = array(
						'name'        => 'date_from',
						'type'				=> 'date',
						'id'          => 'date_from',
						'value'       => '',
						'maxlength'   => '',
						'size'        => '',
						'style'       => '',
						'js'					=> '',
						'label'				=> 'From',
						'show_edit'		=> '0',
						'show_search' => '1',
						'show_view'		=> '0',
						'script'			=> "$(\"#date_from\").datepicker();"
					);

		$data['elements']['fields']['date_to'] = array(
						'name'        => 'date_to',
						'type'				=> 'date',
						'id'          => 'date_to',
						'value'       => '',
						'maxlength'   => '',
						'size'        => '',
						'style'       => '',
						'js'					=> '',
						'label'				=> 'To',
						'show_edit'		=> '0',
						'show_search' => '1',
						'show_view'		=> '0',
						'script'			=> "$(\"#date_to\").datepicker();"
					);

		$data['elements']['fields']['number_from'] = array(
						'name'        => 'number_from',
						'type'				=> 'number',
						'id'          => 'number_from',
						'value'       => '',
						'maxlength'   => '',
						'size'        => '',
						'style'       => '',
						'js'					=> '',
						'label'				=> 'Barcode from',
						'show_edit'		=> '0',
						'show_search' => '1',
						'show_view'		=> '0',
					);

		$data['elements']['fields']['number_to'] = array(
						'name'        => 'number_to',
						'type'				=> 'number',
						'id'          => 'number_to',
						'value'       => '',
						'maxlength'   => '',
						'size'        => '',
						'style'       => '',
						'js'					=> '',
						'label'				=> 'Barcode to',
						'show_edit'		=> '0',
						'show_search' => '1',
						'show_view'		=> '0',
					);

		$data['elements']['fields']['list_from'] = array(
						'name'        => 'list_from',
						'type'				=> 'textarea',
						'cols'				=> '7',
						'rows'				=> '10',
						'id'          => 'list_from',
						'value'       => '',
						'maxlength'   => '',
						'size'        => '',
						'style'       => '',
						'js'					=> '',
						'label'				=> 'Barcodes',
						'show_edit'		=> '0',
						'show_search' => '1',
						'show_view'		=> '0',
					);

		foreach($data['elements']['fields'] as $name => $field)
		{
			if(is_array($this->input->post()) && array_key_exists($name, $this->input->post()))
				$data['elements']['fields'][$name]['value'] = $this->input->post($name);
		}

		if(is_array($this->input->post()) && array_key_exists('type', $this->input->post()))
		{
			$data['focustab'] = $this->input->post("type");
			$data['focusfield'] = $this->input->post("type")."_from";

			$data['elements']['tabletitle'] = "Elements to print";

			$data['elements']['labels']['serial_number'] = "Barcode";
			$data['elements']['labels']['dewey_decimal_classification'] = "DDC";
			$data['elements']['labels']['book_number'] = "Book Number";

//print_r($this->input->post());

			$data['elements']['items'] = $this->settings_model->printlabels($this->input->post());

//print_r($data['focustab']);

			$data['export_type'] = $this->input->post("submit");
		}

		if($this->input->post('submit') == 'Export')
		{
			$this->load->library('excel');
			//$this->exportlabels($data);
			$objWriter = $this->settings_model->exportlabels($data);
			//force user to download the Excel file without writing it to server's HD
			switch($data['elements']['fields']['format']['value'])
			{
				case 1: $formattitle = "spine_"; break;
				case 2: $formattitle = "barcode_"; break;
				default: $formattitle = ""; break;
			}
			$filename = "AVLibLabels_".$formattitle.date("Ymj_Hi").".xls";
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
	
			$objWriter->save('php://output');
		}
		else
		{
			if(array_key_exists("items", $data['elements']))
				$data['elements']['resultsize'] = count($data['elements']['items']);
			$this->load->view('templates/avlibheader', $data);
			//$this->load->view('templates/deauthenticate', $data);
			$this->load->view('settings/menubar', $data);
			$this->load->view('settings/printlabels', $data);
			$this->load->view('templates/tableview', $data);
			$this->load->view('templates/avlibfooter', $data);
		}
	}


  public function blacklist($action = NULL, $id = NULL)
  {
		if( ! is_admin())
		{
			$this->login();
			return;
		}
		$this->load->library('form_validation');

		$data['title'] = 'Client computer list';
		$data['selectedMenu'] = 'Settings';
		$data['listname'] = "Client IP list";

		if($action)
		{
			switch($action)
			{
				case "delete":
					if($id)
					{
						$this->settings_model->delete_blacklist_item($id);
						$data['redirect'] = "settings/blacklist/";		
						$this->load->view('templates/avlibheader', $data);
						//$this->load->view('templates/deauthenticate', $data);
						$this->load->view('settings/menubar', $data);
						$this->load->view('settings/blacklist', $data);
						$this->load->view('templates/messages', $data);
						$this->load->view('templates/saved', $data);
						$this->load->view('templates/avlibfooter', $data);
						return;
					}
				break;
			}
		}

		// Save submitted date
		if($data['submitted'] = $this->input->post())
		{
//print_r($this->input->post());
			if($this->settings_model->save_blacklist($this->input->post()))
				$data['messages'][] = array("success" => "Successfully updated blacklist");
			else
				$data['messages'][] = array("notice" => "Error updating list ; please try again");
			$data['messagescustomlocation'] = 1;
		}

		$data['list'] = $this->settings_model->load_blacklist();

		$this->load->view('templates/avlibheader', $data);
		//$this->load->view('templates/deauthenticate', $data);
		$this->load->view('settings/menubar', $data);
		$this->load->view('settings/blacklist', $data);
		$this->load->view('templates/messages', $data);
		$this->load->view('settings/blacklistseditor', $data);
		$this->load->view('templates/avlibfooter', $data);
  }


	public function password()
	{
		if( ! is_admin())
		{
			$this->login();
			return;
		}

    $data['title'] = "Change password";
		$data['selectedMenu'] = 'Settings';
		// Focus on start date
		$data['focusfield'] = "newpassword";
		$data['helpLink'] = "/library/help/staff#password";

		$newpassword = $this->input->post('newpassword');
		if(isset($newpassword) && $newpassword !== FALSE)
		{
			$this->ldb->query("UPDATE user set password = PASSWORD('".$newpassword."') WHERE name='staff'");
			$data['messages'][] = array("success" => "Successfully update the password");
		}

		$newadminpassword = $this->input->post('newadminpassword');
		if(isset($newadminpassword) && $newadminpassword !== FALSE)
		{
			$this->ldb->query("UPDATE user set password = PASSWORD('".$newadminpassword."') WHERE name='admin'");
			$data['messages'][] = array("success" => "Successfully update the password");
		}

		$this->load->view('templates/avlibheader', $data);
		$this->load->view('settings/menubar', $data);
		//$this->load->view('templates/deauthenticate', $data);
		$this->load->view('settings/password', $data);
		$this->load->view('templates/avlibfooter', $data);
	}

	public function merge($listname = NULL)
	{
		if( ! is_admin())
		{
			$this->login();
			return;
		}

    $data['title'] = "Merge";
		$data['selectedMenu'] = 'Settings';
		$data['lists'] = $this->mergelists;
		$data['helpLink'] = "/library/help/staff#merge";

		// List selected
		if($listname)
		{
			// Save submitted date
			if($data['submitted'] = $this->input->post())
			{
				if($this->settings_model->merge($listname, $this->input->post()))
					$data['messages'][] = array("success" => "Successfully merged items");
				else
					$data['messages'][] = array("notice" => "Error merging items; please try again");
				$data['messagescustomlocation'] = 1;
			}

			$data['listtitle'] = $data['lists'][$listname];
			$data['listname'] = $listname;
			//$data['list'] = $this->settings_model->load_list($data['listname']);

			$this->load->view('templates/avlibheader', $data);
			//$this->load->view('templates/deauthenticate', $data);
			$this->load->view('settings/menubar', $data);
			$this->load->view('settings/merge', $data);
			$this->load->view('templates/messages', $data);
			$this->load->view('settings/mergeeditor', $data);
			$this->load->view('templates/avlibfooter', $data);
		}

		else
		{
			$this->load->view('templates/avlibheader', $data);
			//$this->load->view('templates/deauthenticate', $data);
			$this->load->view('settings/menubar', $data);
			$this->load->view('settings/merge', $data);
			$this->load->view('templates/messages', $data);
			$this->load->view('templates/avlibfooter', $data);
		}


	}

	function reminders()
	{
		if( ! is_admin())
		{
			$this->login();
			return;
		}

		$this->load->library('form_validation');

		$data['title'] = 'Edit reminders';
		$data['selectedMenu'] = 'Settings';
		$data['lists'] = $this->lists;
		$data['helpLink'] = "/library/help/staff#reminders";

		// Save submitted date
		if($this->input->post())
		{
//print_r($this->input->post());
			if($this->settings_model->save_reminders($this->input->post()))
				$data['messages'][] = array("success" => "Successfully updated reminders");
			else
				$data['messages'][] = array("notice" => "Error updating reminders ; please try again");
			$data['messagescustomlocation'] = 1;
		}

		$data['reminders'] = $this->settings_model->load_reminders();

		$this->load->view('templates/avlibheader', $data);
		//$this->load->view('templates/deauthenticate', $data);
		$this->load->view('settings/menubar', $data);
		$this->load->view('settings/reminderstitle', $data);
		$this->load->view('templates/messages', $data);
		$this->load->view('settings/reminderseditor', $data);
		$this->load->view('templates/avlibfooter', $data);
	}

	function backup()
	{
		if( ! is_admin())
		{
			$this->login();
			return;
		}
		
		// Load the DB utility class
		$this->load->dbutil();

		// Backup your entire database and assign it to a variable
		$prefs = array(
                'tables'      => array(),  					// Array of tables to backup.
                'ignore'      => array(),           // List of tables to omit from the backup
                'format'      => 'txt',            // gzip, zip, txt
                'filename'    => $name.'.sql',    	// File name - NEEDED ONLY WITH ZIP FILES
                'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
                'newline'     => "\n"               // Newline character used in backup file
              );

$backup =& $this->dbutil->backup($prefs);
$name = "AVLibBackup_".date("Ymj_Hi");
//die(zlib_decode($backup, 1000));
		//$backup =& $this->ldbutil->backup(); 
		
		// Load the file helper and write the file to your server
		//$this->load->helper('file');
		//write_file('/Library/WebServer/Documents/library/application/cache/mybackup.sql', $backup); 
		
		// Load the download helper and send the file to your desktop
		$this->load->helper('download');
		header('Content-Type: text/plain', true);
		header('Content-Disposition: attachment; filename="'.$name.'.sql"', true);
//header("Expires: Mon, 26 Jul 1997 05:00:00 GMT", true);
//header('Accept-Ranges: bytes', true);
//header("Cache-control: private", true);
//header('Pragma: private', true);
		force_download($name.".sql", $backup);
	}
	
	public function changeDatabase($newDB = 'default')
	{
		$this->session->set_userdata('avlib_database', $newDB);
		if(array_key_exists("HTTP_REFERER", $_SERVER))
			header('Location: '.$_SERVER["HTTP_REFERER"]);
		else
			header("Location: /library ");
	}
}
?>