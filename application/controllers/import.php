<?php

# Set the character encoding to UTF-8 for all page output
class Import extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('avlib');
		$this->load->helper('form');
		$this->load->model('import_model');
		$this->fields = $this->import_model->fields;
	}

  public function index()
  {
		$this->books();
	}

	public function books($strAction = NULL)
	{
    $this->load->helper('avlib');    

//print("encoding: <br/>");
//iconv_set_encoding("internal_encoding", "UTF-8");
//iconv_set_encoding("output_encoding", "ISO-8859-1");
//iconv_set_encoding("input_encoding", "UTF-8");
//var_dump(iconv_get_encoding('all'));


    $data['title'] = "Imporing";

    //$data['elements']['tabletitle'] = "Import sample";
    //$data['elements']['items'] = $this->import_model->import();


    $data['title'] = "Imporing Books";
		$data['elements']['tabletitle'] = "Sample of imported data (first 100 rows)";

		switch($strAction)
		{
			case "import":
				$this->import_model->truncate_table("books");
				$data['elements']['items'] = $this->import_model->load_books();

//print("<pre>");
//print_r($data['elements']['items']);
//print("</pre>");
//die();

				$this->import_model->save_table("books", $data['elements']['items']);
				$data['elements']['items'] = array_slice($data['elements']['items'], 0, 100);
			break;
		}

		//if(count($data['elements']['items']))
		//	foreach($data['elements']['items'] as $i => $v)
		//	  if(!is_array($v))
		//	    $data['elements']['items'][$i] = utf8_decode($v);



    $data['elements']['labels'] = array(
			"id" => "id",
			"date_created" => "date_created",
			"title" => "title",
			"statement_of_responsability" => "statement_of_responsability",
			"edition_statement" => "edition_statement",
			"xml_wholerecord" => "xml_wholerecord",
			"serial_number" => "serial_number",
			"dewey_decimal_classification" => "dewey_decimal_classification",
			"book_number" => "book_number",
			"isbn" => "isbn",
			"issn" => "issn",
      );

		$data['affected_rows'] = $this->import_model->affected_rows();
		$data['count_rows'] = $this->import_model->count_rows("books");

    $this->load->view('templates/avlibheader', $data);
    $this->load->view('import/menubar', $data);
		$this->load->view('import/books', $data);
		$this->load->view('templates/tableview', $data);
		$this->load->view('import/metadata', $data);
    $this->load->view('templates/avlibfooter', $data);
  }

	public function authors($strAction = NULL)
	{
		$this->load->helper('avlib');    

    $data['title'] = "Imporing Authors";
		$data['elements']['tabletitle'] = "Sample of imported data (first 100 rows)";

		switch($strAction)
		{
			case "import":
				$this->import_model->truncate_table("authors");
				$data['elements']['items'] = $this->import_model->truncate_table("authors_books");

				$data['elements']['items'] = $this->import_model->load_authors();
				$this->import_model->save_table("authors", $data['elements']['items']);

				$data['elements']['items'] = $this->import_model->load_authors_books();
				$this->import_model->save_table("authors_books", $data['elements']['items']);
				$data['elements']['items'] = array_slice($data['elements']['items'], 0, 100);
			break;
		}


    $data['elements']['labels'] = array(
      "author_id" => "Author	id",
			"book_id" => "Book id",
      );

		$data['affected_rows'] = $this->import_model->affected_rows();
		$data['count_rows'] = $this->import_model->count_rows("authors_books");

    $this->load->view('templates/avlibheader', $data);
    $this->load->view('import/menubar', $data);
		$this->load->view('import/authors', $data);
		$this->load->view('templates/tableview', $data);
		$this->load->view('import/metadata', $data);
    $this->load->view('templates/avlibfooter', $data);
	}

	public function authors_books($strAction = NULL)
	{
		$this->load->helper('avlib');    

    $data['title'] = "Imporing Authors-Book links";
		$data['elements']['tabletitle'] = "Sample of imported data (first 100 rows)";

		switch($strAction)
		{
			case "import":
				$data['elements']['items'] = $this->import_model->load_authors_books();
				$this->import_model->save_table("authors_books", $data['elements']['items']);
				$data['elements']['items'] = array_slice($data['elements']['items'], 0, 100);
			break;

			case "truncate":
				$data['elements']['items'] = $this->import_model->truncate_table("authors_books");
			break;
		}

//foreach($data['elements']['items'] as $i => $v)
//  if(!is_array($v))
//    $data['elements']['items'][$i] = utf8_decode($v);

    $data['elements']['labels'] = array(
// cataloguerecord.created_on table
      "author_id" => "Author	id",
			"book_id" => "Book id",
      );

		$data['affected_rows'] = $this->import_model->affected_rows();
		$data['count_rows'] = $this->import_model->count_rows("authors_books");

    $this->load->view('templates/avlibheader', $data);
    $this->load->view('import/menubar', $data);
		$this->load->view('import/authors_books', $data);
		$this->load->view('templates/tableview', $data);
		$this->load->view('import/metadata', $data);
    $this->load->view('templates/avlibfooter', $data);
	}

	public function languages($strAction = NULL)
	{
		$this->load->helper('avlib');    

    $data['title'] = "Imporing Languages";
		$data['elements']['tabletitle'] = "Sample of imported data (first 100 rows)";

		switch($strAction)
		{
			case "import":
				$this->import_model->truncate_table("languages");
				$this->import_model->truncate_table("books_languages");

				$data['elements']['languages'] = $this->import_model->load_distinct_languages();
				$this->import_model->save_table("languages", $data['elements']['languages']);
				$data['elements']['books_languages'] = $this->import_model->load_books_languages();
				$this->import_model->save_table("books_languages", $data['elements']['books_languages']);
			break;
		}

		$data['affected_rows'] = $this->import_model->affected_rows();
		$data['count_rows'] = $this->import_model->count_rows("books_languages");

    $this->load->view('templates/avlibheader', $data);
    $this->load->view('import/menubar', $data);
		$this->load->view('import/languages', $data);
		$this->load->view('import/metadata', $data);
    $this->load->view('templates/avlibfooter', $data);
	}

	public function keywords($strAction = NULL)
	{
		$this->load->helper('avlib');    

    $data['title'] = "Imporing Keywords";
		$data['elements']['tabletitle'] = "Sample of imported data (first 100 rows)";

		switch($strAction)
		{
			case "import":
				$this->import_model->truncate_table("keywords");
				$this->import_model->truncate_table("books_keywords");

				$data['elements']['keywords'] = $this->import_model->load_distinct_keywords();
				$this->import_model->save_table("keywords", $data['elements']['keywords']);
				$data['elements']['books_keywords'] = $this->import_model->load_books_keywords();
				$this->import_model->save_table("books_keywords", $data['elements']['books_keywords']);
			break;
		}

		$data['affected_rows'] = $this->import_model->affected_rows();
		$data['count_rows'] = $this->import_model->count_rows("books_keywords");

    $this->load->view('templates/avlibheader', $data);
    $this->load->view('import/menubar', $data);
		$this->load->view('import/keywords', $data);
		$this->load->view('import/metadata', $data);
    $this->load->view('templates/avlibfooter', $data);
	}

	public function topicalterms($strAction = NULL)
	{
		$this->load->helper('avlib');    

    $data['title'] = "Imporing Topical Terms";
		$data['elements']['tabletitle'] = "Sample of imported data (first 100 rows)";

		switch($strAction)
		{
			case "import":
				$this->import_model->truncate_table("topicalterms");
				$this->import_model->truncate_table("books_topicalterms");

				$this->import_model->save_table("topicalterms", $this->import_model->load_topicalterms());
				$this->import_model->save_table("books_topicalterms", $this->import_model->load_books_topicalterms());
			break;
		}

		$data['affected_rows'] = $this->import_model->affected_rows();
		$data['count_rows'] = $this->import_model->count_rows("books_topicalterms");

    $this->load->view('templates/avlibheader', $data);
    $this->load->view('import/menubar', $data);
		$this->load->view('import/topicalterms', $data);
		$this->load->view('import/metadata', $data);
    $this->load->view('templates/avlibfooter', $data);
	}


	public function series($strAction = NULL)
	{
		$this->load->helper('avlib');    

    $data['title'] = "Imporing Series";

		switch($strAction)
		{
			case "import":
				$this->import_model->import_series();
			break;
		}

		$data['affected_rows'] = $this->import_model->affected_rows();
		$data['count_rows'] = $this->import_model->count_rows("books_topicalterms");

    $this->load->view('templates/avlibheader', $data);
    $this->load->view('import/menubar', $data);
		$this->load->view('import/series', $data);
		$this->load->view('import/metadata', $data);
    $this->load->view('templates/avlibfooter', $data);
	}

	public function xml($strAction = NULL, $id = false)
	{
		$this->load->helper('avlib');    

    $data['title'] = "Imporing XML";

		switch($strAction)
		{
			case "import":
				$this->import_model->load_xml($id);
			break;
		}

		$data['affected_rows'] = $this->import_model->affected_rows();
		$data['count_rows'] = $this->import_model->count_rows("books");

    $this->load->view('templates/avlibheader', $data);
    $this->load->view('import/menubar', $data);
		$this->load->view('import/xml', $data);
		$this->load->view('import/metadata', $data);
    $this->load->view('templates/avlibfooter', $data);
	}

	public function notes($strAction = NULL)
	{
		$this->load->helper('avlib');    

    $data['title'] = "Imporing notes";

		switch($strAction)
		{
			case "import":
				$this->import_model->load_notes();
			break;
		}

		$data['affected_rows'] = $this->import_model->affected_rows();
		$data['count_rows'] = $this->import_model->count_rows("books");

    $this->load->view('templates/avlibheader', $data);
    $this->load->view('import/menubar', $data);
		$this->load->view('import/notes', $data);
		$this->load->view('import/metadata', $data);
    $this->load->view('templates/avlibfooter', $data);
	}

	public function isbn($strAction = null)
	{
		$this->load->helper('avlib');    

    $data['title'] = "Cleaning ISBNs";

		switch($strAction)
		{
			case "import":
				$this->import_model->clean_isbn();
			break;
		}

		$data['affected_rows'] = $this->import_model->affected_rows();
		$data['count_rows'] = $this->import_model->count_rows("books");

    $this->load->view('templates/avlibheader', $data);
    $this->load->view('import/menubar', $data);
		$this->load->view('import/isbn', $data);
		$this->load->view('import/metadata', $data);
    $this->load->view('templates/avlibfooter', $data);
	}

	public function clients($strAction = null)
	{
		$this->load->helper('avlib');    

    $data['title'] = "Importing Clients";

		switch($strAction)
		{
			case "import":
				$this->import_model->truncate_table("clients");
				$data = $this->import_model->load_clients();
				$this->import_model->save_table("clients", $data);
				$this->import_model->client_generate_groups();
				$this->import_model->truncate_table("books_clients");
				$this->import_model->load_transactions();
			break;
		}

		$data['affected_rows'] = $this->import_model->affected_rows();
		$data['count_rows'] = $this->import_model->count_rows("clients");

    $this->load->view('templates/avlibheader', $data);
    $this->load->view('import/menubar', $data);
		$this->load->view('import/clients', $data);
		$this->load->view('import/metadata', $data);
    $this->load->view('templates/avlibfooter', $data);
	}

}
?>