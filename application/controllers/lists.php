<?php
class Lists extends CI_Controller {
	
	private $ldb;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('lists_model');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->load->helper('avlib');
		$this->ldb = $this->lists_model->ldb;
	}

	public function index()
	{
    if( ! is_admin())
		{
			$this->login();
			return;
		}
		$this->search();
	}
  
  private function login($redirect = null)
	{
		$data['title'] = "Please authenticate";
		$data['selectedMenu'] = 'Settings';
		if($redirect)
			$data['redirect'] = $redirect;
		// Focus on password field
		$data['focusfield'] = "password";
		$this->load->view('templates/avlibheader', $data);
		//$this->load->view('books/menubar', $data);
		$this->load->view('templates/loginform', $data);
		$this->load->view('templates/avlibfooter', $data);
	}
  
  public function edit($list, $id = null)
  {
		if( ! is_admin())
		{
			if(!$id)
			{
				$this->login();
				return;
			}
			else{
				$this->view($list, $id);
				return;
			}
		}

		$data['title'] = 'Edit '.$this->lists_model->singular($list);
		$data['selectedMenu'] = 'Settings';
		//$data['elements']['fields'] = $this->fields;
		// Focus on password field
		$data['focusfield'] = $list;

		$data['elements']['fields'] = array();
    // New entry
    if($id == null)
		{
			if(array_key_exists("id", $data['elements']['fields']))
			{
				$id = $this->lists_model->get_next_id($list);
			}
		}
    // Edit existing entry
		else
		{
      // Get data
			$data['elements']['fields'] = $this->lists_model->load($list, $id);
			$data['elements']['fields']['list'] = $list;
			
			$data['elements']['id_next'] = $this->lists_model->get_id_next($list, $id);
			$data['elements']['id_prev'] = $this->lists_model->get_id_prev($list, $id);
			
			//print("next: ".$id_next);
			//print("prev: ".$id_prev);
			
      // Get books
      $data['elements']['items'] = $this->lists_model->load_associated_books($list, $id);
			$data['elements']['resultsize'] = count($data['elements']['items']);
			//print_r($data['elements']['items']);
			//$data['elements']['labels'] = array("serial_number" => "Barcode", "title" => "Title", "language" => "Language", "dewey_decimal_classification" =>	"DDC", "book_number" =>	"Book number", "author_name" => "Author(s)");
			$data['elements']['labels'] = array("serial_number" => "Barcode", "title" => "Title", "dewey_decimal_classification" =>	"DDC", "book_number" =>	"Book number");
			$data['elements']['tabletitle'] = "This subject appears in the following books";
		}

		//print_r($data['elements']['fields']);
		
		$this->form_validation->set_rules('name', 'Name', 'required');
    // Display data
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/avlibheader', $data);
			$this->load->view('settings/menubar', $data);
			$this->load->view('lists/edit', $data);
			$this->load->view('templates/tableview', $data);
			$this->load->view('templates/avlibfooter', $data);
		}
    // Save
		else
		{
      $this->lists_model->save($list, $id, $this->input->post('name'), $this->input->post('notes'));
			$this->load->view('templates/avlibheader', $data);
			$this->load->view('templates/saved', $data);
			$this->load->view('templates/avlibfooter', $data);
		}
  }

 public function view($list, $id)
  {
		## Admin => edit
		if(is_admin())
		{
			$this->edit($list, $id);
			return;
		}

		$data['title'] = 'View '.$this->lists_model->singular($list);
		$data['selectedMenu'] = 'Books';
		//$data['elements']['fields'] = $this->fields;
		// Focus on password field
		$data['focusfield'] = $list;

		$data['elements']['fields'] = array();
    // New entry
    if($id == null)
		{
			if(array_key_exists("id", $data['elements']['fields']))
			{
				$id = $this->lists_model->get_next_id($list);
			}
		}
    // Edit existing entry
		else
		{
      // Get data
			$data['elements']['fields'] = $this->lists_model->load($list, $id);
			$data['elements']['fields']['list'] = $list;
			
			$data['elements']['id_next'] = $this->lists_model->get_id_next($list, $id);
			$data['elements']['id_prev'] = $this->lists_model->get_id_prev($list, $id);
			
			//print("next: ".$id_next);
			//print("prev: ".$id_prev);
			
      // Get books
      $data['elements']['items'] = $this->lists_model->load_associated_books($list, $id);
			$data['elements']['resultsize'] = count($data['elements']['items']);
			//print_r($data['elements']['items']);
			//$data['elements']['labels'] = array("serial_number" => "Barcode", "title" => "Title", "language" => "Language", "dewey_decimal_classification" =>	"DDC", "book_number" =>	"Book number", "author_name" => "Author(s)");
			$data['elements']['labels'] = array("serial_number" => "Barcode", "title" => "Title", "dewey_decimal_classification" =>	"DDC", "book_number" =>	"Book number");
			$data['elements']['tabletitle'] = "This subject appears in the following books";
		}

		//print_r($data['elements']['fields']);
		
		$this->form_validation->set_rules('name', 'Name', 'required');
    // Display data
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/avlibheader', $data);
			if(is_staff())
				$this->load->view('settings/menubar', $data);
			else
				$this->load->view('books/menubar_clients', $data);
			$this->load->view('lists/view', $data);
			$this->load->view('templates/tableview', $data);
			$this->load->view('templates/avlibfooter', $data);
		}
    // Save
		else
		{
      $this->lists_model->save($list, $this->input->post('id'), $this->input->post('name'), $this->input->post('notes'));
			$this->load->view('templates/avlibheader', $data);
			$this->load->view('templates/saved', $data);
			$this->load->view('templates/avlibfooter', $data);
		}
  }

	public function delete($list, $id)
	{
		if( ! is_admin())
		{
			$this->login();
			return;
		}
		$this->lists_model->delete($list, $id);
		$data['redirect'] = "lists/search";

		$this->load->view('templates/avlibheader', $data);
		$this->load->view('templates/saved', $data);
		$this->load->view('templates/avlibfooter', $data);
	}
  
  public function search()
  {
    if( ! is_admin())
		{
			$this->login();
			return;
		}

		$data['title'] = "Search list";
		$data['selectedMenu'] = 'Settings';
		$data['focusfield'] = "name";
		
		$list = $this->input->post("list");
		$name = $this->input->post("name");
		$notes = $this->input->post("notes");
		
		$data['elements']['fields']['list']['name'] = 'list';
		$data['elements']['fields']['list']['label'] = 'List';
		$data['elements']['fields']['list']['type'] = 'select';
		$data['elements']['fields']['list']['options'] = array("features"=>'Features', "keywords"=>'Keywords', "languages"=>'Languages', "physicaldetails"=>'Physical Details', "topicalterms"=>'Topical Terms');
		$data['elements']['fields']['list']['ljtable'] = 'nill';
		$data['elements']['fields']['list']['value'] = $this->input->post("list");
		$data['elements']['fields']['name']['name'] = 'name';
		$data['elements']['fields']['name']['label'] = 'Name';
		$data['elements']['fields']['name']['value'] = $this->input->post("name");
		$data['elements']['fields']['notes']['label'] = 'Notes';
		$data['elements']['fields']['notes']['name'] = 'notes';
		$data['elements']['fields']['notes']['value'] = $this->input->post("notes");

		// Get books
		$data['elements']['items'] = $this->lists_model->search($data['elements']['fields']);
		$data['elements']['resultsize'] = count($data['elements']['items']);
		//print_r($data['elements']['items']);
		$data['elements']['labels'] = array($this->lists_model->singular($list) => $data['elements']['fields']['list']['options'][$list], "notes" => "Notes", "bookcount" => "Occurences");

		$this->load->view('templates/avlibheader', $data);
		$this->load->view('settings/menubar', $data);
		$this->load->view('lists/search', $data);
		//print_r($data['elements']['items']);
		$this->load->view('templates/tableview', $data);
		$this->load->view('templates/avlibfooter', $data);
  }
}