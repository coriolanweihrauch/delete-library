<?php
class help extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	public function index()
	{
		$data=array();
		$this->load->view('templates/avlibheader', $data);
		$this->load->view('help/helpscript', $data);
		$this->load->view('help/help', $data);
		$this->load->view('templates/avlibfooter', $data);
	}
	
	public function staff()
	{
		$data=array();
		$this->load->view('templates/avlibheader', $data);
		$this->load->view('help/helpscript', $data);
		$this->load->view('help/help-staff', $data);
		$this->load->view('templates/avlibfooter', $data);
	}
}
?>