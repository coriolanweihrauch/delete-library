<?php
if(isset($list))
{
  print("<h3>".$listname."</h3>");
	print(form_open('/settings/lists/'.$listname));  
  foreach($list as $i => $r)
  {
    print(form_label($r['id'].": "));
    print(form_input(array("name" => "name[".$r['id']."]", "value" => $r['name'], "size" => "80")));
		print("<a href=\"javascript:confirmDeleteListItem(".$r['id'].")\"><img src=\"/library/assets/img/cross.png\"></a><br/>\n");
  }
  print(form_label("Add new item: "));
  print(form_input(array("name" => "new"))."<br/>\n");
  print(form_submit(null, "Save"));
}
?>
<script>
function confirmDeleteListItem(id)
{
	listname = "<?php print($listname); ?>";
	bConfirm = confirm("Do you really want to delete this "+listname+"?\n\nThis will delete the entry from all menus but also --ALL-- it's appearences throughout the database!\n\nBe sure you understand what you're doing!");
	if(bConfirm)
		//alert(window.location+"/delete/"+id);
		window.location = window.location+"/delete/"+id
}
</script>