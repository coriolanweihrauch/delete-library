<?php
print("<h3>".$listtitle."</h3>");

if($listtitle == "Authors")
{
	$strAjax = "\"/library/authors/ajaxsearchauthor/\"";
	//$strData = 
}
else
{
	$strAjax = "\"/library/books/ajaxlookupcomplete/\"";
}


$fields['left'] = array(
	'name'        => "left",
	'type'				=> '',
	'id'          => "left",
	'size'        => '50',
	'label'				=> "Left ".$listtitle,
	'script'			=> "$(\"#left\").autocomplete({
											source: function(request, response) {
												$.ajax({
													url: ".$strAjax.",
													data: { term: $(\"#left\").val(), name: \"".$listname."\"},
													dataType: \"json\",
													type: \"POST\",
													success: function(data){
														response(data);
													}
												});
											},
											minLength: 1,
											select: function( event, ui ) {
												$(\"#mergeleftname\").html(ui.item.label)
												//alert(\"label: \"+ ui.item.label);
											}
										});",
);

$fields['right'] = array(
	'name'        => "right",
	'type'				=> '',
	'id'          => "right",
	'size'        => '50',
	'label'				=> "Right ".$listtitle,
	'script'			=> "$(\"#right\").autocomplete({
											source: function(request, response) {
												$.ajax({
													url: ".$strAjax.",
													data: { term: $(\"#right\").val(), name: \"".$listname."\"},
													dataType: \"json\",
													type: \"POST\",
													success: function(data){
														response(data);
													}
												});
											},
											minLength: 1,
											select: function( event, ui ) {
												$(\"#mergerightname\").html(ui.item.label)
												//alert(\"label: \"+ ui.item.label);
											}
										});",
);


print(form_open('settings/merge/'.$listname));
//generate_form_table($fields,array("name"=>"submit","label"=>"Merge"));

print("<table class=\"formtable\">\n");
print("  <tr>\n");
foreach($fields as $f)
{
	print("    <td>");
	generate_form_field_label($f);
	print("</td>\n");
}
// Spacer for submit button
print("    <td/>");
print("  </tr>\n");
print("  <tr>\n");
foreach($fields as $f)
{
	print("    <td>");
	generate_form_field($f);
	print("</td>\n");
}
print("    <td>");
print(form_submit("submit", "save"));
print("</td>\n");
print("  </tr>\n");
print("<tr><td id=\"mergeleftname\"></td><td id=\"mergerightname\"></td></tr>");
print("</table>\n");

foreach ($fields as $f)
	if(array_key_exists('script', $f))
		print("<script>\n".$f['script']."</script>\n");
?><br/><span class="note">Note: Right will be merged on left ; Right will then be deleted</span>