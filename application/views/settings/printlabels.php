<?php
echo form_open('settings/printlabels/');

print($data)

?>
<h3>Print labels: </h3>
<div id="tabs">
	<ul>
		<li><a href="#tabs-date">Date range</a></li>
		<li><a href="#tabs-number">Number range</a></li>
		<li><a href="#tabs-list">Individual numbers</a></li>
	</ul>
	<div id="tabs-date">
<?php
  echo form_open('settings/printlabels#tabs-date');
  print(form_hidden('type', 'date'));
  generate_form_field_br($elements['fields']['format']);
  generate_form_field_br($elements['fields']['date_from']);
  print("<script>\n".$elements['fields']['date_from']['script']."</script>\n");
  generate_form_field_br($elements['fields']['date_to']);
  print("<script>\n".$elements['fields']['date_to']['script']."</script>\n");
  //print(form_submit('submit_time', 'View'));
  print(form_submit('submit', 'View'));
  print(" &nbsp; ");
  print(form_submit('submit', 'Export'));
  print(form_close());
?>
	</div>
	<div id="tabs-number">
<?php
  echo form_open('settings/printlabels#tabs-number');
  print(form_hidden('type', 'number'));
  generate_form_field_br($elements['fields']['format']);
  generate_form_field_br($elements['fields']['number_from']);
  generate_form_field_br($elements['fields']['number_to']);
  print(form_submit('submit', 'View'));
  print(" &nbsp; ");
  print(form_submit('submit', 'Export'));
  print(form_close());
?>
	</div>
	<div id="tabs-list">
<?php
  echo form_open('settings/printlabels#tabs-list');
  print(form_hidden('type', 'list'));
  generate_form_field_br($elements['fields']['format']);
  generate_form_field_br($elements['fields']['list_from']);
  print(form_submit('submit', 'View'));
  print(" &nbsp; ");
  print(form_submit('submit', 'Export'));
  print(form_close());
?>
	</div>
</div>
<script>
	$(function() {
		$( "#tabs" ).tabs();
	});

<?php
if(isset($focustab))
{
?>
var $tabs = $('#tabs').tabs();
var tabid = "#tabs-<?php print($focustab); ?>";
console.log(tabid);
$tabs.tabs('select', tabid);
<?php
}
?>
</script>