<div class="author-quickadd">
  <script>
	$(function() {
		$( "#personid" ).autocomplete({
			source: function(request, response) {
				$.ajax({ url: "/library/authors/ajaxsearchauthor/",
					data: { term: $("#personid").val()},
					dataType: "json",
					type: "POST",
					success: function(data){
							response(data);
					}
				});
			},
			minLength: 1,
		});
	});

	function deletePersonFromBook(bookid, personid, roleid)
	{
		bConfirm = confirm("are you sure you want to delete this author ?");
		if(bConfirm)
		{
			$.post('/library/authors/ajaxremoverole/',
						{ bookid: bookid, personid: personid, roleid: roleid },
						 function(data){
							loadPersons(bookid);
						}
			);
		}
	}

	function addPersonToBook(bookid, personid, roleid)
	{
		$.post('/library/authors/ajaxaddrole/',
				  { bookid: bookid, personid: personid, roleid: roleid },
					 function(data){
						$("#personid").val("");
						loadPersons(bookid);
 					}
		);
	}

	function loadPersons(bookid)
	{
		$.post('/library/authors/ajaxlistroles/',
				  { bookid: bookid },
					 function(data){

					// Print labels row
					s = "<tr>";
					$.each( data.labels, function(i, n){
						s += "<th>"+n+"</th>";
					});
					s += "</tr>\n";
			
					// Print item row(s)
					for(var item in data.items) {
						 var i= data.items[item];
					
						s += "<tr>";	
						for(lable in data.labels){
							s += "<td>"+i[lable]+"</td>";
						}
						s += "</tr>\n";
					
					}

					// Put them on the screen
					$("#quickaddtable").html(s);
 					},
 					"json"
		);
	}
</script>
  <h3>Authors & other people</h3>
<?php

//print_r($authors['elements']['fields']);
//print(form_open(""));
generate_form_table_no_submit($authors['quickaddform']['fields']);
//print(form_close());

//$this->load->view('templates/tableview', $authors['tableview']);

//print_r($authors['tableview']);

//$this->load->view('templates/tableview', $authors['tableview']);

print("<table id=\"lookupval_author\" class=\"tableview\"></table>");
print("<script>loadPersons(".$elements['fields']['id']['value'].");</script>")
?>
</div>