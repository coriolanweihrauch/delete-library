<div id="submenubar">
  <span class="menuitem"><a href="/library/books/search/">Search</a></span>
  <span class="menuitem"><a href="/library/books/search_advanced/">Adv. Search</a></span>
  <span class="menuitem"><a href="/library/books/edit/">Add Book</a></span>
  <span class="menuitem"><a href="/library/authors/edit/">Add Author</a></span>
  <span class="menuitem"><a href="/library/books/overdue/">List: Overdue</a></span>
  <span class="menuitem"><a href="/library/books/catalogue/entrydate">List: New</a></span>
  <span class="menuitem"><a href="/library/books/catalogue/borrowfrequency">List: Scores</a></span>
  <!--<span class="menuitem"><a href="/library/books/catalogue/notborrowed">List: Never borrowed</a></span>-->
  <span class="menuitem"><a href="/library/books/checkoutcount">List: Checkouts</a></span>
</div>
<br/>