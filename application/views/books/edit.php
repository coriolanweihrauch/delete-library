<?php echo validation_errors(); ?>

<?php
//print_r($elements); 
//print_r($loaded_book);
//print("<h3>".$title."</h3>");
if($elements['id_prev'])
	$link_prev = "<a href=\"/library/books/edit/".$elements['id_prev']."\">&lt;&lt;</a> ";
if($elements['id_next'])
	$link_next = " <a href=\"/library/books/edit/".$elements['id_next']."\">&gt;&gt;</a>";
print("<h3>".$link_prev.$title.$link_next."</h3>");
//print("<h3><a href=\"/library/books/edit/".$elements['id_prev']."\">&lt;&lt;</a> ".$title." <a href=\"/library/books/edit/".$elements['id_next']."\">&gt;&gt;</a></h3>");
?>



<?php
// ###  Make this link neater  ###
//echo form_open('books/edit/'.$elements['id']['value']);
if(array_key_exists("id", $elements['fields']))
	echo form_open('books/edit/'.$elements['fields']['id']['value'], array("id" => "bookeditform"));
else
	echo form_open('books/edit', array("id" => "bookeditform"));

generate_form_table_vertical($elements['fields']);
?>
<p>  <input type="button" name="delete" class="delete" value="Delete book" onClick="javascript:deleteBook('<?php print($elements['fields']['id']['value']); ?>');"/> </p>

<p>  <input type="button" name="duplicate" class="duplicate" value="Duplicate this book" onClick="javascript:duplicateBook('<?php print($elements['fields']['id']['value']); ?>');"/> </p>

</form>
<br/>
<script type="text/javascript" language="javascript">
  function deleteBook(bookid)
  {
    bConfirm = confirm('Are you sure you want to delete this book permanently ?\nThis will delete the book and ALL references to it !');
    if(bConfirm)
      window.location.href="/library/books/delete/"+bookid
  }

  function duplicateBook(bookid)
  {
    bConfirm = confirm('Are you sure you want to duplicate this book ?');
    if(bConfirm)
      window.location.href="/library/books/duplicate/"+bookid
  }
</script>