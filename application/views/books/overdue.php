<br/>
<div id="remindermails"><?php
if(count($sendlist) > 1)
  print("<input type=\"button\" value=\" Send ".count($sendlist)." email reminders\" name=\"remindermailsbutton\" onClick=\"javascript:sendmails();\"/><br/>");
elseif(count($sendlist) == 1)
  print("<input type=\"button\" value=\" Send ".count($sendlist)." email reminder\" name=\"remindermailsbutton\" onClick=\"javascript:sendmails();\"/><br/>");
else
  print("<input type=\"button\" value=\" No reminders to send\" name=\"remindermailsbutton\" onClick=\"javascript:alert('no reminders to send');\"/><br/>");
if($reminderemail['count_noemail'] > 1)
  print("<span class=\"note\">Note: ".$reminderemail['count_noemail']." overdues have no email ID</span/>");
elseif($reminderemail['count_noemail'] == 1)
  print("<span class=\"note\">Note: ".$reminderemail['count_noemail']." overdue has no email ID</span/>");
?></div>
<script language="javascript">
var errorcount = 0;
var successcount = 0;
<?php
//print_r($sendlist);
print("var list = new Array(".implode(",", $sendlist).");\n");
?>

function sendmails()
{
  // sendmode
  // 0 = will send
  // 1 = will make a trial send
  trialmode = 0;
  
  if(list.length > 0)
  {
    $("#remindermails").html("<span id=\"img_remindermails\"><img src=\"/library/assets/img/loading.gif\" /></span> <span id=\"msg_remindermails\"></span><div id=\"suc_remindermails\">0 success(es)</div><div id=\"err_remindermails\">0 error(s)</div>");
    for(var i=0; i<list.length; i++)
    {
      
      
      $.post("/library/books/overduesendreminder/"+list[i]+"/"+trialmode,
      { transactionid: list[i], trial: trialmode},
        function(data){
        //alert(JSON.stringify(data));
        if(data['error'] != null)
        {
          errorcount++;
          update_count();
        }
        else
        {
          successcount++;
          update_count();
        }
      },
      "json"
      );
    }
    $("#msg_remindermails").html("Sending "+list.length+" messages ... please wait");
  }
  else
    alert("no messages to send");
}

function update_count()
{
  $("#err_remindermails").html(errorcount+" error(s)");
  $("#suc_remindermails").html(successcount+" success(es)");
  if((errorcount+successcount) == list.length)
  {
    //alert(list.length+" vs. "+(errorcount+successcount));
    $("#img_remindermails").html("<img src=\"/library/assets/img/tick.png\" />");
    $("#msg_remindermails").html("Finished sending");
  }
}
</script>