<table id="booknumber">
  <tr>
    <td><p class="viewbooknumber" style="border:1px solid black; padding: 5px;"><?php print($elements['fields']['dewey_decimal_classification']['value']."<br/>".$elements['fields']['book_number']['value']."<br/>".$elements['fields']['serial_number']['value']); ?></td>
    <td style="padding: 10px">Your book's location:<br/><?php
			if(isset($elements['fields']['client']))
				print("Book is NOT available");
			else
				print("Book is available");
    ?></td>
  </tr>
</table>

<p class="viewmainauthor"><?php
if(is_array($elements['fields']['author']['value']) && count($elements['fields']['author']['value']) > 0)
foreach($elements['fields']['author']['value'] as $i => $v)
{
  if($i == 0)
    print("<b><a href=\"/library/authors/view/".$elements['fields']['author']['value'][$i]['id']."\">".$elements['fields']['author']['value'][$i]['value']."</a></b> (".$elements['fields']['author']['value'][$i]['role'].")<br/>");
  else
		print("<b><a href=\"/library/authors/view/".$elements['fields']['author']['value'][$i]['id']."\">".$elements['fields']['author']['value'][$i]['value']."</a></b> (".$elements['fields']['author']['value'][$i]['role'].")<br/>");
	  //print($elements['fields']['author']['value'][$i]."<br/>");
		//print_r($elements['fields']['author']['value'][$i]);
}
?></p>


<p class="viewtitle"><?php
//print("<pre>");
//print_r($elements['fields']);
//print("</pre>");
    print("<b>".$elements['fields']['title']['value']."</b>");
    if($elements['fields']['part_x']['value'] != "")
      print(" - <b>".$elements['fields']['part_x']['value']."</b><br/>");
    else
      print("<br/>\n");
    if($elements['fields']['remainder_of_title']['value'] != "" && $elements['fields']['statement_of_responsability']['value'] != "")
      print("<i>".$elements['fields']['remainder_of_title']['value'] . "</i> / " . $elements['fields']['statement_of_responsability']['value']);
    else
      print("<i>".$elements['fields']['remainder_of_title']['value'] . "</i>". $elements['fields']['statement_of_responsability']['value']);
    ?></p>


<table>
  <tr>
    <td><?php
    print($elements['fields']['publication_place']['value']
    .": ".$elements['fields']['publication_publisher']['value']
    .", ".$elements['fields']['publication_year']['value']
    .". - ".$elements['fields']['edition_statement']['value']);
    ?></td>
  </tr>
  <tr>
    <td><?php
    print($elements['fields']['collation']['value']);
		if(is_array($elements['fields']['feature']['value']) && count($elements['fields']['feature']['value']) > 0)
		{
			print(" - ");
			foreach($elements['fields']['feature']['value'] as $i => $v)
			{
				//if($i == 0)
				//	print("<a href=\"/library/lists/view/features/".$elements['fields']['feature']['value'][$i]['id']."\">".$elements['fields']['feature']['value'][$i]['value']."</a> ");
				//else
				//	print("<a href=\"/library/lists/view/features/".$elements['fields']['feature']['value'][$i]['id']."\">".$elements['fields']['feature']['value'][$i]['value']."</a> ");
				echo $elements['fields']['feature']['value'][$i]['value']." ";
			}
		}

    ?></td>
  </tr>
  <?php
  if($elements['fields']['series_name']['value'] != "")
    print("<tr><td>".$elements['fields']['series_name']['value']." - ".$elements['fields']['series_part_no']['value'].": ".$elements['fields']['series_part_name']['value']."</tr></td>");
  ?>
  <tr><td><u>Varying form of title</u>: <?php print($elements['fields']['parallel_title']['value']); ?></td></tr>
  <tr><td><u>Original title</u>: <?php print($elements['fields']['original_title']['value']); ?></td></tr>
  <tr><td><u>ISBN</u>: <?php print($elements['fields']['ISBN']['value']); ?></td></tr>
  <tr><td><u>Language</u>: <?php print($elements['fields']['language']['value']); ?></td></tr>
	  <tr><td><u>Subjects</u> / <u>Topical terms</u>:<br/><ul><?php
	//foreach( as $i => $v)
		//$arrSubject[] = "<a href=\"/library/lists/view/topicalterms/".$v['id']."\">".$v['value']."</a>";
	
	//print_r($arrSubject);
	
	if(is_array($elements['fields']['topicalterm']['value']) && count($elements['fields']['topicalterm']['value']) > 0)
	{
		foreach($elements['fields']['topicalterm']['value'] as $i => $v)
			print("<li><a href=\"/library/lists/view/topicalterms/".$v['id']."\">".$v['value']."</a></li>");
	}

	else
		print("");
	?></ul></td></tr>
  <tr><td><u>Date added</u>: <?php print(date("d/m/Y",strtotime($elements['fields']['date_created']['value']))); ?></td></tr>
  <tr><td><i></i></td></tr>
</table>

<p class="viewnotes"><i><?php print($elements['fields']['notes']['value']); ?></i></p>