<?php
if(isset($elements) && array_key_exists("clientid", $elements))
{
  print("<h3>User Information</h3>\n");
  print("<div class=\"client_summary\">\n");
  print("<div class=\"client_name\"><a href=\"/library/clients/edit/".$elements['client']['id']."\">".$elements['client']['name']." ".$elements['client']['surname']."</a></div>\n");
  print("<div class=\"client_address\">".$elements['client']['address']."</div>\n");
  print("<div class=\"client_contact\">".str_replace("\n", ", ", $elements['client']['telephone']).", ".$elements['client']['email']."</div>\n");
  print("<div class=\"client_permissions\">Can borrow ".$elements['client']['number_of_books']." books for ".$elements['client']['number_of_days']." days</div>\n");
  print("<div class=\"client_registration\">Registered since ".$elements['client']['date_registration']."</div>\n");
  print("</div><br/>\n");
}
?>
