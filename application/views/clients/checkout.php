<?php echo validation_errors(); ?>

<?php
print("<h3>".$title."</h3>");
// ###  Make this link neater  ###
//echo form_open('books/edit/'.$elements['id']['value']);
print(form_open('books/checkout'));
print(form_label("User ID")."<br/>\n");

$arrClientField = array('name' => 'clientid', 'id' => 'clientidfield', 'size' => '30');
if(isset($elements) && array_key_exists("clientid", $elements))
{
  $arrClientField['value'] = $elements['clientid'];
}
echo form_input($arrClientField);
echo form_submit('submit', 'Select');
if(isset($elements) && array_key_exists("clientid", $elements))
  print(form_button('resetclientid', 'Clear', "onClick='location=window.location'"));
echo form_close();
?>
<div id="clientnamediv"></div>
<script>
	$(function() {
		$( "#clientidfield" ).autocomplete({
			source: function(request, response) {
				$.ajax({ url: "/library/clients/ajaxsearchclient/",
					data: { term: $("#clientidfield").val()},
					dataType: "json",
					type: "POST",
					success: function(data){
							response(data);
					}
				});
			},
			minLength: 1,
			select: function( event, ui ) {
				$("#clientnamediv").html(ui.item.label)
				//alert("label: "+ ui.item.label);
			}
		});
	});

</script>
<br/>