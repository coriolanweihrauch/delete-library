<?php echo validation_errors(); ?>

<?php
//print_r($elements); 
//print_r($loaded_client);

//print("<h3>".$title."</h3>");
if($elements['id_prev'])
	$link_prev = "<a href=\"/library/clients/edit/".$elements['id_prev']."\">&lt;&lt;</a> ";
if($elements['id_next'])
	$link_next = " <a href=\"/library/clients/edit/".$elements['id_next']."\">&gt;&gt;</a>";
print("<h3>".$link_prev.$title.$link_next."</h3>");
?>

<?php
// ###  Make this link neater  ###
//echo form_open('books/edit/'.$elements['id']['value']);
if(array_key_exists("id", $elements['fields']))
	echo form_open('clients/edit/'.$elements['fields']['id']['value'], array("id"=>"client_form", "name"=>"client_form"));
else
	echo form_open('clients/edit', array("id"=>"client_form", "name"=>"client_form"));
?>
	
<?php
foreach ($elements['fields'] as $f):
	generate_form_field_br($f);
	if(array_key_exists('script', $f))
		print("<script>\n".$f['script']."</script>\n");
endforeach;

//foreach ($elements as $f):
//	if(array_key_exists('script', $f))
//		$s[] = $f['script'];
//endforeach;
?>
  
	<input type="submit" value="Save" />
  
<p>  <input type="button" name="delete" class="delete" value="Delete user" onClick="javascript:deleteClient('<?php print($elements['fields']['id']['value']); ?>');"/> </p>
</form>

<?php

if(isset($s) && is_array($s))
	print("<script>\n".implode("\n",$s)."</script>\n");
	//print("hello");

?>
<br/>
<script type="text/javascript" language="javascript">
  function deleteClient(clientid)
  {
    bConfirm = confirm('Are you sure you want to delete this user permanently ?\nThis will delete the user and ALL references to him');
    if(bConfirm)
      window.location.href="/library/clients/delete/"+clientid
  }

	function asynctosuggest()
	{
    search = new Object();
    search.aurovillename = $("#client_aurovillename").val();
    search.name = $("#client_name").val();
    search.surname = $("#client_surname").val();
    search.telephone = $("#client_telephone").val();
    search.email = $("#client_email").val();
    search.asynctoid = $("#client_asynctoid").val();
    //alert(search.name+"-"+search.surname+"-"+search.landline+"-"+search.cellphone+"-"+search.email+"-".search.asynctoid);

		$.post('/asyncto/index.php/api/ajaxsearch/',
				  { search: search, format: "json"},
					 function(data){
						//alert(data);
            //oAsyncto = new Array();

            strHTML = "";
            strHTML += "<div id=\"asynctosuggestion\">\n";
            strHTML += "<div id=\"asynctoclosebutton\"><a href=\"javascript:asynctosuggest_close();\"><img src=\"/library/assets/img/CloseButtonNew.png\" /></a></div>\n";
            strHTML += "<div id=\"asynctodatadiv\">";
            $.each(data['data'],
                   function(key,value)
                   {
                    arrAsynctoFields = new Array("aurovillename", "name", "surname", "telephone", "email", "asynctoid");
                    strHTML += "<div class=\"asynctodata\">";
                    for(var i=0; i < arrAsynctoFields.length; i++)
                    //alert(arrAsynctoFields[i]);
                      strHTML += "<div class=\"asynctodata-"+arrAsynctoFields[i]+"\">"+arrAsynctoFields[i]+": "+data['data'][key][arrAsynctoFields[i]]+"</div>";
                    strHTML += "</div>\n";
                    strHTML += "<div class=\"asynctotitle\"><a href=\"javascript:asynctobindid('"+data['data'][key]['asynctoid']+"');\">Bind this asyncto user</a></div>\n";
                    });
            strHTML += "</div>";
            strHTML += "</div>";
            //alert(strHTML);

            strlog = "";
            $.each(data['log'],
                   function(key,value)
                   {
                    $.each(value, function(k,v){strlog += "["+k+"] "+v+"\n";});
                    });
            //alert(strlog);

//http://buildinternet.com/2009/08/lights-out-dimmingcovering-background-content-with-jquery/
            $('#content').append(strHTML);
            $("#asynctocontainer").css("height", $(document).height());
            $("#asynctocontainer").click(function(){asynctosuggest_close();});
            $("#asynctocontainer").fadeIn();
            $("#asynctosuggestion").fadeIn();

//$('a[rel*=facebox]').facebox();

 					},
          "json"
		);
	}

  function asynctosuggest_close()
  {
    $("#asynctocontainer").fadeOut();
    $("#asynctosuggestion").fadeOut();
  }

  function asynctobindid(strID)
  {
    asynctosuggest_close();
    $("#client_asynctoid").val(strID);
    $("#client_form").submit();
  }

  function asynctosync()
  {
    asynctoid = $("#client_asynctoid").val();
    
    if(!asynctoid || asynctoid == "")
    {
      alert("Please map this client to an asyncto ID before trying to sync.")
      return 0;
    }

		$.post('/asyncto/index.php/api/ajaxsync/',
				  { asynctoid: asynctoid},
					 function(data){

          $("#client_aurovillename").val(data['data'][0]['aurovillename']);
          $("#client_name").val(data['data'][0]['name']);
          $("#client_surname").val(data['data'][0]['surname']);
          $("#client_address").val(data['data'][0]['address']);
          // Separate telephone numbers by newline
          var telephone = String(data['data'][0]['telephone']);
          telephone = telephone.replace(",","\n");
          $("#client_telephone").val(telephone);
          // Take only the first email ID
          if($.isArray(data['data'][0]['email']))
            $("#client_email").val(data['data'][0]['email'][0]);
          else
            $("#client_email").val(data['data'][0]['email']);
          alert('Synchronization complete - verify data and save');
           }
          );
  }
</script>