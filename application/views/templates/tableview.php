<?php
//print_r($elements['labels']);
//print("<br/>");

if(array_key_exists('tabletitle', $elements))
    print("<h3>".$elements['tabletitle']."</h3>");


 if(array_key_exists('resultsize', $elements) && array_key_exists("items", $elements) && count($elements['items']) > 0)
 {
  if($elements['resultsize'] != 1)
   print("<div id=\"resultcount\">".$elements['resultsize']." results</div>");
  else
   print("<div id=\"resultcount\">".$elements['resultsize']." result</div>");
  unset($elements['resultsize']);
 }

if(isset($elements) && array_key_exists('labels', $elements) && array_key_exists("items", $elements))
{
    // Sort labels as per default order
    //$elements['labels'] = sort_labels($elements['labels']);

    // Remove unused labels:
    if(array_key_exists("remainder_of_title", $elements['labels']))
     unset($elements['labels']['remainder_of_title']);
    if(array_key_exists("parallel_title", $elements['labels']))
     unset($elements['labels']['parallel_title']);
    if(array_key_exists("original_title", $elements['labels']))
     unset($elements['labels']['original_title']);


    // Display pre-processing
    //unset($elements['labels']['id']);
    if(count($elements['items']) > 0)
        foreach($elements['items'] as $i => $b)
        {
            // Pad barcode with 000s
            if(array_key_exists('serial_number', $elements['items'][$i]))
             for($c = strlen($elements['items'][$i]['serial_number']); $c < 6; $c++)
              $elements['items'][$i]['serial_number'] = "0".$elements['items'][$i]['serial_number'];


            // Highlight deleted books
            if(array_key_exists('serial_number', $elements['items'][$i]) && array_key_exists('delete_flag', $elements['items'][$i]) && $elements['items'][$i]['delete_flag'] == 1)
            {
             $elements['items'][$i]['serial_number'] = "<span class=\"delete\">".$elements['items'][$i]['serial_number']."</span>";
            }

            // Make link to client
            if(array_key_exists('client', $elements['items'][$i]) && array_key_exists('clientid', $elements['items'][$i]))
                $elements['items'][$i]['client'] = "<a href=\"/library/clients/edit/".$b['clientid']."\" class=\"clientlink\">".$b['client']."</a>";
            elseif(array_key_exists('name', $elements['items'][$i]) && array_key_exists('id', $elements['items'][$i]))
                $elements['items'][$i]['name'] = "<a href=\"/library/clients/edit/".$b['id']."\" class=\"clientlink\">".$b['name']."</a>";
            // Make link to book
            if(array_key_exists('title', $elements['items'][$i]) && array_key_exists('id', $elements['items'][$i]))
                $elements['items'][$i]['title'] = "<a href=\"/library/books/view/".$b['id']."\" class=\"booklink\">".$b['title']."</a>";
            elseif(array_key_exists('title', $elements['items'][$i]) && array_key_exists('bookid', $elements['items'][$i]))
                $elements['items'][$i]['title'] = "<a href=\"/library/books/view/".$b['bookid']."\" class=\"booklink\">".$b['title']."</a>";
            // Highlight pending return
            if(array_key_exists('date_return', $elements['items'][$i]) && $elements['items'][$i]['date_return'] == "0000-00-00")
                $elements['items'][$i]['date_return'] = "<span id=\"bookout\"></span>";
            // Delete id from labels / fields
            //if(array_key_exists('id', $elements['items'][$i]))
            //    unset($elements['items'][$i]['id']);

            // Add part number
            if(array_key_exists("part_x", $elements['items'][$i]) && $elements['items'][$i]['part_x'] != "" && $elements['items'][$i]['part_x'] != "0")
             $elements['items'][$i]['title'] .= " - ".$elements['items'][$i]['part_x'];

            $altTitles = array();
            // Add varying forms of title
            if(array_key_exists("remainder_of_title", $elements['items'][$i]) && $elements['items'][$i]['remainder_of_title'] != "" && $elements['items'][$i]['remainder_of_title'] != "0")
             $altTitles[] = $elements['items'][$i]['remainder_of_title'];
            if(array_key_exists("parallel_title", $elements['items'][$i]) && $elements['items'][$i]['parallel_title'] != "" && $elements['items'][$i]['parallel_title'] != "0")
             $altTitles[] = $elements['items'][$i]['parallel_title'];
            if(array_key_exists("original_title", $elements['items'][$i]) && $elements['items'][$i]['original_title'] != "" && $elements['items'][$i]['original_title'] != "0")
             $altTitles[] = $elements['items'][$i]['original_title'];
            if(count($altTitles) > 0)
             $elements['items'][$i]['title'] .= "<br>".implode('<br/>', $altTitles);


            // Make link to author
            if(array_key_exists('author_name', $elements['items'][$i]) && array_key_exists('id', $elements['items'][$i]))
            {
             if(strpos($elements['items'][$i]['author_name'], ";") !== false && strpos($elements['items'][$i]['author_id'], ";") !== false)
             {
              $arrName = explode(";", $elements['items'][$i]['author_name']);
              $arrID = explode(";", $elements['items'][$i]['author_id']);
              $arrAuthor = array();
              foreach($arrID as $index => $id)
              {
               $arrAuthor[] = "<a href=\"/library/authors/view/".$arrID[$index]."\" class=\"authorlink\">".$arrName[$index]."</a>";
              }
              if(count($arrAuthor))
               $elements['items'][$i]['author_name'] = implode(" - ", $arrAuthor);
             }
             else
              $elements['items'][$i]['author_name'] = "<a href=\"/library/authors/view/".$b['author_id']."\" class=\"authorlink\">".$b['author_name']."</a>";
            }

            // Make link to list elements
            if(array_key_exists('feature', $elements['items'][$i]) && array_key_exists('featureid', $elements['items'][$i]))
             $elements['items'][$i]['feature'] = "<a href=\"/library/lists/view/features/".$elements['items'][$i]['featureid']."\" class=\"listlink\">".$elements['items'][$i]['feature']."</a>";
            if(array_key_exists('keyword', $elements['items'][$i]) && array_key_exists('keywordid', $elements['items'][$i]))
             $elements['items'][$i]['keyword'] = "<a href=\"/library/lists/view/keywords/".$elements['items'][$i]['keywordid']."\" class=\"listlink\">".$elements['items'][$i]['keyword']."</a>";
            if(array_key_exists('language', $elements['items'][$i]) && array_key_exists('languageid', $elements['items'][$i]))
             $elements['items'][$i]['language'] = "<a href=\"/library/lists/view/languages/".$elements['items'][$i]['languageid']."\" class=\"listlink\">".$elements['items'][$i]['language']."</a>";
            if(array_key_exists('physicaldetail', $elements['items'][$i]) && array_key_exists('physicaldetailid', $elements['items'][$i]))
             $elements['items'][$i]['physicaldetail'] = "<a href=\"/library/lists/view/physicaldetails/".$elements['items'][$i]['physicaldetailid']."\" class=\"listlink\">".$elements['items'][$i]['physicaldetail']."</a>";
            if(array_key_exists('topicalterm', $elements['items'][$i]) && array_key_exists('topicaltermid', $elements['items'][$i]))
             $elements['items'][$i]['topicalterm'] = "<a href=\"/library/lists/view/topicalterms/".$elements['items'][$i]['topicaltermid']."\" class=\"listlink\">".$elements['items'][$i]['topicalterm']."</a>";



            //elseif(array_key_exists('name', $elements['items'][$i]) && array_key_exists('id', $elements['items'][$i]))
            //    $elements['items'][$i]['name'] = "<a href=\"/library/clients/edit/".$b['id']."\" class=\"clientlink\">".$b['name']."</a>";

            // Do not display reminder date "0000-00-00"
            if(array_key_exists('date_reminder', $elements['items'][$i]) && $elements['items'][$i]['date_reminder'] == "0000-00-00")
                    $elements['items'][$i]['date_reminder'] = "";

            // Make reminder link //
           if(array_key_exists('overdueid', $elements['items'][$i]))
           {            
/*            if(array_key_exists('email', $elements['items'][$i]) && $elements['items'][$i]['email'] != "")
            {
                // Get reminder level (1,2)
                if(array_key_exists('date_due', $elements['items'][$i]) && $elements['items'][$i]['date_due'] != "0000-00-00")
                {
                    if(strtotime(strip_tags($elements['items'][$i]['date_due'])) > time())
                        $strMsg = "Send early notice";
                    elseif(array_key_exists('date_reminder', $elements['items'][$i]) && isset($elements['items'][$i]['date_reminder']) && $elements['items'][$i]['date_reminder'] != "" && $elements['items'][$i]['date_reminder'] != "0000-00-00")
                        $strMsg = "Send 2nd reminder";
                    else
                        $strMsg = "Send 1st reminder";
                }
                else
                {
                    $strMsg = "Send reminder";
                }
                // Display reminder link
                $id = $elements['items'][$i]['overdueid'];
                $elements['items'][$i]['overdueid'] = "<div id =\"".$elements['items'][$i]['overdueid']."\"><a href=\"javascript:ajaxreminder(".$id.");\" id=\"a_".$id."\" class=\"reminderlink\">".$strMsg."</a> <img src=\"/library/assets/img/spacer.gif\" id=\"img_".$id."\" width=\"16\" height=\"16\"/></div>";
            }
            else
                $elements['items'][$i]['overdueid'] = "No email ID";
*/            
/*            if(array_key_exists('email', $elements['items'][$i]) && $elements['items'][$i]['email'] != "")
            {
             if(array_key_exists("reminder_level", $elements['items'][$i]))
             {
              if($elements['items'][$i]['reminder_level'] == 0)
              {
               if(strtotime($elements['items'][$i]['date_due']."+3 days", time()) > time())
                $elements['items'][$i]['overdueid'] = "Too early for reminder";
               else
                $elements['items'][$i]['overdueid'] = "Sending 1st reminder";
              }
              elseif($elements['items'][$i]['reminder_level'] == 1)
              {
               if(strtotime($elements['items'][$i]['date_reminder']."+10 days", time()) > time())
                $elements['items'][$i]['overdueid'] = "Too early for 2nd reminder";
               else
                $elements['items'][$i]['overdueid'] = "Sending 2nd reminder";
               
              }
              elseif($elements['items'][$i]['reminder_level'] >= 2)
               $elements['items'][$i]['overdueid'] = "<div id =\"".$elements['items'][$i]['overdueid']."\"><a href=\"javascript:ajaxreminder(".$elements['items'][$i]['overdueid'].");\" id=\"a_".$elements['items'][$i]['overdueid']."\" class=\"reminderlink\">Manual reminder</a> <img src=\"/library/assets/img/spacer.gif\" id=\"img_".$elements['items'][$i]['overdueid']."\" width=\"16\" height=\"16\"/></div>";
             }
            }
            else
             $elements['items'][$i]['overdueid'] = "No email ID";*/
           if(array_key_exists('email', $elements['items'][$i]) && $elements['items'][$i]['email'] == "")
            $elements['items'][$i]['overdueid'] = "No email ID";
           }



            // Change dates to D/M/Y format
            $arrDates = array("date_created", "date_removed", "date_due", "date_return", "date_borrow", "date_reminder");
            foreach($arrDates as $strDateName)
             if(array_key_exists($strDateName, $elements['items'][$i]))
              $elements['items'][$i][$strDateName] = format_date($elements['items'][$i][$strDateName]);



            // Add ID to modify the date with javascript           
            if(array_key_exists('date_reminder', $elements['items'][$i]) && isset($id))
                $elements['items'][$i]['date_reminder'] = "<span id=\"date_reminder_".$id."\">".$elements['items'][$i]['date_reminder']."</span>";

        }


//print_r($elements);



    print("\n\n<table class=\"tableview\" id=\"tableview\">\n");
    // Labels
    print("<thead>");
    print("  <tr>");
    foreach($elements['labels'] as $l)
    {
     if($l == "DDC" || $l == "Notes")
        print("<th class=\"{sorter: 'text'}\">".$l."</th>");
     else
        print("<th>".$l."</th>");
    }
    print("</tr>\n");    
    print("</thead>");
    print("<tbody>");
    // Rows
    if(is_array($elements['items']))
    {
        if(count($elements['items']) > 0)
        {
            foreach($elements['items'] as $row)
            {
                //if(array_key_exists('delete_flag', $row) && $row['delete_flag'] == 1)
                 //print("  <tr class=\"deleted\">");
                //else
                 print("  <tr>");
                foreach($elements['labels'] as $t => $l)
                {
                    if(array_key_exists($t, $row))
                    {
                        if(is_array($row[$t]))
                        {
    
                            $cell = array();
                            foreach($row[$t] as $v)
                                foreach($v as $s)
                                    $cell[] = $s;
                            print("<td>".implode("<br/>", $cell)."</td>");
                        }
                        else
                            print("<td>".$row[$t]."</td>");
                    }
                    else
                        print("Item ".$t." (".$l.") not found");
                }
                print("</tr>\n");
              //print("<hr><pre>\n");
              //print_r($b);
              //print("</pre><br>\n");
            //endforeach
            }
        }
        else
        {
            print("  <tr><td class=\"noresult\" colspan=\"".count($elements['labels'])."\">No results found matching your parameters</td></tr>\n");
        }
    }
    print("</tbody>");
    print("</table>\n");
}

function format_date($strDate)
{
 //print(htmlentities($strDate)."<br/>");
 if(!isset($strDate) || !is_string($strDate) || $strDate == "")
  return($strDate);
 if(strip_tags($strDate) != $strDate)
 {
  $strDateNoTags = strip_tags($strDate);
  return str_replace($strDateNoTags, format_date($strDateNoTags), $strDate);
 }
 else
 {
  if($strDate == "0000-00-00")
   return "";
  $arrDate = explode("-", $strDate);
  return $arrDate[2]."/".$arrDate[1]."/".$arrDate[0];
 }
}

?>
<br/>