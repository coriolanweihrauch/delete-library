<h3>Authentication required</h3>
<?php
if(!$this->session->userdata('loggedin'))
{
  print(form_open('settings/authenticate'));
  $credentials = "staff";
  print(form_label("Please enter the <b>".$credentials."</b> password")."<br/>\n");
  print(form_hidden("username", $credentials));
  if(isset($redirect))
    print(form_hidden("redirect", $redirect));
  print(form_password(array('name' => 'password', 'id' => 'password', 'size' => '30')));
  print(form_submit('submit', 'Log in'));
  print(form_close());
  
  print("<p> -- OR -- </p>");
}

print(form_open('settings/authenticate'));
$credentials = "admin";
print(form_label("Please enter the <b>".$credentials."</b> password")."<br/>\n");
print(form_hidden("username", $credentials));
if(isset($redirect))
  print(form_hidden("redirect", $redirect));
print(form_password(array('name' => 'password', 'id' => 'password', 'size' => '30')));
print(form_submit('submit', 'Log in'));
print(form_close());
?>