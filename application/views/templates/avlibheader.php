<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php print('<meta http-equiv="Content-type" content="text/html; charset='.$this->config->item('charset').'" />'); ?>
	<title><?php
if(isset($title))
	echo $title;	
?></title>
	<link rel="stylesheet" type="text/css" href="/library/assets/css/jquery-ui-1.8.23.custom.css" />
	<link rel="stylesheet" type="text/css" href="/library/assets/css/avlib.css" />
<?php
if($this->session->userdata('loggedin'))
{
	if($this->session->userdata('credentials') == 'admin')
	
		print("<link rel=\"stylesheet\" type=\"text/css\" href=\"/library/assets/css/avlib_admin.css\" />");
	//else
	//	print("<link rel=\"stylesheet\" type=\"text/css\" href=\"/library/assets/css/avlib_staff.css\" />");
}
else
	print("<link rel=\"stylesheet\" type=\"text/css\" href=\"/library/assets/css/avlib_guest.css\" />");
?>
<link rel="icon" href="/library/assets/img/favicon.png" type="image/ico">
<!--<script type="text/javascript" src="/library/assets/js/jquery-1.8.0.min.js"></script>-->
<!--<script type="text/javascript" src="/library/assets/js/jquery-ui-1.8.23.custom.min.js"></script>-->
	<script type="text/javascript" src="/library/assets/js/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="/library/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="/library/assets/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="/library/assets/js/jquery.metadata.js"></script>

<script type="text/javascript" language="javascript">
	$.datepicker.setDefaults({dateFormat: "yy-mm-dd" });

	function addLookup(name, bookid, fieldid)
	{
		//alert(fieldid);
		jqfieldid = "#"+fieldid;
		value = $(jqfieldid).val();
//alert("location:"+name+"\nbookid:"+bookid+"\nVal:"+value);
//		return;
		$.post('/library/books/ajaxlookupadd/',
				  { name: name, bookid: bookid, value: value},
					 function(data){
						$(jqfieldid).val("");
						loadLookup(name, bookid);
 					}
		);
	}

	function loadLookup(name, bookid)
	{
//alert('loading');
		$.post('/library/books/ajaxlookupload/',
		{ name: name,
			bookid: bookid },
		 function(data){
	
		 
		var category = "";
		// Print labels row
		s = "<tr>";
		$.each( data.labels, function(i, n){
			category = i;
			s += "<th>"+n+"</th>";
		});
		//alert(category);
		s += "</tr>\n";
	
		// Print item row(s)
		for(var item in data.items) {
			var i= data.items[item];
		
		console.log(i);
		
			s += "<tr>";	
			for(label in data.labels){
				if (label != "notes")
					s += "<td><a href=\"/library/lists/edit/"+label+"s/"+i[label+'_id']+"\">"+i[label]+"</a></td>";
				else
					s += "<td>"+i[label]+"</td>";
			}
			s += "</tr>\n";
		
		}
	
		// Put them on the screen
		$("#lookupval_"+name).html(s);
	//alert(data);
		},
		"json"
		);
	}

	function deleteLookup(name, bookid, lookupid)
	{
		bConfirm = confirm("are you sure you want to delete this entry ?");
		if(bConfirm)
		{
			$.post('/library/books/ajaxlookupdelete',
						{ name: name, lookupid: lookupid },
						 function(data){
							loadLookup(name, bookid);
						}
			);
		}
	}

	function ajaxreminder(transactionid)
	{
		$("#img_"+transactionid).attr("src", "/library/assets/img/ajax-loader.gif");
		$.post("/library/books/overduesendreminder/"+transactionid+"/1",
				  { transactionid: transactionid, trial: true },
					 function(data){

						if(data['error'] != null)
						{
							alert(data['error'])
							$("#img_"+transactionid).attr("src", "/library/assets/img/cross.png");
							$("#a_"+transactionid).html("Error - try again ?");
						}
						else
						{
							// Print labels row
							alert("server response:"+JSON.stringify(data));
							$("#img_"+transactionid).attr("src", "/library/assets/img/tick.png");
							$("#a_"+transactionid).parent().html("Sent !");
							$("#date_reminder_"+transactionid).html("Today !");
						}
					},
 					"json"
		);
	}

	function confirmbeforeredirect(turl)
	{
		if(confirm('Are you sure ?'))
			document.location=turl;
	}

// Clear form
/*$(".reset").click(function() {
	alert('hello');
    $(this).closest('form').find("input[type=text], textarea").val("");
});*/

	function resetForm(formid)
	{
		$(":input").not(":button, :submit, :reset, :hidden").each( function() {
			$("#"+this.id).attr({ value: '' }); 
		});
		//console.log(window.location.pathname);
		//document.href = document.URL;
		
		//$f = $("#"+formid);
		//$f.submit();
		
		//$("#"+formid).submit();
		//document.getElementById(eval(formid)).submit();
		
		//document.location = document.href;
		//location.reload();
	}
	
	function changeDatabase(menu) {
		document.location = "/library/settings/changeDatabase/"+$(menu).val();
	}

	function deletePersonFromBook(bookid, personid, roleid)
	{
		bConfirm = confirm("are you sure you want to delete this author ?");
		if(bConfirm)
		{
			$.post('/library/authors/ajaxremoverole/',
						{ bookid: bookid, personid: personid, roleid: roleid },
						 function(data){
							loadPersons(bookid);
						}
			);
		}
	}

	function addPersonToBook(bookid, personid, roleid)
	{
		$.post('/library/authors/ajaxaddrole/',
				  { bookid: bookid, personid: personid, roleid: roleid },
					 function(data){
						$("#personid").val("");
						loadPersons(bookid);
 					}
		);
	}

	function loadPersons(bookid)
	{
		$.post('/library/authors/ajaxlistroles/',
				  { bookid: bookid },
					 function(data){

					// Print labels row
					s = "<tr>";
					$.each( data.labels, function(i, n){
						s += "<th>"+n+"</th>";
					});
					s += "</tr>\n";
			
					// Print item row(s)
					for(var item in data.items) {
						 var i= data.items[item];
					
						s += "<tr>";	
						for(lable in data.labels){
							if (i[lable] == null || i[lable] == "null")
								i[lable] = "";
							s += "<td>"+i[lable]+"</td>";
						}
						s += "</tr>\n";
					
					}

					// Put them on the screen
					$("#lookupval_author").html(s);
 					},
 					"json"
		);
	}

	$(document).ready(function() 
		{
			$.tablesorter.defaults.widgets = ['zebra']; 
			$("#tableview").tablesorter({
					//debug: true,
					//textExtraction: 'complex',
					//headers: {
					//	1: { sorter: 'text' }
					//}
			});
		}
	);

	// For help text
	function showHideSection(sectionId)
	{
		if($("#"+sectionId).css('display') == "none")
			$("#"+sectionId).show(250);
		else
			$("#"+sectionId).hide(150);
	}

// does not work :(
//$('.noEnterSubmit').bind('keypress', false);
<?php
// Focus on specific field on load of page
if(isset($focusfield))
{
	print("\$(document).ready(function() {  $('#".$focusfield."').focus().val(\$('#".$focusfield."').val());});");
}
?>
</script>
</head>
<body>
<div id="asynctocontainer"></div>
<div id="container">
	<div id="header">
		<div id="titlebar" onClick="javascript:window.location='/library/'">
			<div id="titlelogo"><img src="/library/assets/img/avlogo.png" width="100%" height="100%" /></div>
			<div id="titletextbox"><div id="titleitem"><?php
$CI =& get_instance();
$CI->load->library('session');
print($CI->config->item('avlib_title'));
			?></div></div>
		</div>			

		<div id="menubar">
<?php
// Multiple database pull-down
$dbset = $CI->config->item('avlib_database_set');
$dbdefault = $CI->config->item('avlib_database_default');
$dbsession = $CI->session->userdata('avlib_database');
if(isset($dbset) && is_array($dbset) && count($dbset) > 1 && array_key_exists($dbdefault, $dbset)){
	if(isset($dbsession) && $dbsession != "")
		print("      <span class=\"menuitem_active\">".form_dropdown('dbselect', $dbset, $dbsession, "onChange=changeDatabase(this);")."</span>\n");
	else
		print("      <span class=\"menuitem_active\">".form_dropdown('dbselect', $dbset, $dbdefault, "onChange=changeDatabase(this);")."</span>\n");
}



// Main Menu
if($this->session->userdata('loggedin'))
{
	$arrMenuItems = array(
		"Check-in" => "/library/books/checkin",
		"Check-out" => "/library/books/checkout",
		"Users" => "/library/clients/search",
		"Books" => "/library/books/search",
		"Settings" => "/library/settings",
	);
	
	// Menu items
	foreach($arrMenuItems as $n => $u)
	{
		if(isset($selectedMenu) && $n == $selectedMenu)
			print("      <span class=\"menuitem_active\"><a href=\"".$u."\">".$n."</a></span>\n");
		else
			print("      <span class=\"menuitem\"><a href=\"".$u."\">".$n."</a></span>\n");
	}
	
	// Help link
	if($helpLink && $helpLink != "")
		print("<span class=\"menuitem\"><a href=\"".$helpLink."\" target=\"_blank\">Help</a></span>");

	// Login / logout
	if($this->session->userdata('credentials') != "admin")
		print("<span class=\"menuitem\"><a href=\"/library/books/switchtoedit\"><img src=\"/library/assets/img/pen.png\" /></a></span>");
	print("<span class=\"menuitem\"><a href=\"/library/settings/deauthenticate\"><img src=\"/library/assets/img/lock.png\" /></a></span>");
	
	print("    </div>\n");
}
else
{
	// Help link
	if($helpLink && $helpLink != "")
		print("<span class=\"menuitem\"><a href=\"".$helpLink."\" target=\"_blank\">Help</a></span>");

?>
			<span class="menuitem"><a href="/library/books/checkin"><img src="/library/assets/img/lock.png" /></a></span>
		</div>
<?php
}



?>
	</div>

	<div id="content">
		<div>
<?php
// Display success, warning or error messages
if(isset($messages) && !isset($messagescustomlocation))
{
	print("			<div id=\"messagebox\">\n");
	foreach ($messages as $i => $msg)
	{
		foreach($msg as $t => $m)
			print("			  <div class=\"".$t."\">".$m."</div>\n");
	}
	print("		</div>\n");
}
?>