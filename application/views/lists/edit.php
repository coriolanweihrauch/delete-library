<?php echo validation_errors(); ?>

<?php
//print_r($elements); 
//print_r($loaded_book);
if($elements['id_prev'])
	$link_prev = "<a href=\"/library/lists/edit/".$elements['fields']['list']."/".$elements['id_prev']."\">&lt;&lt;</a> ";
if($elements['id_next'])
	$link_next = " <a href=\"/library/lists/edit/".$elements['fields']['list']."/".$elements['id_next']."\">&gt;&gt;</a>";
print("<h3>".$link_prev.$title.$link_next."</h3>");
//print("<h3><a href=\"/library/lists/edit/".$elements['fields']['list']."/".$elements['id_prev']."\">&lt;&lt;</a> ".$title." <a href=\"/library/lists/edit/".$elements['fields']['list']."/".$elements['id_next']."\">&gt;&gt;</a></h3>");
?>



<?php
// ###  Make this link neater  ###
//echo form_open('books/edit/'.$elements['id']['value']);
if(array_key_exists("id", $elements['fields']))
	echo form_open('lists/edit/'.$elements['fields']['list']."/".$elements['fields']['id'], array("id" => "listeditform"));
else
	echo form_open('lists/edit/'.$elements['fields']['list']);


?>
<table>
  <tr>
    <td>Name</td><td><input type="text" name="name" value="<?php print($elements['fields']['name']); ?>" size="50" maxlength="200"/></td>
  </tr>
  <tr>
    <td>Notes</td><td><input type="text" name="notes" value="<?php print($elements['fields']['notes']); ?>" size="50" maxlength="128"/></td>
  </tr>
</table>



<input type="submit" value="Save" />

<?php
if(array_key_exists("id", $elements['fields']))
{
  ?>
<p>  <input type="button" name="delete" class="delete" value="Delete item" onClick="javascript:deleteList('<?php print($elements['fields']['list']."','".$elements['fields']['id']); ?>');"/> </p>
  <?php
}
?>


</form>
<br/>
<script type="text/javascript" language="javascript">
  function deleteList(listname, itemid)
  {
    bConfirm = confirm('Are you sure you want to delete this item from '+listname+' permanently ?\nThis will delete the item and ALL references to it !');
    if(bConfirm)
      window.location.href="/library/lists/delete/"+listname+"/"+itemid;
  }
</script>