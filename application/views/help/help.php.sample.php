<H3>Help Files</H3>

<i>Dear User,<br>
we have created this text to empower you to find your own way through the library. It should answer all of the basic questions, and some more. Nevertheless, if you have trouble finding a book, or if you have suggestions regarding this help text, please feel free to approach a librarian anytime.</i>

<H4><i><u>Using the Library</u>:</i></H4>

<a name="contact">
<div class="help-title" id="title_0"><a href="javascript:showHideSection('library_0')">Contact & Opening hours</a></div>
<div class="help-content" id="library_0">
<p>
<b>Address</b>:<br>
The Auroville Library<br>
Crown rd 2<br>
Auroville, TN, India<br>
605 101<br>
</p>
<p>
The building is located opposite Kailash Clinic
</p>
<p>
<b>Contact</b>:<br>
Phone 262 2894<br>
Email <a href="mailto:avlib@auroville.org.in">avlib@auroville.org.in</a><br>
Web <a href="http://library.auroville.org.in/" target=_blank>http://library.auroville.org.in/</a>
</p>
<p>
<b>Opening hours</b>:<br>
Monday - Saturday<br>
9:00 am - 12:30 am<br>
2:00 pm - 4:30 pm<br>
</p>
</div>
</a>

<a name="reading">
<div class="help-title" id="title_00"><a href="javascript:showHideSection('library_00')">Reading without lending</a></div>
<div class="help-content" id="library_00">
<p>Reading books within the library's hall is free of charge and does not require any registration.</p>
</div>
</a>

<a name="membership">
<div class="help-title" id="title_1"><a href="javascript:showHideSection('library_1')">Opening a user account</a></div>
<div class="help-content" id="library_1">
<p>
<b>Aurovilians</b> and <b>newcomers</b> who want to borrow books simply come to the front desk to register. Please bring a valid ID of any kind. We will open a new account for you, and note some basic contact information, after which you can immediately lend books.
</p>
<p>
<b>Guests</b> of Auroville, previous to registration, please submit a deposit of 1000 Rs to the Library's deposit account, at the Town Hall. Bring us the receipt and a copy of your passport's main page for registration at our front desk. After that you can immediately lend books. The deposit will be refunded once you have cleared & closed your user account.<br>
<b>In our facilities, we do NOT accept payment</b>, be it cash, cheque, credit cards, or AuroCard.
</P>
</div>
</a>

<a name="borrowing">
<div class="help-title" id="title_2"><a href="javascript:showHideSection('library_2')">Borrowing books</a></div>
<div class="help-content" id="library_2">
<p>
Once you've opened a user account you can immediately borrow books. Guests may take up to three books, Aurovilians and newcomers up to five. The initial lending period is 30 days. It can be extended by another 30 days on presenting the volumes to our personnel. Borrowing is for free.
</p>
<p>
Please keep the material dry and safe, do not scribble or note on their pages, nor earmark them. Think of putting back all extra materials like CDs, DVDs, maps, plates, and gimmicks on returning your books.
</p>
</div>
</a>

<a name="interloan">
<div class="help-title" id="title_2a"><a href="javascript:showHideSection('library_2a')">Inter-library loan</a></div>
<div class="help-content" id="library_2a">
<p>
The Auroville Library cannot retrieve books from other libraries for you. Neither do we hold catalogues of other libraries whatsoever, nor did Auroville build up a union catalogue so far.
</p>
</div>
</a>

<a name="arrangement">
<div class="help-title" id="title_3"><a href="javascript:showHideSection('library_3')">Arrangement of books</a></div>
<div class="help-content" id="library_3">
<p>
The books of the AV Library are put separate by their languages. Within each language they are arranged by subjects. Within each subject they are arranged by author, and within one author's works, by title.<br>
Languages, subjects, authors, and titles are written, in short form, on a square label at the bottom of each book's spine. Read the following three help topics to find out how that works in detail.
</p>
</div>
</a>

<a name="labels">
<div class="help-title" id="title_4"><a href="javascript:showHideSection('library_4')">Book labels</a></div>
<div class="help-content" id="library_4">
If you take one of the books you will see a square label at the bottom of the book's spine.<br>
It could look like this:<br>
<p>
<table border="0" align=left bgcolor="#DDEEFF" cellpadding=7><tr><th align=left>G 392.5<br>PETE 8<br>035001</th></tr></table> 
<b>Language and subject</b> are encoded in the first line: "G 392.5" from this example stands for German books on marriage customs.<br>
The second line of the label gives you <b>author and title</b> in short form: Mr. PETErsen wrote his book <i>"Hochzeit in Afrika"</i>. H, the first letter of the title, is the 8th letter of the alphabet.<br>
Do not mind the number "035001" in the third line of the label. It is the unique identifier of the book; it is only interesting for the librarians.
</p>
</div>
</a>

<a name="subjects">
<div class="help-title" id="title_5"><a href="javascript:showHideSection('library_5')">Finding books by subject</a></div>
<div class="help-content" id="library_5">
<p>
The books of the AV Library are arranged by subjects. Subjects are encoded by a system of numbers called "Dewey Decimal Classification", or "DDC" in short. You find them, e.g., on a square label at the bottom of each book's spine.
It might say something like "392.5", where 3 stands for "Social topics", 9 specifies "Customs & folklore", 2 stands for "Life cycle", and 5 narrows it in on "Marriage". So "392.5" stands for books on marriage customs.
</p>
<ul>
<li>As you see you could find books on a certain subject by going to the shelves, following the numbers from "most general" (3, in this case), to "most specific" (5, here). The meaning of the first and second digits are given on the shelf signs. More specific information is provided on each shelf board.</li>
<li>A letter-size chart with the headline <i>"The order of books in the Auroville Library"</i> gives you a quick list of general subjects and their DDC numbers. The chart is supposed to be lying on all tables.</li>
<li>If you want to look up more specific subjects or keywords, a librarian can show you a printed register. Ask for the DDC books.</li>
<li>Searching the computer database, though, may be much simpler for you. A librarian can show you the user terminal or look up a few volumes for you.</li>
</ul>
<p>
DDC is a fairly common classification system all over the world, especially in the US. People who are already familiar with it may notice minor differences in the Auroville Library. The adjustments we made are explained in the next paragraph.
</p>
</div>
</a>

<a name="DDC">
<div class="help-title" id="title_5a"><a href="javascript:showHideSection('library_5a')">DDC deviations in the AV Library</a></div>
<div class="help-content" id="library_5a">
<p>
We made the following changes and additions to Dewey's Standard Classification:
</p>
<ul>
<li>006 Computers</li>
	<ul>
<li>006.1 – General, Hardware</li>
<li>006.2 – Operating systems</li>
<li>006.5 – Programming</li>
<li>006.6 – Graphics</li>
<li>006.7 – Video</li>
<li>006.8 – Sound</li>
<li>006.9 – Networking</li>
	</ul>
<li>007 Internet</li>
	<ul>
<li>[similar to 006]</li>
	</ul>
<li>298 Spirituality</li>
	<ul>
<li>298.1 – Relationships & attitudes</li>
<li>298.11 – Spirituality & Science</li>
<li>298.12 – Spirituality & Religion</li>
<li>298.13 – Spirituality & Philosophy</li>
<li>298.14 – Spirituality & Business</li>
<li>298.4 – Spiritual life</li>
<li>298.41 – Teachers & teachings, guides</li>
<li>298.42 – Spiritual enlightenment</li>
<li>298.44 – Spiritual practice</li>
<li>298.5 – Spiritual morals</li>
<li>298.6 – Literary works</li>
<li>298.7 – Spiritual memoirs & diaries</li>
<li>298.8 – Philosophical treatment</li>
<li>298.9 – Historical treatment</li>
	</ul>
<li>420 Languages</li>
	<ul>
<li>[420's of all languages placed in English section]</li>
<li>421 – Orthography</li>
<li>421.5 – Phonology</li>
<li>422 – Etymology</li>
<li>423 – Dictionaries</li>
<li>424 – Thesauri</li>
<li>425 – Grammar & textbooks</li>
<li>427 – Nonstandard language</li>
<li>428 – Usage (idioms, travel)</li>
	</ul>
<li>820 English literatures</li>
	<ul>
<li>823 – English Novels & short stories</li>
<li>823 s – English Science Fiction</li>
<li>823 t – English Thrillers</li>
	</ul>
<li>[along the same lines:]</li>
	<ul>
<li>D 830 – Dutch literatures</li>
<li>G 830 – German literatures</li>
<li>F 840 – French literatures</li>
<li>I 850 – Italian literatures</li>
<li>S 860 – Spanish literatures</li>
<li>R 890 – Russian literatures</li>
<li>T 890 – Tamil literatures</li>
<li>[DDC is given according to the book's actual language, <i>not</i> the origin of the work]</li>
	</ul>
<li>920 Biographies</li>
	<ul>
<li>920 – Biographies--Collections</li>
<li>921 – Biographies--Specific (individuals & families)</li>
<li>922 – Memoirs & autobiographies</li>
<li>923 – Diaries, letters, interviews, & other original sources</li>
	</ul>
<li>SA Sri Aurobindo</li>
	<ul>
<li>10 – Collected  works</li>
<li>15 – Single works</li>
<li>16 – Talks / Agenda</li>
<li>17 – Compilations</li>
<li>19 – Indices, concordances, glossaries</li>
<li>20 – Disciples on Integral Yoga</li>
<li>21 – Studies on Integral Yoga</li>
<li>25 – Studies on single works</li>
<li>30 – Biographies</li>
<li>50 – Disciples’ works (on anything)</li>
<li>90 – Ashram publications</li>
	</ul>
<li>TM The Mother</li>
	<ul>
<li>[similar to SA]</li>
	</ul>
<li>AV Auroville</li>
	<ul>
<li>[books on Auroville]</li>
<li>307.1 – Philosophical background</li>
<li>307.3 – Law & economics</li>
<li>307.5 – Events & activities</li>
<li>307.7 – Architecture & arts</li>
<li>307.8 – AV in literature</li>
<li>307.91 – Travel accounts</li>
<li>307.92 – Memoirs & biographies</li>
<li>307.94 – Historical accounts</li>
<li>307.95 – History</li>
	</ul>
</ul>
</div>
</a>

<a name="languages">
<div class="help-title" id="title_6"><a href="javascript:showHideSection('library_6')">What is available</a></div>
<div class="help-content" id="library_6">
<p>
As of December 2013, the Library has more than 30,000 books in its collection, most of which can be borrowed.<br>
Basically <b>all fields of knowledge</b> are covered. Though we have a few volumes that meet scientific standards, the focus is clearly on popular works that explain matters intelligibly to all.<br>
Due to many Aurovilians' typical interests, especially works on spirituality, Hinduism, Buddhism, arts, alternative medicine, and history have accumulated over the years.<br>
You can also find quite a lot of books of Indian authors and/or about India all over the place.<br>
As a public library, our second focus is on works of <b>fiction</b> which make up about one third of the collection. Following novels in general (DDC=823), there is each a corpus of thrillers (marked 823 t) and science fiction/fantasy books (823 s) standing on the shelves.
</p>
<p>
<b>Sri Aurobindo</b>, <b>The Mother</b>, and books from/about <b>Auroville</b> deserved a special position, of course. They have been given their own corners in the library (DDC starting with SA, TM, and AV, respectively).
</p>
<p>
Each of the <b>languages</b> has a separate section in the main hall. In clockwise direction, these are
<ul>
<li>English</li>
<li>Dutch</li>
<li>French</li>
<li>German</li>
<li>Italian</li>
<li>Russian</li>
<li>Spanish</li>
<li>Tamil</li>
</ul>
Except for English, the DDC of each book starts with the first letter of each language, e.g. "G 910" for a German book on travelling.
</p>
<p>
There are also some <b>magazines</b> in Tamil, English, French and German, and <b>newspapers</b> in Tamil and English. Those can be read within the library's hall only.
</p>
<p>
<b>Children</b>'s books can be found in the room opposite to the main entrance of the hall. They are marked "c" for the youngest (0-5 years), "k" for kids, age 5-10, and "y" for teenaged youth.<br>
Young readers are free to talk, read and play in that room.<br>
For borrowing, please come with your books to the front desk in the main hall.
</p>
<p>
Our collection <i>does not</i> contain films and music. Please visit the Music library and the Video library in the Townhall/MMC.
</p>
</div>
</a>

<a name="IT">
<div class="help-title" id="title_7"><a href="javascript:showHideSection('library_7')">Laptops & WiFi possible?</a></div>
<div class="help-content" id="library_7">
<p>
You can bring your laptop for work in the library, yet we do not offer WiFi. Please bring your own internet stick if you need access to the web.
</p>
</div>
</a>

<a name="donations">
<div class="help-title" id="title_8"><a href="javascript:showHideSection('library_8')">Donation of books</a></div>
<div class="help-content" id="library_8">
<p>
Book donations are always welcome, be it novels, or science books, or photo albums, or what you have. It might happen that we have to sort out some volumes if there is already a copy present, or if the book is outdated, or unsuitable for a public library.<br> 
Please do not donate photocopies, spiral-bound volumes, or books in languages other than those already present in the AV Library.
</p>
</div>
</a>

<H4><i><u>Using the Catalogue Database</u>:</i></H4>

<a name="where">
<div class="help-title" id="title_9a"><a href="javascript:showHideSection('dbase_1')">Where is the Catalogue?</a></div>
<div class="help-content" id="dbase_1">
<p>
The AV Library's catalogue is stored on a web page <!-- (<a href="http://library.auroville.org.in/">http://library.auroville.org.in/</a>) --> which you can access from <!--your home internet connection. There is also--> a <b>user terminal</b> in the library's main hall. Our librarians can explain you how to use it, or they can perform a quick search for the books you are interested in. Also consider coming back to this page to read the following help topics.
</p>
</div>
</a>

<a name="search">
<div class="help-title" id="title_9b"><a href="javascript:showHideSection('dbase_2')">Performing a simple search</a></div>
<div class="help-content" id="dbase_2">
<p>
To perform a quick search for a specific book of which you know elements of the title and/or the author's name, click on "Search" in the menu.<br>
On your search page you see two forms. You can use one, or the other, or both. Upper/lower case letters don't make a difference.<br>
Let's say you are looking for Tolkien's <i>"The lord of the rings"</i>.
</p>
<p>
It is not advisable to look for "the" (as it appears in so many book titles, and you cannot be sure whether it is part of Tolkien's), but the beginning of any meaningful word will do for a title search. Type "lord ring" into the title field, for instance, then click the "search" button. The database will look up your search phrase in main titles, extended titles, serials titles, even original titles. That's why your results list contains also books in languages other than English.
</p>
<p>
Let's say you know the title, but not the exact spelling of the author's name. Try a combined search:<br> 
Type "Rings" into the title field, add "Tolk" in the author field, click the "search" button.
</p>
<p>
You could also try to find books by the author's name alone. The search results would show you all his works which we have in our collection. In Tolkien's case, it's more than 40 already.
</p>
<p>
 There may be cases where you need to be much more specific, or where you neither know anything about the title nor the author's name. Or you want to find books by subjects. You should then switch to "Advanced search"
</p>
</div>
</a>

<a name="search_advanced">
<div class="help-title" id="title_10"><a href="javascript:showHideSection('dbase_3')">Performing an advanced search</a></div>
<div class="help-content" id="dbase_3">
<p>
To perform an advanced search, click on "Adv.Search" in the menu.<br>
The search form looks a bit more complicated, and actually, to receive relevant results here you have to be precise. But it is a very powerful tool to get exactly what you are looking for. It also allows for looking up books by subjects (<i>"topical terms"</i>).<br>
You can enter between one and three search terms and combine them in different ways. The form has to be filled line by line, starting with the uppermost.<br>
</p>
<ol type="1">
<li>Click on the space where it initially says "Title". A pulldown menu opens, from which you can select the (data) field in which to perform the search.</li>
<li>Then click on the space where it initially says "Contains". Another pulldown menu opens, from which you can select where your search term is located in the data field, in the beginning, at the end, anywhere, or if it is exactly as you type it.</li>
<li>Click into the text field in the third column. Type the search term here.</li>
<li>If you want to give more than one search term, choose an operator from the last column. "AND" will find books that match all search terms, "OR" will find books that match one or the other.</li>
<li>If you want to give more than one search term, continue in line 2 as described above.</li>
<li>Click the "Search" button.</li>
</ol>
<p>
<b>Some examples.</b><br>
A) You know that the title ends with "in India", and that the book has been published during the last 10-15 years.<br>
In line 1, choose "Title" from the fields, then choose "ends with" as a criteria, type "in india" as a value, choose "AND" as an operator.<br>
In line 2, choose the field "Publication year", choose "begins with" as criteria, type "20" (meaning, twothousandsomething) into the value field, then click the "Search" button.<br>
 <br>
B) You are looking for books of a certain author, but you are not quite sure how to spell his name. Something like Eckard or Eckhart.<br>
In line 1, choose "Author & other people", then "Begins with" as criteria, type "Eckar" as a value, choose "OR" as operator<br>
In line 2, choose "Author & other people", then "Begins with" as criteria, type "Eckhar" as a value, click the "Search" button.<br>
 <br>
C) You would like to cook Chinese meals, therefore you need a recipe book. Let's try with a search by subject (=<i>"topical term"</i>).<br>
In line 1, choose "Topical term", then "Contains" as criteria, type "chin" as a value, choose "AND" as operator<br>
In line 2, choose "Topical term", then "Contains" as criteria, type "cook" as a value, click the "Search" button.<br>
 <br>
D) You are interested in German-language historical novels that have been added to our collection during your holidays, in December 2013.<br>
In line 1, choose "Date added", then "Begins with" as criteria, type "2013-12" as a value, choose "AND" as operator<br>
In line 2, choose "DDC", then "Equals (soft)" as criteria, type "G 833" as a value, choose "AND" as operator<br>
In line 3, choose "Topical term", then "Contains" as criteria, type "histor" as a value, click the "Search" button.<br>
 <br>
E) You remember having seen this photo book on black tribal people. Funny as it seems, you can't recall the author, but clearly remember "Harvill" being the publishing house, and the language was English. A search containing "Africa" resulted in too many hits, so...<br>
In line 1, choose "Publisher", then "Contains" as criteria, type "Harvill" as a value, choose "AND" as operator<br>
In line 2, choose "Features", then "Contains" as criteria, type "photo" as a value, choose "AND" as operator<br>
In line 3, choose "Language", then "Contains" as criteria, type "ENG" as a value, click the "Search" button.<br>
Voil&agrave;!
</p>
</div>
</a>

<a name="search_tips">
<div class="help-title" id="title_10a"><a href="javascript:showHideSection('dbase_3b')">More search tips</a></div>
<div class="help-content" id="dbase_3b">
<ul>
<li>To keep the search broad, shorten the search phrase to a minimum. If you enter "scien" the database will deliver hits containing science, sciences, life sciences, scientist, scientific, scientifically, science-based, and all the rest of it.</li>
<li>The less specific you are searching the more results will show up. If you get too many (irrelevant) results try to enter additional information into the search fields.</li>
<li>Currently, (simple) Search does <i>not</i> show results containing special characters if the search phrase was written with basic characters, or the other way round.<br>
Example 1: If you typed the name "Bjorn", then results containing "Bj&ouml;rn" or "Bj&oslash;rn" will not show up.<br>
Example 2: If you typed the name "Fr&eacute;d&eacute;rick", then your results will not contain "Frederick".</li>
<li>Advanced Search does that trick, though. </li>
<li>You can sort results lists by clicking on one of the table headers, e.g. clicking on "Title" will sort the list by that criteria.<br>
A click on any of the book titles will take you to the details view of that volume.<br>
A click on any of the authors names will show you information on the person, and a list of their books we have in the library.<br></li>
</ul>
</div>
</a>

<a name="details_1">
<div class="help-title" id="title_11"><a href="javascript:showHideSection('dbase_3a')">Detail info on books, authors, and subjects</a></div>
<div class="help-content" id="dbase_3a">
<ul>
<li>Having clicked on a title from a search results list brings you to the <b>details view of a book</b>. It shows the spine label and the availability of the volume, followed by all its catalog information and, sometimes, a summary. You can click on the author's name, or on one of the subjects to find additional relevant books.</li>
<li>Having clicked on an author's name from a search results list brings you to the <b>details view of a person</b>. It contains honours and academic titles, fuller forms of the name, lifespan, and nationality, followed by a list of books the library holds.</li>
<li>Having clicked on a <b>subject</b> in the Book details page brings you to a list of titles with the same subject (we call it <b>topical term</b>).<br>
The line where it says "Notes:" mentions a number. This is the DDC number which is identical to the location of the subject on our book shelves. For instance, the topical term "Spiritual memoirs" has a note saying "298.7". Go to the respective shelf to find more material on that subject.</li>
</ul>
</div>
</a>

<a name="listnew">
<div class="help-title" id="title_12"><a href="javascript:showHideSection('dbase_4')">Search for new arrivals</a></div>
<div class="help-content" id="dbase_4">
<p>
Due to a constant flow of donations and purchases the library is growing by more than 3500 volumes each year. That makes ten newly arriving books per day. Guess you had been out of station for three months - how to keep track?<br>
Click "List: New" in the main menu, then fill dates into the form. You can browse through the calendar, or you enter dates in the style yyyy-mm-dd (e.g. "2013-12-25" for Christmas day 2013). Having entered the time frame you click on the "Search" button. Be prepared for a few seconds, or even minutes, of waiting. The list might be long.
</p>
<p>
Sort the results list by clicking on one of the table headers, e.g. clicking on "DDC" will sort the list by subjects.<br>
A click on any of the book titles will take you to the details view of that volume.<br>
A click on any of the authors names will show you information on the person, and a list of their books we have in the library.
</p>
</div>
</a>

<a name="myhistory">
<div class="help-title" id="title_13"><a href="javascript:showHideSection('dbase_5')">My account details</a></div>
<div class="help-content" id="dbase_5">
<p>
To check the validity of your data, or to find a good book you once read go to "My History" in the main menu.<br>
Enter your library number (retrieve it at the front desk) and your first name, then click "View my history".<br>
The following page shows a list of stored data from your account, and your borrowing history. Please keep your contact infos up to date. To change any of it, kindly tell the libarian at the front desk. The borrowing history, though, cannot be changed or removed.
</p>
</div>
</a>
<a name="freesoft">
<div class="help-title" id="title_19"><a href="javascript:showHideSection('dbase_9')">Database program is Open Source</a></div>
<div class="help-content" id="dbase_9">
<p>
The AVL database program is Open-Source software, free for download, adaptation, and distribution. Its use has only two conditions to it:
</p>
<ul>
<li>Passing it on has to be free of charge.</li>
<li>On distribution you pass on all of the above conditions as well, without changes to them.</li>
</ul>
Several Aurovilian libraries have already adopted the AVL database program, among them the <i>Centre of Indian Studies</i> and various schools.
</div>
</a>

