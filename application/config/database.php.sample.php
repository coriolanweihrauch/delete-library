<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;

// Primary database
// Set username, password and database name - and hostname if required
$db['default']['hostname'] = '127.0.0.1';
$db['default']['username'] = 'username_default';
$db['default']['password'] = 'password_default';
$db['default']['database'] = 'db_default';
$db['default']['dbdriver'] = 'mysql';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;

// If a secondary database is required, fill this out
// You can add as many secondary databases as required
// just make sure that the name $db['name'] is unique
// and name is added to the config.php file under $config['avlib_database_set']
$db['secondary']['hostname'] = '127.0.0.1';
$db['secondary']['username'] = 'username_secondary';
$db['secondary']['password'] = 'password_secondary';
$db['secondary']['database'] = 'db_secondary';
$db['secondary']['dbdriver'] = 'mysql';
$db['secondary']['dbprefix'] = '';
$db['secondary']['pconnect'] = TRUE;
$db['secondary']['db_debug'] = TRUE;
$db['secondary']['cache_on'] = FALSE;
$db['secondary']['cachedir'] = '';
$db['secondary']['char_set'] = 'utf8';
$db['secondary']['dbcollat'] = 'utf8_general_ci';
$db['secondary']['swap_pre'] = '';
$db['secondary']['autoinit'] = TRUE;
$db['secondary']['stricton'] = FALSE;

// For import from NewGenLib (Postgres)
$db['import']['hostname'] = '127.0.0.1';
$db['import']['username'] = 'username_import';
$db['import']['password'] = 'password_import';
$db['import']['database'] = 'db_import';
$db['import']['dbdriver'] = 'postgre';
$db['import']['dbprefix'] = ''; 
$db['import']['pconnect'] = TRUE;
$db['import']['db_debug'] = TRUE;
$db['import']['cache_on'] = FALSE;
$db['import']['cachedir'] = '';
$db['import']['char_set'] = 'utf8';
$db['import']['dbcollat'] = 'utf8_general_ci';
$db['import']['swap_pre'] = '';
$db['import']['autoinit'] = TRUE;
$db['import']['stricton'] = FALSE;

/* End of file database.php */
/* Location: ./application/config/database.php */