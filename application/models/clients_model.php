<?php
//include_once(base_url().'application/models/include/common.php');
//include_once(base_url().'application/models/include/fields_clients.php');


class Clients_model extends CI_Model {

	public $fields;
	private $classname = 'client';
	public $ldb;

	public function __construct()
	{
		//$ci = get_instance();
		//$ci->load->helper('avlib');
		$this->load->helper('avlib');
		$this->load->helper('html');
		$this->load->database();

		$this->ldb = $this->load->database(get_current_database(), TRUE);


$fields['aurovillename'] = array(
              'name'        => 'aurovillename',
							'type'				=> 'text',
              'id'          => 'client_aurovillename',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Aurovillename',
							'show_edit'		=> '1',
							'show_search' => '1',
							'show_result'		=> '1',
            );

$fields['name'] = array(
              'name'        => 'name',
							'type'				=> 'text',
              'id'          => 'client_name',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Name',
							'show_edit'		=> '1',
							'show_search' => '1',
							'show_result'		=> '1',
            );

$fields['surname'] = array(
              'name'        => 'surname',
							'type'				=> 'text',
              'id'          => 'client_surname',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Surname',
							'show_edit'		=> '1',
							'show_search' => '1',
							'show_result'		=> '1',
            );

$fields['workplace'] = array(
              'name'        => 'workplace',
							'type'				=> 'text',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Workplace',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '1',
            );

$fields['address'] = array(
              'name'        => 'address',
							'type'				=> 'textarea',
							'cols'				=> '30',
							'rows'				=> '2',
              'id'          => 'client_address',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Address',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '1',
							'script'			=> "$(\"#client_address\").autocomplete({
																	source: function(request, response) {
																		$.ajax({
																			url: \"/library/clients/ajaxsearch/\",
																			data: { term: $(\"#client_address\").val(), field: \"address\"},
																			dataType: \"json\",
																			type: \"POST\",
																			success: function(data){
																				response(data);
																			}
																		});
																	},
																	minLength: 1,
																});",
            );

$fields['telephone'] = array(
              'name'        => 'telephone',
							'type'				=> 'textarea',
              'id'          => 'client_telephone',
              'value'       => '',
              'cols'   			=> '30',
              'rows'        => '3',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Telephone',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '1',
            );

/*$fields['cellphone'] = array(
              'name'        => 'cellphone',
							'type'				=> 'text',
              'id'          => 'client_cellphone',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Cellphone',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '1',
            );
*/
$fields['email'] = array(
              'name'        => 'email',
							'type'				=> 'text',
              'id'          => 'client_email',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '40',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'E-mail',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$fields['group'] = array(
							'name'				=> 'group',
							'type'				=> 'select',
							'ljtable'			=> 'clients_groups',
              'id'          => 'group',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Status',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
							'script'			=> "$(\"#group\").change( function(){
								if($(\"#group\").val() == \"1\"){ $(\"#number_of_books\").val(\"5\");$(\"#number_of_days\").val(\"30\"); }
								else{ $(\"#number_of_books\").val(\"3\");$(\"#number_of_days\").val(\"30\"); }
								});"
						);

$fields['number_of_books'] = array(
              'name'        => 'number_of_books',
							'type'				=> 'number',
              'id'          => 'number_of_books',
              'value'       => '5',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Number of Books',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '1',
            );

$fields['number_of_days'] = array(
              'name'        => 'number_of_days',
							'type'				=> 'number',
              'id'          => 'number_of_days',
              'value'       => '30',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Number of days',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '1',
            );

$fields['overdue'] = array(
							'name'				=> 'overdue',
							'type'				=> 'select',
							'ljtable'			=> 'clients_overdues',
              'id'          => 'overdue',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Overdue status',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
							'script'			=> '',
							/*"$(\"#group\").change( function(){
								if($(\"#group\").val() == \"1\"){ $(\"#number_of_books\").val(\"5\");$(\"#number_of_days\").val(\"30\"); }
								else{ $(\"#number_of_books\").val(\"3\");$(\"#number_of_days\").val(\"30\"); }
								});"*/
						);

$fields['date_registration'] = array(
              'name'        => 'date_registration',
							'type'				=> 'text',
              'id'          => 'date_registration',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Registration date',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
							'script'			=> "$(\"#date_registration\").datepicker();",
            );

$fields['date_termination'] = array(
              'name'        => 'date_termination',
							'type'				=> 'text',
              'id'          => 'date_termination',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Termination date',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
							'script'			=> "$(\"#date_termination\").datepicker();",
            );

$fields['serial_number'] = array(
              'name'        => 'serial_number',
							'type'				=> 'text',
              'id'          => 'client_serial_number',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Old ID',
							'show_edit'		=> '1',
							'show_search' => '1',
							'show_result'		=> '0',
            );

//ID MUST be first field in array
$fields['id'] = array(
              'name'        => 'id',
							'type'				=> 'hidden',
              'id'          => 'client_id',
              'value'       => '',
              'maxlength'   => '100',
              'size'        => '10',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'New ID',
							'show_edit'		=> '1',
							'show_search' => '1',
							'show_result'		=> '1',
            );

$fields['asynctoid'] = array(
              'name'        => 'asynctoid',
							'type'				=> 'text',
              'id'          => 'client_asynctoid',
              'value'       => '',
              'maxlength'   => '32',
              'size'        => '40',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'ASyncTo ID',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

		$this->fields = load_ljtables($fields);
	}


	// #### Common Code - Do not modify here #### //
	public function get($id = FALSE)
	{
		// #### Get list of fields to query #### //
		foreach($this->fields as $f)
		{
			// Main table
			if(!array_key_exists('ljtable', $f))
				$fieldlist[] = $this->classname."s.".$f['name'];
			// Linked table
			else
			{
				$ljfieldlist[] = $f['ljtable'].".".$f['name']."_id";
				$ljoperationlist[] = array($f['ljtable'], $this->classname."s.id = ".$f['ljtable'].".".$this->classname."_id");
			}
		}

		// #### Get all items #### //
		if($id === FALSE)
		{
			$this->ldb->select(implode(',', $fieldlist));
			$this->ldb->select(implode(',', $ljfieldlist));
			$this->ldb->from($this->classname."s");
			foreach($ljoperationlist as $lj)
				$this->ldb->join($lj[0], $lj[1], "left");
			$query = $this->ldb->get();
//print($this->ldb->last_query()."<br/>");
			return $query->result_array();
		}

		// #### Get one specific record - by ID #### //
		else{
			// Get main table
			$this->ldb->select(implode(',', $fieldlist));
			$this->ldb->from($this->classname."s");
			$this->ldb->where($this->classname."s.id",$id);
			$query = $this->ldb->get();
//print($this->ldb->last_query()."<br/>");
			$result = $query->result_array();

			// No records found
			if(count($result) <1 )
				return Null;

			// Get linked tables
			foreach($this->fields as $i => $f)
			{
				if(array_key_exists('ljtable', $f))
				{
					$this->ldb->select($f['name']."_id");
					$this->ldb->from($f['ljtable']);
					$this->ldb->where(array($this->classname."_id" => $id));
					$q = $this->ldb->get();
//print($this->ldb->last_query()."<br/>");
					foreach($q->result_array() as $r)
					{
/*
						foreach($r as $r2)
*/
						$result[0][$f['name']][] = $r2[$f['name']."_id"];
					}
				}
			}
			return $result[0];
		}
	}

	public function get_id_prev($id)
	{
		$this->ldb->select("Max(id) as id");
		$this->ldb->where("id < ".$id);
		$this->ldb->limit(1);
		$q = $this->ldb->get($this->classname."s");
		$r = $q->result_array();
		return $r[0]['id'];
	}

	public function get_id_next($id)
	{
		$this->ldb->select("Min(id) as id");
		$this->ldb->where("id > ".$id);
		$this->ldb->limit(1);
		$q = $this->ldb->get($this->classname."s");
		$r = $q->result_array();

		if($r[0]['id'])
			return $r[0]['id'];
		else
			return 1;
	}

	public function set($data, $id = FALSE)
	{
//print_r($this->input->post());
//print("<br/>\n");
//die();
//print_r($data['main']);

		// #### Main table #### //
		// Update existing entry
		if($id !== FALSE)
		{
			$this->ldb->where('id', $id);
			$this->ldb->update($this->classname."s", $data['main']);
		}
		// Create new entry
		else{
			$this->ldb->insert($this->classname."s", $data['main']);
			$id = $this->ldb->insert_id();
		}

		// #### Delete entries from LJ tables #### //
		foreach($data['ljtable'] as $f)
			$qdelete[] = $f['ljtable'];
		$this->ldb->where($this->classname."_id", $id);
		$this->ldb->delete($qdelete);

		// #### Insert entries into LJ tables #### //
		foreach($data['ljtable'] as $f)
		{
			$d = null;
			if(is_array($f['value_submitted']))
			{
				foreach($f['value_submitted'] as $v)
					$d[] = array($this->classname."_id" => $id, $f['name']."_id"=>$v);
			}
			else
				$d[0] = array($this->classname."_id" => $id, $f['name']."_id"=>$f['value_submitted']);

			if(count($d) > 1)
				$this->ldb->insert_batch($f['ljtable'], $d);
			else
				$this->ldb->insert($f['ljtable'], $d[0]);

//print($this->ldb->last_query()."<br/>\n");
//die();
		}

		// Return the ID if newly generated
		return $id;
	}

	public function delete($id)
	{
		$this->ldb->where("clients.id", $id);
		$this->ldb->delete("clients");
		$this->ldb->where("books_clients.client_id", $id);
		$this->ldb->delete("books_clients");
		$this->ldb->where("clients_groups.client_id", $id);
		$this->ldb->delete("clients_groups");
	}


	function search($needles, $offset=0)
	{
//print("<pre>");
//print_r($needles);
//print("</pre>");
		$limit = 100;
		if(count($needles) > 0)
		{

			$q = array();
			$w = array();
			if(array_key_exists("search_deleted", $needles) && $needles['search_deleted']['value'] == 1)
			{
				$w[] = "date_termination < CURDATE()";
				unset($needles['search_deleted']);
			}
			else
				$w[] = "(date_termination > CURDATE() OR date_termination = '0000-00-00' OR date_termination IS NULL)";

			foreach($needles as $i => $n)
			{
				if(array_key_exists('value', $n) && $n['value'] != '')
					$q[$i] = $n['value'];
			}
			if(count($q) > 0)
			{
				// Return only fields which are required to be viewn
				foreach($this->fields as $f)
				{
					if(array_key_exists('show_result', $f) && $f['show_result'] == 1)
					{
						if(!array_key_exists('ljtable', $f))
						{
							//if($f['name'] == "id")
							//	$f['name'] = $f['name']." as ".$this->classname.$f['name'];
							$fieldlist[] = $this->classname."s.".$f['name'];
						}
					}
				}

				$this->ldb->select(implode(',', $fieldlist));
				//$this->ldb->select(array("books_clients.date_borrow", "clients.id as clientid", "CONCAT(clients.name, ' ', clients.surname, ' (', clients.address, ')'	) as client"));
				//$this->ldb->join("books_clients", "books_clients.book_id = books.id and books_clients.date_return = 0000-00-00", "left");
				//$this->ldb->join("clients", "books_clients.client_id = clients.id", "left");
				//$this->ldb->where("");
				foreach($q as $i => $n)
				{
					$this->ldb->like($i,$n, "after");
				}
				if(count($w))
					foreach($w as $n)
						$this->ldb->where($n);
				$this->ldb->from($this->classname."s");
				$this->ldb->limit($limit);
				$this->ldb->offset($offset);
				$query = $this->ldb->get();
				$result = $query->result_array();
//print($this->ldb->last_query());
//print("<br/>\n");

				// Look up LJTables
				foreach($result as $i => $r)
				{
					// Return only fields which are required to be viewn
					foreach($this->fields as $f)
					{
						if(array_key_exists('ljtable', $f) && array_key_exists('show_result', $f) && $f['show_result'] == 1)
						{
							$this->ldb->select($f['name']."s.name as ".$f['name']);
							$this->ldb->from($f['ljtable']);
							$this->ldb->join($f['name']."s", $f['ljtable'].".".$f['name']."_id = ".$f['name']."s.id", "left");
							$this->ldb->where($f['ljtable'].".book_id", $r['id']);
							$query = $this->ldb->get();
print($this->ldb->last_query());
print("<br/>\n");
							$ljr = $query->result_array();
							if(is_array($ljr) && count($ljr) > 0)
								$result[$i][$f['name']] = $ljr;
							else
								$result[$i][$f['name']] = NULL;
						}
					}
				}

			return $result;
			}
		}
		return array();
	}

	function get_books($id = FALSE, $history = FALSE)
	{
		$this->load->model('Books_model');
		$bf = $this->Books_model->fields;

		foreach($bf as $i => $f)
		{
			if(array_key_exists('show_edit', $f) && $f['show_edit'] == 1)
			{
				// Main table
				if(!array_key_exists('ljtable', $f))
					$fieldlist[] = "books.".$f['name'];
				// Linked table
				else
				{
/*					// ### Special code / check before Copy-Pesto ### //
					// litteraryform.name as litteraryform
					$ljfieldlist[] = $f['name']."s.name as ".$f['name'];
					// LJ books_litteraryforms on books.id = books_litteraryforms.book_id
					$ljoperationlist[] = array($f['ljtable'], "books.id = ".$f['ljtable'].".book_id");
					// LJ litteraryforms on books_litteraryforms.litteraryform_id = litteraryforms.id
					$ljoperationlist[] = array($f['name']."s", $f['ljtable'].".".$f['name']."_id = ".$f['name']."s.id");
*/

				if(array_key_exists('ljtable', $f) && $f['show_result'] == 1)
				{
					$strSelect = "(SELECT GROUP_CONCAT(".$f['name']."s.name)\n";
					$strSelect .= "FROM ".$f['ljtable']."\n";
					$strSelect .= "LEFT JOIN ".$f['name']."s ON ". $f['ljtable'].".".$f['name']."_id = ".$f['name']."s.id\n";
					$strSelect .= "WHERE ".$f['ljtable'].".book_id = books.id\n";
					$strSelect .= ") as ".$f['name'];
					$this->ldb->select($strSelect);
				}

				}
			}
		}

		if($id !== FALSE)
		{
			$fieldlist[] = "books_clients.date_borrow";
			$fieldlist[] = "books_clients.date_due";
			$fieldlist[] = "books_clients.date_reminder";
			$this->ldb->select(implode(',', $fieldlist));
			$this->ldb->join("books", "books.id = books_clients.book_id", "left");
			if(isset($ljoperationlist) && is_array($ljoperationlist) && count($ljoperationlist)>0)
			{
				$this->ldb->select(implode(',', $ljfieldlist));
				foreach($ljoperationlist as $lj)
					$this->ldb->join($lj[0], $lj[1], "left");
			}


			//$this->ldb->select(array("authors.name as author_name", "authors.id as author_id"));
			//$this->ldb->join("authors_books", "authors_books.book_id = books.id", "left");
			//$this->ldb->join("authors", "authors_books.author_id = authors.id", "left");
			$strQuery = "(
				SELECT group_concat(`languages`.`name`)
				FROM books_languages
				LEFT JOIN `languages` ON `books_languages`.`language_id` = `languages`.`id`
				 WHERE `books_languages`.`book_id` = `books`.`id`
			) AS language,
			(
				SELECT GROUP_CONCAT(`authors`.`id` SEPARATOR ';')
				FROM authors_books
				LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
				 WHERE `authors_books`.`book_id` = `books`.`id`
			)  AS author_id,
			(
				SELECT GROUP_CONCAT(`authors`.`name` SEPARATOR ';')
				FROM authors_books
				LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
				 WHERE `authors_books`.`book_id` = `books`.`id`
			)  AS author_name\n";

		$this->ldb->select($strQuery);


			$this->ldb->group_by("books.id");

			$this->ldb->from("books_clients");
			if($history == 1)
				$this->ldb->where("books_clients.client_id", $id);
			else
				$this->ldb->where(array("books_clients.client_id" => $id, "books_clients.date_return" => "0000-00-00"));
			$query = $this->ldb->get();

//print($this->ldb->last_query()."<br/>");

			$r = $query->result_array();

//print_r($r);
//print("<br/>");

			return $r;
		}
	}

	function validate_client($id, $name)
	{
		if($name == FALSE || $id == FALSE)
			return false;
		$this->ldb->select(array("id"));
		$this->ldb->where("id = '".$id."' AND aurovillename LIKE '".$name."'");
		$this->ldb->from($this->classname."s");
		$this->ldb->limit(1);
		$query = $this->ldb->get();
		$r = $query->result_array();
		if($r[0]['id'] == $id)
			return true;
		else
			return false;
	}

	function ajaxsearchclient($needle)
	{
		$this->ldb->select(array("id", "aurovillename", "name", "surname", "serial_number"));
		$this->ldb->where("(date_termination = '0000-00-00' OR date_termination > NOW()) AND (`id` like '".$needle."%' OR `name` like '".$needle."%' OR `surname` like '".$needle."%' OR `serial_number` like '".$needle."%')");
		$this->ldb->from($this->classname."s");
		$this->ldb->limit(20);
		$query = $this->ldb->get();
//print($this->ldb->last_query());
//print("<br/>\n");
		$r = $query->result_array();
		if(is_array($r) && count($r) > 0)
		{
			foreach($r as $res)
			{
				//$ret[] = array('label' => $res['aurovillename']." - ".$res['name']." ".$res['surname']." (ID ".$res['id']." - old ID ".$res['serial_number'].")", 'value' => $res['id']);
				$ret[] = array('label' => $res['aurovillename']." - ".$res['name']." ".$res['surname'], 'value' => $res['id']);
			}
			return $ret;
		}
		else
			return null;
	}

	function ajaxsearch($needle, $haystack)
	{
		$this->ldb->distinct();
		$this->ldb->select($haystack);
		$this->ldb->where("(`".$haystack."` like '".$needle."%')");
		$this->ldb->order_by($haystack, "asc");
		$this->ldb->from($this->classname."s");
		$this->ldb->limit(20);
		$query = $this->ldb->get();
		$r = $query->result_array();

		if(is_array($r) && count($r) > 0)
		{
			foreach($r as $res)
			{
				$ret[] = array('label' => $res[$haystack], 'value' => $res[$haystack]);
			}
			return $ret;
		}
		else
			return null;
	}


	function get_client_borrowhistory($id, $limit=100)
	{
		$strQuery = "SELECT `books`.`id`, `books`.`serial_number`, `books`.`title`, `books`.`remainder_of_title`, `books`.`parallel_title`, `books`.`original_title`, `books`.`book_number`, `books_clients`.`date_borrow`, `books_clients`.`date_due`, `books_clients`.`date_return`,";
		$strQuery .= "\n(
  SELECT group_concat(`languages`.`name`)
  FROM books_languages
  LEFT JOIN `languages` ON `books_languages`.`language_id` = `languages`.`id`
   WHERE `books_languages`.`book_id` = `books`.`id`
) AS language,
(
  SELECT GROUP_CONCAT(`authors`.`id` SEPARATOR ';')
  FROM authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   WHERE `authors_books`.`book_id` = `books`.`id`
)  AS author_id,
(
  SELECT GROUP_CONCAT(`authors`.`name` SEPARATOR ';')
  FROM authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   WHERE `authors_books`.`book_id` = `books`.`id`
)  AS author_name\n";

		$strQuery .= "FROM books_clients\n";

		$strQuery .= "LEFT JOIN `books` ON `books_clients`.`book_id` = `books`.`id`
									WHERE books_clients.client_id = ".$id."
									ORDER BY books_clients.date_borrow DESC\n";

		if($limit !== FALSE)
			$strQuery .= "Limit ".$limit;

		$query = $this->ldb->query($strQuery);
		return $query->result_array();

/*		$this->ldb->select(array("books_clients.date_borrow", "books_clients.date_due", "books_clients.date_return", "books.id", "books.serial_number", "books.title"));
		$this->ldb->join("books", "books_clients.book_id = books.id", "left");
		$this->ldb->from("books_clients");
		$this->ldb->where("books_clients.client_id = ".$id);
		$this->ldb->order_by("books_clients.date_borrow", "DESC");
		$query = $this->ldb->get();
		return $query->result_array();*/
	}

	function setlongoverdue($clientid)
	{
		$this->ldb->query("REPLACE INTO `clients_overdues` (`client_id`, `overdue_id`) VALUES ('".$clientid."', '2')");
	}
}