<?php
class Lists_model extends CI_Model {

	public $ldb;

	public function singular($list)
	{
		return substr($list, 0, strlen($list)-1);
	}
	
	public function __construct()
	{
		$this->load->database();
		$this->load->helper('avlib');
    $this->ldb = $this->load->database(get_current_database(), TRUE);
  }
	
	public function search($fields)
	{
		$list = $fields['list']['value'];
		$name = $fields['name']['value'];
		$notes = $fields['notes']['value'];

		if(!$name && !$notes)
			return array();
		
		//print("search for ".$name." - ".$notes);
		
		$this->ldb->select($list.".id as ".$this->singular($list)."id, ".$list.".name as ".$this->singular($list).", ".$list.".notes, count(books_".$list.".id) as bookcount");
		$this->ldb->from($list);
		$this->ldb->join("books_".$list, "books_".$list.".".$this->singular($list)."_id = ".$list.".id", "left");
		$this->ldb->where("name like '%".$name."%' and notes like '%".$notes."%'");
		$this->ldb->group_by($list.".id");
		$q = $this->ldb->get();
		//print($this->ldb->last_query());
		return $q->result_array();
	}
  
  public function load($list, $id)
  {
    $q = $this->ldb->get_where($list, "id = ".$id);
    //print_r($this->ldb->last_query());
    $r = $q->result_array();
		return $r[0];
  }
  
	public function get_id_prev($list, $id)
	{
		$this->ldb->select("Max(id) as id");
		$this->ldb->where("id < ".$id);
		$this->ldb->limit(1);
		$q = $this->ldb->get($list);
		$r = $q->result_array();
		return $r[0]['id'];
	}
	
	public function get_id_next($list, $id)
	{
		$this->ldb->select("Min(id) as id");
		$this->ldb->where("id > ".$id);
		$this->ldb->limit(1);
		$q = $this->ldb->get($list);
		$r = $q->result_array();
		return $r[0]['id'];
	}
	
  public function load_associated_books($list, $id)
  {

		if(is_admin())
		{
			$df = "`delete_flag` >= '0'";
			//$df = "`delete_flag` = '1'";
			//unset($needles['search_deleted']);
		}
		else
			$df = "`delete_flag` = '0'";

		$strQuery = "SELECT books.id, books.serial_number, books.book_number, dewey_decimal_classification, books.title, books.remainder_of_title, books.part_x, books.parallel_title, books.original_title, books.delete_flag\n";
//		, (
//  SELECT group_concat(`languages`.`name`)
//  FROM books_languages
//  LEFT JOIN `languages` ON `books_languages`.`language_id` = `languages`.`id`
//   WHERE `books_languages`.`book_id` = `books`.`id`
//	) AS language\n";
	//, GROUP_CONCAT(authors.id SEPARATOR ';') as author_id, GROUP_CONCAT(authors.name SEPARATOR ';') as author_name
	//";
		$strQuery .= "FROM books_".$list."\n";
		$strQuery .= "LEFT JOIN books ON books.id = books_".$list.".book_id\n";
		//$strQuery .= "LEFT JOIN books_languages ON books_languages.book_id = books.id\n";
		//$strQuery .= "LEFT JOIN languages ON languages.id = books_languages.language_id\n";
		//$strQuery .= "LEFT JOIN books_clients ON books_clients.book_id = books.id and books_clients.date_return = 0000-00-00\n";
		//$strQuery .= "LEFT JOIN clients ON books_clients.client_id = clients.id\n";
		//$strQuery .= "LEFT JOIN authors_books ON authors_books.book_id = books.id\n";
		//$strQuery .= "LEFT JOIN authors ON authors.id = authors_books.author_id\n";
		

		$strQuery .= "WHERE books_".$list.".".$this->singular($list)."_id = ".$id."\n";
		$strQuery .= "AND ".$df;
		$strQuery .= "GROUP BY books.id\n";

		$q = $this->ldb->query($strQuery);
		return $q->result_array();
  }
	
	public function save($list, $id, $name, $notes)
	{
		$this->ldb->where("id", $id);
		$this->ldb->update($list, array("name" => $name, "notes" => $notes));
		
		print($this->ldb->last_query());
	}
	
	public function delete($list, $id)
	{
		if($list && $id)
		{
			// Delete list
			$this->ldb->limit(1);
			$this->ldb->delete($list, array("id" => $id));
			// Delete list item in book
			$this->ldb->delete("books_".$list, array($this->singular($list)."_id" => $id));
		}
	}
}