<?php
class Authors_model extends CI_Model {

	public $fields;
	private $classname = 'author';
	public $ldb;

	public function __construct()
	{
		//$ci = get_instance();
		$this->load->helper('avlib');
		$this->load->helper('html');
		$this->load->database();

		$this->ldb = $this->load->database(get_current_database(), TRUE);

		$this->author_name_string = "authors.name as author_name";

// ID MUST be first field in array
$this->fields['id'] = array(
              'name'        => 'id',
							'type'				=> 'hidden',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '100',
              'size'        => '10',
              'style'       => '',
							'js'					=> '',
							'label'				=> '',
							'show_edit'		=> '0',
							'show_search' => '0',
							'show_view'		=> '1',
            );

$this->fields['name'] = array(
              'name'        => 'name',
							'type'				=> 'text',
              'id'          => 'author_name',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Name',
							'show_edit'		=> '1',
							'show_search' => '1',
							'show_view'		=> '0',
            );

$this->fields['title'] = array(
              'name'        => 'title',
							'type'				=> 'text',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Title',
							'show_edit'		=> '1',
							'show_search' => '1',
							'show_view'		=> '0',
            );

$this->fields['name_fuller_form'] = array(
              'name'        => 'name_fuller_form',
							'type'				=> 'text',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Fuller form of the name',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_view'		=> '0',
            );

$this->fields['lifetime'] = array(
              'name'        => 'lifetime',
							'type'				=> 'text',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Lifetime',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_view'		=> '1'
            );


$this->fields['notes'] = array(
              'name'        => 'notes',
							'type'				=> 'textarea',
              'id'          => 'author_notes',
              'value'       => '',
              'maxlength'   => '',
              'cols'				=> '53',
							'rows'				=> '4',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Notes',
							'show_edit'		=> '1',
							'show_search' => '1',
							'show_view'		=> '0',
							'script'			=> "$(\"#author_notes\").autocomplete({
																	source: function(request, response) {
																		$.ajax({
																			url: \"/library/authors/ajaxsearch/\",
																			data: { term: $(\"#author_notes\").val(), field: \"notes\"},
																			dataType: \"json\",
																			type: \"POST\",
																			success: function(data){
																				response(data);
																			}
																		});
																	},
																	minLength: 1,
																});"
            );

		//$this->load_ljtables();
		$this->fields = load_ljtables($this->fields);
	}

//	public function load_ljtables()
//	{
//		foreach($this->fields as $i => $f)
//			if(array_key_exists('ljtable', $f))
//			{
//				$q = $this->ldb->get($f['name']."s");
//				foreach($q->result_array() as $r)
//					$this->fields[$i]['options'][$r['id']] = $r['name'];
//				//$this->fields[$i]['values'] = $q->result_array();
////print_r($this->fields[$i]['options']);
//			}
//	}

	public function get($id = FALSE)
	{
//		$fieldlist[] = $this->classname."s.id";
		foreach($this->fields as $f)
		{
			if(!array_key_exists('ljtable', $f))
				$fieldlist[] = $this->classname."s.".$f['name'];
			else
			{
				$ljfieldlist[] = $f['ljtable'].".".$f['name']."_id";
				$ljoperationlist[] = array($f['ljtable'], $this->classname."s.id = ".$f['ljtable'].".".$this->classname."_id");
			}
		}

		// #### Get all clients #### //
		if($id === FALSE){
//			$query = $this->ldb->get($this->classname."s");
//			return $query->result_array();
			$this->ldb->select(implode(',', $fieldlist));
			if(is_set($ljfieldlist))
				$this->ldb->select(implode(',', $ljfieldlist));
			$this->ldb->from($this->classname."s");
			foreach($ljoperationlist as $lj)
				$this->ldb->join($lj[0], $lj[1], "left");
			$query = $this->ldb->get();
print($this->ldb->last_query()."<br/>");
			return $query->result_array();
		}

		// #### ID Provided #### //
		else{
			//$query = $this->ldb->get_where($this->classname."s", array('id' => $id));
			//return $query->row_array();

			// Get main table
			$this->ldb->select(implode(',', $fieldlist));
			$this->ldb->from($this->classname."s");
			$this->ldb->where($this->classname."s.id",$id);
			$query = $this->ldb->get();
//print($this->ldb->last_query()."<br/>");
			$result = $query->result_array();

			// Get linked tables
			foreach($this->fields as $i => $f)
			{
				if(array_key_exists('ljtable', $f))
				{
					$this->ldb->select($f['name']."_id");
					$this->ldb->from($f['ljtable']);
					$this->ldb->where(array($this->classname."_id" => $id));
					$q = $this->ldb->get();
//print($this->ldb->last_query()."<br/>");
					foreach($q->result_array() as $r)
//						foreach($r as $r2)
							$result[0][$f['name']][] = $r2[$f['name']."_id"];
				}
			}
			return $result[0];
		}
	}

	public function get_id_prev($id)
	{
		$this->ldb->select("Max(id) as id");
		$this->ldb->where("id < ".$id);
		$this->ldb->limit(1);
		$q = $this->ldb->get($this->classname."s");
		$r = $q->result_array();
		return $r[0]['id'];
	}

	public function get_id_next($id)
	{
		$this->ldb->select("Min(id) as id");
		$this->ldb->where("id > ".$id);
		$this->ldb->limit(1);
		$q = $this->ldb->get($this->classname."s");
		$r = $q->result_array();
		return $r[0]['id'];
	}

	public function set($data, $id = FALSE)
	{
//print_r($this->input->post());
//print("<br/>\n");

//print_r($this->fields);
//print("<br/>\n");
//die();

//print_r($data);
//print("<br/>\n");
//print_r($inserts);
//print("<br/>\n");
foreach($data['main'] as $i => $v)
{
	$data['main'][$i] = empty($v) ? NULL : $v;
}

		if($id !== FALSE)
		{
			// Update main table
			$this->ldb->where('id', $id);
			$this->ldb->update($this->classname."s", $data['main']);
			//$this->ldb->insert($this->classname."s", $data['main']);
//print_r($this->input->post());
//print("<hr>".$this->ldb->last_query()."<hr><br/>");
//die();

		}
		else{
			$this->ldb->insert($this->classname."s", $data['main']);
			$id = $this->ldb->insert_id();
		}


		if(array_key_exists("ljtable", $data))
		{
			// Delete entries from LJ tables
			foreach($data['ljtable'] as $f)
				$qdelete[] = $f['ljtable'];
			$this->ldb->where($this->classname."_id", $id);
			$this->ldb->delete($qdelete);

			// Insert entries into LJ tables
			foreach($data['ljtable'] as $f)
			{
				$d = null;
				if(is_array($f['value_submitted']))
				{
					foreach($f['value_submitted'] as $v)
						$d[] = array($this->classname."_id" => $id, $f['name']."_id"=>$v);
				}
				else
					$d[0] = array($this->classname."_id" => $id, $f['name']."_id"=>$f['value_submitted']);

				if(count($d) > 1)
					$this->ldb->insert_batch($f['ljtable'], $d);
				else
					$this->ldb->insert($f['ljtable'], $d[0]);

//print("<hr>".$this->ldb->last_query()."<hr><br/>");
//die();
			}
		}

		// Return the ID if newly generated
		return $id;
	}



	function search($needles, $offset=0)
	{
		$limit = 100;
		if(count($needles) > 0)
		{
			$q = array();
			foreach($needles as $i => $n)
			{
				if(array_key_exists('value', $n) && $n['value'] != '')
					$q[$i] = $n['value'];
			}
			if(count($q) > 0)
			{
				// Return only fields which are required to be viewn
				foreach($this->fields as $f)
				{
					if(array_key_exists('show_view', $f) && $f['show_view'] == 1)
					{
						if(!array_key_exists('ljtable', $f))
						{
							//if($f['name'] == "id")
							//	$f['name'] = $f['name']." as ".$this->classname.$f['name'];
							$fieldlist[] = $this->classname."s.".$f['name'];
						}
					}
				}

			//
			//$this->ldb->select(array("books_clients.date_borrow", "books_clients.date_due", "books_clients.date_return", "books.id", "books.serial_number", "books.title"));
			//$this->ldb->join("books", "books_clients.book_id = books.id", "left");
			//$this->ldb->from("books_clients");
			//$this->ldb->where("books_clients.client_id = ".$id);
			//$this->ldb->order_by("books_clients.date_borrow", "DESC");
			//
			//
			//

				$this->ldb->select(implode(',', $fieldlist));
				$this->ldb->select(array($this->author_name_string, "authors.id as author_id"));
				//$this->ldb->join("books_clients", "books_clients.book_id = books.id and books_clients.date_return = 0000-00-00", "left");
				//$this->ldb->join("clients", "books_clients.client_id = clients.id", "left");
				//$this->ldb->where("");
				$this->ldb->like($q);
				$this->ldb->from($this->classname."s");
				$this->ldb->limit($limit);
				$this->ldb->offset($offset);
				$query = $this->ldb->get();
				$result = $query->result_array();
//print($this->ldb->last_query());
//print("<br/>\n");

				// Look up LJTables
				foreach($result as $i => $r)
				{
					// Return only fields which are required to be viewn
					foreach($this->fields as $f)
					{
						if(array_key_exists('ljtable', $f) && array_key_exists('show_view', $f) && $f['show_view'] == 1)
						{
							$this->ldb->select($f['name']."s.name as ".$f['name']);
							$this->ldb->from($f['ljtable']);
							$this->ldb->join($f['name']."s", $f['ljtable'].".".$f['name']."_id = ".$f['name']."s.id", "left");
							$this->ldb->where($f['ljtable'].".book_id", $r['id']);
							$query = $this->ldb->get();
//print($this->ldb->last_query());
//print("<br/>\n");
							$ljr = $query->result_array();
							if(is_array($ljr) && count($ljr) > 0)
								$result[$i][$f['name']] = $ljr;
							else
								$result[$i][$f['name']] = NULL;
						}
					}
				}

			return $result;
			}
		}
		return array();
	}

	function ajaxsearchauthor($needle)
	{
		$this->ldb->select(array("id", "name", "name_fuller_form", "notes", "title", "lifetime"));
		$this->ldb->where("(`id` like '".addslashes($needle)."%' OR `name` like '".addslashes($needle)."%' OR `name_fuller_form` like '".addslashes($needle)."%')");
		$this->ldb->from($this->classname."s");
		$this->ldb->limit(20);
		$q = $this->ldb->get();
//print($this->ldb->last_query());
//print("<br/>\n");
		$r = $q->result_array();
//print_r($r);
//die();

		if(is_array($r) && count($r) > 0)
		{
			foreach($r as $res)
			{
				if(isset($res['lifetime']) && $res['lifetime'] != "" )
					$res['lifetime'] = " (".$res['lifetime'].")";
				if(isset($res['notes']) && $res['notes'] != "")
					$res['notes'] = " ".$res['notes'];
				if(isset($res['title']) && $res['title'] != "")
					$res['title'] = ", ".$res['title'];
				if(isset($res['name_fuller_form']) && $res['name_fuller_form'] != "")
					$res['name_fuller_form'] = ", ".$res['name_fuller_form'];
				$strRes = $res['name'].$res['notes'].$res['lifetime'].$res['title'].$res['name_fuller_form'];

				if(strlen($strRes) > 40)
					$strRes = substr($strRes,0,37)."...";
				// convert to UTF-8 (required for tamil characters)
				$ret[] = array('label' => mb_convert_encoding($strRes, "UTF-8"), 'value' => $res['id']);
				//print_r($ret);
			}
			return $ret;
		}
		else
			return array("name" => "No result found");
	}

	function ajaxaddrole($bookid, $personid, $roleid)
	{
		$this->ldb->insert("authors_books", array("book_id" => addslashes($bookid), "author_id" => addslashes($personid), "role_id" => addslashes($roleid)));
		return true;
	}

	function ajaxremoverole($bookid, $personid, $roleid)
	{
		$this->ldb->delete("authors_books", array("book_id" => addslashes($bookid), "author_id" => addslashes($personid), "role_id" => addslashes($roleid)));
		return true;
	}

	function ajaxlistroles($bookid)
	{
			$data['labels'] = array("author_name" => "Name", "lifetime" => "Lifetime", "notes" => "Notes", "role" =>"Role", "action"=> "");
			$data['items'] = $this->get_authors_for_book($bookid);
			foreach($data['items'] as $i => $v)
			{
//print_r($v);
//print("<br/>");
				// deletePersonFromBook(bookid, personid, roleid)
				$data['items'][$i]['author_name'] = "<a href=\"/library/authors/edit/".$v['id']."\">".$data['items'][$i]['author_name']."</a>";
				$data['items'][$i]['action'] = "<a href=\"javascript:deletePersonFromBook('".$bookid."', '".$v['id']."', '".$v['roleid']."');\">".img("assets/img/cross.png")."</a>";
			}
			return $data;
	}

	function ajaxsearch($needle, $haystack)
	{
		$this->ldb->distinct();
		$this->ldb->select($haystack);
		$this->ldb->where("(`".addslashes($haystack)."` like '".addslashes($needle)."%')");
		$this->ldb->order_by($haystack, "asc");
		$this->ldb->from($this->classname."s");
		$this->ldb->limit(20);
		$query = $this->ldb->get();
		$r = $query->result_array();

		if(is_array($r) && count($r) > 0)
		{
			foreach($r as $res)
			{
				$ret[] = array('label' => $res[$haystack], 'value' => $res[$haystack]);
			}
			return $ret;
		}
		else
			return null;
	}

	function get_authors_for_book($bookid)
	{
		$this->ldb->select(array("authors.id", $this->author_name_string, "roles.name as role", "authors_books.role_id as roleid", "authors.lifetime", "authors.notes"));
		//$this->ldb->where("(`id` like '".$needle."%' OR `name_first` like '".$needle."%' OR `name_last` like '".$needle."%' OR `name_fuller_form` like '".$needle."%')");
		$this->ldb->from("authors_books");
		$this->ldb->join("authors", "authors.id = authors_books.author_id", "left");
		$this->ldb->join("roles", "roles.id = authors_books.role_id", "left");
		$this->ldb->where(array("book_id" => $bookid));
		$this->ldb->limit(20);
		$query = $this->ldb->get();
//print($this->ldb->last_query());
//print("<br/>\n");
		return $query->result_array();
//print_r($r);
		//return array(0 => array("id" => "1", "author_name"=>"Karl Marx", "function"=>"Author"), 1=> array("id" => "2", "author_name"=>"VL Lenin", "function"=>"Co-author"));
	}

	function get_author_books($authorid)
	{
		//$this->ldb->select(array("authors.id", "roles.name as role", "books.title", "books.remainder_of_title", "languages.name as language", "books.id", "books.serial_number", "books.book_number"));
		//$this->ldb->select("SELECT IF((SELECT date_return FROM books_clients WHERE books_clients.book_id = books.id ORDER BY books_clients.id DESC LIMIT 1) = '0000-00-00', IN, OUT ) AS date_return");
		//$this->ldb->from("authors");
		//$this->ldb->join("authors_books", "authors_books.author_id = authors.id", "left");
		//$this->ldb->join("roles", "roles.id = authors_books.role_id", "left");
		//$this->ldb->join("books", "books.id = authors_books.book_id", "left");
		//$this->ldb->join("books_languages", "books_languages.book_id = books.id", "left");
		//$this->ldb->join("languages", "languages.id = books_languages.language_id", "left");
		//$this->ldb->where(array("authors.id" => $authorid));

		if(is_admin())
		{
			$df = "`delete_flag` >= '0'";
			//$df = "`delete_flag` = '1'";
			//unset($needles['search_deleted']);
		}
		else
			$df = "`delete_flag` = '0'";

		$strQuery = "SELECT authors.id, roles.name as role, dewey_decimal_classification, books.title, books.remainder_of_title, books.part_x, books.parallel_title, books.original_title, books.delete_flag, (
  SELECT group_concat(`languages`.`name`)
  FROM books_languages
  LEFT JOIN `languages` ON `books_languages`.`language_id` = `languages`.`id`
   WHERE `books_languages`.`book_id` = `books`.`id`
	) AS language, books.id, books.serial_number, books.book_number,
	`clients`.`id` as clientid, CONCAT(clients.name, ' ', clients.surname, ' (', clients.address, ')' ) as client, books_clients.date_due, ";
		$strQuery .= "(SELECT IF((SELECT date_return FROM books_clients WHERE books_clients.book_id = books.id ORDER BY books_clients.id DESC LIMIT 1) = '0000-00-00', \"OUT\", \"IN\")) AS book_status\n";
		$strQuery .= "FROM authors\n";
		$strQuery .= "LEFT JOIN authors_books ON authors_books.author_id = authors.id\n";
		$strQuery .= "LEFT JOIN roles ON roles.id = authors_books.role_id\n";
		$strQuery .= "LEFT JOIN books ON books.id = authors_books.book_id\n";
		$strQuery .= "LEFT JOIN books_languages ON books_languages.book_id = books.id\n";
		$strQuery .= "LEFT JOIN languages ON languages.id = books_languages.language_id\n";
		$strQuery .= "LEFT JOIN books_clients ON books_clients.book_id = books.id and books_clients.date_return = 0000-00-00\n";
		$strQuery .= "LEFT JOIN clients ON books_clients.client_id = clients.id\n";


		$strQuery .= "WHERE authors.id = ".$authorid."\n";
		$strQuery .= "AND ".$df;
		$strQuery .= "GROUP BY books.id\n";


		$query = $this->ldb->query($strQuery);


		//$query = $this->ldb->get();
//print($this->ldb->last_query());
//print("<br/>\n");
//print_r($query->result_array());
//die();
		return $query->result_array();
	}

	function load_ljtables($fields)
	{
		foreach($fields as $i => $f)
			if(array_key_exists('ljtable', $f))
			{
				$q = $this->ldb->get($f['name']."s");
				foreach($q->result_array() as $r)
					$fields[$i]['options'][$r['id']] = $r['name'];
				//$this->fields[$i]['values'] = $q->result_array();
//print_r($this->fields[$i]['options']);
			}
		return $fields;
	}

	function get_quickadd_fields($bookid)
	{
		$asf['bookid'] = array(
				'name'        => 'bookid',
				'type'				=> 'hidden',
				'id'          => 'bookid',
				'value'       => $bookid,
				'maxlength'   => '',
				'size'        => '',
				'style'       => '',
				'js'					=> '',
				'label'				=> '',
				'show_edit'		=> '0',
				'show_search' => '1',
				'show_view'		=> '0',
				'script'			=> ""
			);

			$asf['author'] = array(
				'name'        => 'author',
				'type'				=> 'text',
				'id'          => 'personid',
				'value'       => '',
				'maxlength'   => '',
				'size'        => '',
				'style'       => '',
				'js'					=> '',
				'label'				=> 'Name',
				'show_edit'		=> '0',
				'show_search' => '1',
				'show_view'		=> '0',
				'script'			=> ""
			);

			$asf['role'] = array(
				'name'        => 'role',
				'type'				=> 'select',
				'ljtable'			=> 'authors_books',
				'id'          => 'roleid',
				'value'       => '',
				'maxlength'   => '',
				'size'        => '',
				'style'       => '',
				'js'					=> '',
				'label'				=> 'Role',
				'show_edit'		=> '0',
				'show_search' => '1',
				'show_view'		=> '0',
				'script'			=> ""
			);

			$asf['submit'] = array(
				'name'        => 'submit',
				'type'				=> 'button',
				'id'          => 'quickaddsubmit',
				'value'       => 'add',
				'maxlength'   => '',
				'size'        => '',
				'style'       => '',
				'onClick'			=> "addPersonToBook($('#bookid').val(), $('#personid').val(), $('#roleid').val());",
				'label'				=> '',
				'show_edit'		=> '0',
				'show_search' => '1',
				'show_view'		=> '0',
			);

		return $asf;
	}

}