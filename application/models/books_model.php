<?php
class Books_model extends CI_Model {

	public $fields;
	private $classname = 'book';
	public $ldb;

	public function __construct()
	{
		$this->load->database();
		$this->load->helper('avlib');
		$this->load->helper('html');

		$this->ldb = $this->load->database(get_current_database(), TRUE);

// ID MUST be first field in array
$this->fields['id'] = array(
              'name'        => 'id',
							'type'				=> 'hidden',
              'id'          => 'bookid',
              'value'       => '',
              'maxlength'   => '100',
              'size'        => '10',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'ID',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'	=> '1',
            );

$this->fields['serial_number'] = array(
              'name'        => 'serial_number',
							'type'				=> '',
              'id'          => 'serial_number',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '10',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Barcode',
							'show_edit'		=> '1',
							'show_search' => '1',
							'show_result'		=> '1',
            );

$this->fields['title'] = array(
              'name'        => 'title',
							'type'				=> '',
              'id'          => 'book_title',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Title',
							'show_edit'		=> '1',
							'show_search' => '1',
							'show_result'		=> '1',
            );

$this->fields['remainder_of_title'] = array(
              'name'        => 'remainder_of_title',
							'type'				=> '',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Remainder of title',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '1',
            );

$this->fields['parallel_title'] = array(
              'name'        => 'parallel_title',
							'type'				=> 'textarea',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
							'cols'				=> '53',
							'rows'				=> '2',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Parallel title',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '1',
            );


$this->fields['original_title'] = array(
              'name'        => 'original_title',
							'type'				=> 'textarea',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
							'cols'				=> '53',
							'rows'				=> '2',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Original title',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '1',
            );

$this->fields['part_x'] = array(
              'name'        => 'part_x',
							'type'				=> '',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Part ...',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '1',
            );

$this->fields['part_of'] = array(
              'name'        => 'part_of',
							'type'				=> 'number',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> '... of',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['statement_of_responsability'] = array(
              'name'        => 'statement_of_responsability',
							'type'				=> '',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Statement of responsability',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['author'] = array(
              'name'        => 'author',
							'type'				=> 'lookup',
							'ljtable'			=> 'authors_books',
              'id'          => 'lookup_author',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Author & other people',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'	=> '0',
							'script'			=> '$( "#personid" ).autocomplete({
																	source: function(request, response) {
																	$.ajax({ url: "/library/authors/ajaxsearchauthor/",
																		data: { term: $("#personid").val()},
																		dataType: "json",
																		type: "POST",
																		success: function(data){
																			response(data);
																			}
																		});
																	},
																	minLength: 1,
																});'
																);

$this->fields['language'] = array(
              'name'        => 'language',
							'type'				=> 'multiselect',
							'ljtable'			=> 'books_languages',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Language',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '1',
            );

$this->fields['publication_publisher'] = array(
              'name'        => 'publication_publisher',
							'type'				=> '',
              'id'          => 'publication_publisher',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Publisher',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
							'script'			=> "$(\"#publication_publisher\").autocomplete({
																	source: function(request, response) {
																		$.ajax({
																			url: \"/library/books/ajaxsearch/\",
																			data: { term: $(\"#publication_publisher\").val(), field: \"publication_publisher\"},
																			dataType: \"json\",
																			type: \"POST\",
																			success: function(data){
																				response(data);
																			}
																		});
																	},
																	minLength: 1,
																});",
							);

$this->fields['publication_place'] = array(
              'name'        => 'publication_place',
							'type'				=> '',
              'id'          => 'publication_place',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Publication place',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
							'script'			=> "$(\"#publication_place\").autocomplete({
																	source: function(request, response) {
																		$.ajax({
																			url: \"/library/books/ajaxsearch/\",
																			data: { term: $(\"#publication_place\").val(), field: \"publication_place\"},
																			dataType: \"json\",
																			type: \"POST\",
																			success: function(data){
																				response(data);
																			}
																		});
																	},
																	minLength: 1,
																});",
            );

$this->fields['publication_year'] = array(
              'name'        => 'publication_year',
							'type'				=> '',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Publication year',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );
/*
$this->fields['additionalmaterial'] = array(
							'name'				=> 'additionalmaterial',
							'type'				=> 'multiselect',
							'ljtable'			=> 'additionalmaterials_books',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Additional materials',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
						);
*/
$this->fields['edition_statement'] = array(
              'name'        => 'edition_statement',
							'type'				=> '',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Edition statement',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['collation'] = array(
              'name'        => 'collation',
							'type'				=> '',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Collation',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['size'] = array(
              'name'        => 'size',
							'type'				=> 'number',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Size (cm)',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['physicaldetail'] = array(
							'name'				=> 'physicaldetail',
							'type'				=> 'select',
							'ljtable'			=> 'books_physicaldetails',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Binding',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
						);

$this->fields['feature'] = array(
							'name'				=> 'feature',
							'type'				=> 'lookup',
							'ljtable'			=> 'books_features',
              'id'          => 'lookup_feature',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Features',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
							'subform'			=> array(

							)
						);

$this->fields['ISBN'] = array(
              'name'        => 'ISBN',
							'type'				=> '',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'ISBN',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['ISSN'] = array(
              'name'        => 'ISSN',
							'type'				=> '',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'ISSN',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['series_name'] = array(
              'name'        => 'series_name',
							'type'				=> '',
              'id'          => 'series_name',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Series Name',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
							'script'			=> "$(\"#series_name\").autocomplete({
																	source: function(request, response) {
																		$.ajax({
																			url: \"/library/books/ajaxsearch/\",
																			data: { term: $(\"#series_name\").val(), field: \"series_name\"},
																			dataType: \"json\",
																			type: \"POST\",
																			success: function(data){
																				response(data);
																			}
																		});
																	},
																	minLength: 1,
																});",

            );

$this->fields['series_part_no'] = array(
              'name'        => 'series_part_no',
							'type'				=> '',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Series part no.',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['series_part_name'] = array(
              'name'        => 'series_part_name',
							'type'				=> '',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Series part name',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['topicalterm'] = array(
              'name'        => 'topicalterm',
							'type'				=> 'lookup',
							'ljtable'			=> 'books_topicalterms',
              'id'          => 'lookup_topicalterm',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Topical terms',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['keyword'] = array(
              'name'        => 'keyword',
							'type'				=> 'lookup',
							'ljtable'			=> 'books_keywords',
              'id'          => 'lookup_keyword',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Keywords',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );
/*
$this->fields['topical_keywords'] = array(
              'name'        => 'topical_keywords',
							'type'				=> 'textarea',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Topical keywords',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['genre'] = array(
              'name'        => 'genre',
							'type'				=> '',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Genre',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['category'] = array(
              'name'        => 'category',
							'type'				=> '',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Category',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['litteraryform'] = array(
              'name'        => 'litteraryform',
							'type'				=> 'multiselect',
							'ljtable'			=> 'books_litteraryforms',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Litterary form',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['targetaudience'] = array(
              'name'        => 'targetaudience',
							'type'				=> 'multiselect',
							'ljtable'			=> 'books_targetaudiences',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Target audience',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );
*/
$this->fields['notes'] = array(
              'name'        => 'notes',
							'type'				=> 'textarea',
              'id'          => '',
              'value'       => '',
              'cols'				=> '53',
							'rows'				=> '4',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Notes',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );
/*
$this->fields['bibliographic_note'] = array(
              'name'        => 'bibliographic_note',
							'type'				=> 'textarea',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Bibliographic note',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['personal_names'] = array(
              'name'        => 'personal_names',
							'type'				=> '',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Personal names',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['corporate_names'] = array(
              'name'        => 'corporate_names',
							'type'				=> '',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Corporate names',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );

$this->fields['meeting_names'] = array(
              'name'        => 'meeting_names',
							'type'				=> '',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Meeting names',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
            );
*/
$this->fields['date_created'] = array(
              'name'        => 'date_created',
							'type'				=> '',
              'id'          => 'date_created',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Date added',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
							'script'			=> "$(\"#date_created\").datepicker();"
            );

$this->fields['date_removed'] = array(
              'name'        => 'date_removed',
							'type'				=> '',
              'id'          => 'date_removed',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Date removed',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
							'script'			=> "$(\"#date_removed\").datepicker();"
            );

$this->fields['delete_flag'] = array(
              'name'        => 'delete_flag',
							'type'				=> 'checkbox',
              'id'          => 'delete_flag',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Removed from catalogue',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '0',
							'script'			=> "$(\"#delete_flag\").change(function(){
																	if($(\"#delete_flag\").prop('checked'))
																	{
																		if(confirm('Are you sure ?'))
																		{
																			dt = new Date();
																			$(\"#date_removed\").val(dt.getFullYear()+'-'+(dt.getMonth() < 9 ? '0'+(dt.getMonth()+1) : (dt.getMonth()+1))+'-'+(dt.getDate() < 9 ? '0'+(dt.getDate()+1) : (dt.getDate()+1)));
																		}
																		else
																		{
																			$(\"#delete_flag\").prop('checked', false);
																		}
																	}
																}
															  );"
            );

$this->fields['dewey_decimal_classification'] = array(
              'name'        => 'dewey_decimal_classification',
							'type'				=> '',
              'id'          => '',
              'value'       => '',
              'maxlength'   => '',
              'size'        => '55',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'DDC',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '1',
            );

$this->fields['book_number'] = array(
              'name'        => 'book_number',
							'type'				=> '',
              'id'          => 'book_number',
              'value'       => '',
							'cols'				=> '55',
							'rows'				=> '',
              'style'       => '',
							'js'					=> '',
							'label'				=> 'Book number',
							'show_edit'		=> '1',
							'show_search' => '0',
							'show_result'		=> '1',
							'script'			=> "$(\"#book_number\").focusin(function() {
																	if($(this).val() == '')
																	{
																	$.get('/library/books/ajaxgeneratebooknumber/'+$(\"#bookid\").val(), function(data) {
																		$('.result').html(data);
																		$(\"#book_number\").val(data);
																		});
																	}
																});"
            );



		$this->load_ljtables();
	}

	public function load_ljtables()
	{
		foreach($this->fields as $i => $f)
			if(array_key_exists('ljtable', $f))
			{
				if($f['type'] != "lookup")
				{
					$q = $this->ldb->get($f['name']."s");
					foreach($q->result_array() as $r)
						$this->fields[$i]['options'][$r['id']] = $r['name'];
				}
				else
				{
					if($this->fields[$i]['name'] == "author")
					{
						$this->fields[$i]['subfields']['self'] = array(
							'name'				=> $this->fields[$i]['name'],
							'type'				=> 'text',
							'id'					=> 'lookup_'.$this->fields[$i]['name'],
							'label'				=> $this->fields[$i]['label'],
						);


						$this->fields[$i]['subfields']['submit'] = array(
							'name'        => 'submit',
							'type'				=> 'button',
							'id'          => 'lookup_'.$this->fields[$i]['name'].'_submit',
							'value'       => 'add',
							'maxlength'   => '',
							'size'        => '',
							'style'       => '',
							'onClick'			=> "addLookup('".$this->fields[$i]['name']."', $('#bookid').val(), 'lookup_".$this->fields[$i]['name']."');",
							'label'				=> '',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_result'		=> '0',
						);
					}
					else
					{
						$this->fields[$i]['subfields']['self'] = array(
							'name'				=> $this->fields[$i]['name'],
							'type'				=> 'text',
							'id'					=> 'lookup_'.$this->fields[$i]['name'],
							'label'				=> $this->fields[$i]['label'],
						);


						$this->fields[$i]['subfields']['submit'] = array(
							'name'        => 'submit',
							'type'				=> 'button',
							'id'          => 'lookup_'.$this->fields[$i]['name'].'_submit',
							'value'       => 'add',
							'maxlength'   => '',
							'size'        => '',
							'style'       => '',
							'onClick'			=> "addLookup('".$this->fields[$i]['name']."', $('#bookid').val(), 'lookup_".$this->fields[$i]['name']."');",
							'label'				=> '',
							'show_edit'		=> '0',
							'show_search' => '1',
							'show_result'		=> '0',
						);
					}
				}
				//$this->fields[$i]['values'] = $q->result_array();
//print_r($this->fields[$i]['options']);
			}
	}

	public function get($id = FALSE)
	{
//		$fieldlist[] = $this->classname.".id";
		foreach($this->fields as $f)
		{
			if(array_key_exists('ljtable', $f))
			{
				//print("LJ: ".$f['name']."<br/>");
				if($f['type'] != "lookup")
				{
					$ljfieldlist[] = $f['ljtable'].".".$f['name']."_id";

					$ljoperationlist[] = array($f['ljtable'], $this->classname."s.id = ".$f['ljtable'].".book_id");
				}
			}
			//elseif(array_key_exists('lookuptable', $f))
			//{
			//	print("Lookup: ".$f['name']."<br/>");
			//	$ljfieldlist[] = $f['query'];
			//	$ljoperationlist[] = $f['ljquery'];
			//}
			else
			{
				//print("basic: ".$f['name']."<br/>");
				$fieldlist[] = $this->classname."s.".$f['name'];
			}
		}

		// #### Get all books #### //
		if($id === FALSE){
//		//	$query = $this->ldb->get('books');
//			return $query->result_array();
			$this->ldb->select(implode(',', $fieldlist));
			$this->ldb->select(implode(',', $ljfieldlist));
			$this->ldb->limit(100);
			$this->ldb->from($this->classname."s");
			foreach($ljoperationlist as $lj)
				$this->ldb->join($lj[0], $lj[1], "left");
			$query = $this->ldb->get();
			//print($this->ldb->last_query());
			return $query->result_array();
		}


		// #### ID Provided #### //
		else{
			//$query = $this->ldb->get_where('books', array('id' => $id));
			//return $query->row_array();

			// Get main table
			$this->ldb->select(implode(',', $fieldlist));
			$this->ldb->from($this->classname."s");
			$this->ldb->where($this->classname."s.id",$id);
			$query = $this->ldb->get();
//print($this->ldb->last_query()."<br/>");
			$result = $query->result_array();

			// Get linked tables
			foreach($this->fields as $i => $f)
			{
				if(array_key_exists('ljtable', $f))
				{
					$this->ldb->select($f['name']."_id");
					$this->ldb->from($f['ljtable']);
					$this->ldb->where(array("book_id" => $id));
					$q = $this->ldb->get();
//print($this->ldb->last_query()."<br/>");
					foreach($q->result_array() as $r)
						foreach($r as $r2)
						{
							//print("<pre>");
							//print($f['name']."_id");
							//print_r($r2);
							//print("</pre>");
							//$result[0][$f['name']][] = $r2[$f['name']."_id"];
							$result[0][$f['name']][] = $r2;
						}
				}
			}
			if(array_key_exists(0, $result))
				return $result[0];
			else
				return 0;
		}
	}

	public function get_id_prev($id)
	{
		$this->ldb->select("Max(id) as id");
		$this->ldb->where("id < ".$id);
		$this->ldb->limit(1);
		$q = $this->ldb->get($this->classname."s");
		$r = $q->result_array();
		return $r[0]['id'];
	}

	public function get_id_next($id)
	{
		$this->ldb->select("Min(id) as id");
		$this->ldb->where("id > ".$id);
		$this->ldb->limit(1);
		$q = $this->ldb->get($this->classname."s");
		$r = $q->result_array();
		return $r[0]['id'];
	}

	public function set($id = FALSE, $data_to_save)
	{
//print_r($this->input->post());
//print_r($data_to_save);
//print_r($this->fields);
//print("<br/>\n");
//die();

		// Import submitted values
		foreach($this->fields as $f)
		{

			if(array_key_exists('ljtable', $f))
			{
				if($f['type'] != "lookup")
				{
					if($data_to_save[$f['name']])
					{
						$inserts[$f['name']] = $f;
						$inserts[$f['name']]['submitted_book'] = $data_to_save[$f['name']];
					}
				}
			}
			//elseif(array_key_exists('lookuptable', $f))
			//{
			//	// NOP();
			//}
			else
			{
				if(array_key_exists($f['name'], $data_to_save))
					if(!$data_to_save[$f['name']])
						$data[$f['name']] = NULL;
					else
						$data[$f['name']] = $data_to_save[$f['name']];
				// unchecked checkbox values are not submitted :(
				elseif($f['type'] == 'checkbox')
					$data[$f['name']] = "0";
			}
		}

/*
print($id);
print("<pre>");
print_r($data);
print("<br/>\n");
print_r($inserts);
print("<br/>\n");
print("</pre>");
die();
*/

		if($id !== FALSE)
		{
			// Update books table
			$this->ldb->where('id', $id);
			$this->ldb->update($this->classname."s", $data);
//print("<hr>".$this->ldb->last_query()."<hr><br/>");
			//$this->ldb->insert($this->classname, $data);
		}
		else
		{
			$this->ldb->insert($this->classname."s", $data);
			$id = $this->ldb->insert_id();
		}

//die($id);

		// Delete entries from LJ tables
		foreach($inserts as $f)
			$qdelete[] = $f['ljtable'];
		$this->ldb->where('book_id', $id);
		$this->ldb->delete($qdelete);

		// Insert entries into LJ tables
		foreach($inserts as $f)
		{
			$d = null;
			if(is_array($f['submitted_book']))
			{
				foreach($f['submitted_book'] as $v)
					$d[] = array('book_id' => $id, $f['name']."_id"=>$v);
			}
			else
				$d[0] = array('book_id' => $id, $f['name']."_id"=>$f['submitted_book']);

			if(count($d) > 1)
				$this->ldb->insert_batch($f['ljtable'], $d);
			else
				$this->ldb->insert($f['ljtable'], $d[0]);

//print("<hr>".$this->ldb->last_query()."<hr><br/>");
		}

		// Return the ID if newly generated
		return $id;
	}


	public function delete($id)
	{
		$this->ldb->where("books.id", $id);
		$this->ldb->delete("books");
		$this->ldb->where("books_clients.book_id", $id);
		$this->ldb->delete("books_clients");
		$this->ldb->where("books_clients.book_id", $id);
		$this->ldb->delete("books_clients");
		$this->ldb->where("authors_books.book_id", $id);
		$this->ldb->delete("authors_books");
		$this->ldb->where("books_features.book_id", $id);
		$this->ldb->delete("books_features");
		$this->ldb->where("books_keywords.book_id", $id);
		$this->ldb->delete("books_keywords");
		$this->ldb->where("books_languages.book_id", $id);
		$this->ldb->delete("books_languages");
		$this->ldb->where("books_physicaldetails.book_id", $id);
		$this->ldb->delete("books_physicaldetails");
		$this->ldb->where("books_topicalterms.book_id", $id);
		$this->ldb->delete("books_topicalterms");
	}

	function lookup($table_name, $ljtable, $bookid)
	{
		//print("looking up ".$table_name."<br/>");
		$this->ldb->select(array($table_name."s.id", $table_name."s.name"));
		$this->ldb->from($ljtable);
			$this->ldb->join($table_name."s", $table_name."s.id = ".$ljtable.".".$table_name."_id", "left");
		$this->ldb->where('book_id', $bookid);
		$query = $this->ldb->get();
		$r = $query->result_array();
//print($this->ldb->last_query());
//print_r($r);
//die();
		return $r;
	}

	function lookup_author_role($author_id, $book_id)
	{
		$this->ldb->select("name as role");
		$this->ldb->from("authors_books");
		$this->ldb->join("roles", "roles.id = authors_books.role_id", "left");
		$this->ldb->where(array("authors_books.author_id" => $author_id, "authors_books.book_id" => $book_id));
		$query = $this->ldb->get();
		$r = $query->result_array();
		//print($this->ldb->last_query());
		//print_r($r);
		//print($r[0]['role']."<br/>");
		return $r[0]['role'];
	}

	function get_book_nextserialnumber()
	{
		$this->ldb->select_max('serial_number');
		$q = $this->ldb->get($this->classname."s");
		$r = $q->result_array();
		return $r[0]['serial_number']+1;
	}

	// Get the next book's serial number
	// Strategy: if a book is not yet been saved, it will have a serial_number
	// Search for highest ID. If it has a serial number, create new ; if it does not, use that ID
	// Problem: does not ensure atomicity (2 users can get the same ID if they create a new record at the same time)
	function get_book_nextid()
	{
		$this->ldb->select(array("serial_number", "id"));
		$this->ldb->from($this->classname."s");
		$this->ldb->order_by("id", "DESC");
		$this->ldb->limit(1);
		$query = $this->ldb->get();
		$r = $query->row_array();
//print($this->ldb->last_query());
//print("<br/>\n");
//print_r($r);
//die();
		$sn = $r['serial_number'];

		if($sn)
		{
			$this->ldb->insert($this->classname."s", array("serial_number" => NULL));
			return $this->ldb->insert_id();
		}
		else
		{
			$id = $r['id'];
			if( ! $id)
				$id = 1;
			return $id;
		}
	}

	function get_by_sn($sn)
	{
		$query = $this->ldb->get_where($this->classname."s", array('serial_number' => $sn));
		return $query->row_array();
	}


	function get_checkin_details($sn)
	{
//$checkinfields['date_borrow'] =

		$ljfieldlist[] = "books.id as bookid";
		$ljfieldlist[] = "books.serial_number";
		$ljfieldlist[] = "books_clients.date_borrow";
		$ljfieldlist[] = "books_clients.date_due";
		$ljfieldlist[] = "books_clients.id";
		$ljfieldlist[] = "clients.id as clientid";
		$ljfieldlist[] = "clients.name";
		$ljfieldlist[] = "clients.surname";
		$ljfieldlist[] = "clients.workplace";
		$ljfieldlist[] = "clients.address";

		$ljoperationlist[] = array("books_clients", $this->classname."s.id = books_clients.book_id");
		$ljoperationlist[] = array("clients", "clients.id = books_clients.client_id");

		foreach($this->fields as $f)
		{
			if($f['show_result'] == "1" && $f['name'] != 'id' && !array_key_exists('ljtable', $f))
				$fieldlist[] = $this->classname."s.".$f['name'];
		}

		$this->ldb->select(implode(',', $fieldlist));
		$this->ldb->select(implode(',', $ljfieldlist));
		$this->ldb->from($this->classname."s");
		$this->ldb->where(array("books.serial_number"=>$sn,"books_clients.date_return"=>0));
		foreach($ljoperationlist as $lj)
			$this->ldb->join($lj[0], $lj[1], "inner");
		$query = $this->ldb->get();
//print($this->ldb->last_query());
//print("<br/>\n");
		return $query->row_array();
	}

	function checkin($id)
	{
		$query = $this->ldb->query("update books_clients set date_return = current_date() where id=".$id);
		//print($this->ldb->last_query());
		//print("<br/>\n");
	}

	function checkout($booksn, $clientid)
	{
		// Does this book exists ?
		$query = $this->ldb->get_where('books', array('books.serial_number' => $booksn));
		if(count($r = $query->row_array()) <1)
			return array("message" => "There is no book matching serial number ".$booksn);
		else
			$bookid = $r['id'];

		// Is the book in ?
		$this->ldb->select(array("books.id", "books.serial_number", "books.title", "clients.name", "clients.surname", "clients.address", "books_clients.date_borrow", "books_clients.date_due"));
		$this->ldb->from("books");
		$this->ldb->where(array("books.serial_number" => $booksn, "books_clients.date_return" => "0000-00-00"));
		$this->ldb->join("books_clients", "books_clients.book_id = books.id", "left");
		$this->ldb->join("clients", "clients.id = books_clients.client_id", "left");
		$query = $this->ldb->get();
		if(count($r = $query->row_array()) > 0)
			return array("message" => "The book no ".$r['serial_number']." - ".$r['title']." is book is currently marked as borrowed by ".$r['name']." ".$r['surname']."(".$r['address'].") due by ".$r['date_due']);

		// How long can it be borrowed for ?
		$query = $this->ldb->get_where('clients', array('id' => $clientid));
		if(count($r = $query->row_array()) > 0)
			$number_of_days = $r['number_of_days'];
		else
			$number_of_days = 15;


		// Borrow book
		$query = $this->ldb->query("INSERT INTO `books_clients` (`book_id`, `client_id`, `date_borrow`, `date_due`) VALUES ('".$bookid."', '".$clientid."', current_date(), DATE_ADD(current_date(),INTERVAL ".$number_of_days." day))");
	//print($this->ldb->last_query());
	//print("<br/>\n");
		return array("success" => "Book successfully borrowed");
	}

	function search($needles, $offset=0)
	{
		$bLoadAllTables = false;
		$limit = 500;
		$result = array();
		$recursive_from = "";
		$bFound = 0;
		foreach($needles as $i => $n)
		{
			if(array_key_exists('value', $n) && $n['value'] != '' )
			{
				$bFound = 1;
				break;
			}
		}
		if(!$bFound)
			return $result;

//print("<pre>");
//print_r($needles);

		// Search for deleted ?
		//if(array_key_exists("search_deleted", $needles) && $needles['search_deleted']['value'] == 1)
		if(is_admin())
		{
			$df = "`delete_flag` >= '0'";
			//$df = "`delete_flag` = '1'";
			//unset($needles['search_deleted']);
		}
		else
			$df = "`delete_flag` = '0'";



		// Return only fields which are required to be viewn
		foreach($this->fields as $f)
		{
			if(array_key_exists('show_result', $f) && $f['show_result'] == 1)
			{
				if(!array_key_exists('ljtable', $f))
					$fieldlist[] = $this->classname."s.".$f['name'];
			}
		}


		// Search barcode ; ignore other fields
		if(array_key_exists("serial_number", $needles) && array_key_exists("value", $needles['serial_number']) && $needles['serial_number']['value'] != "")
		{
			$this->ldb->select(implode(',', $fieldlist));
			$this->ldb->select(array("books.delete_flag", "books_clients.date_borrow", "books_clients.date_due", "clients.id as clientid", "CONCAT(clients.name, ' ', clients.surname, ' (', clients.address, ')'	) as client", "GROUP_CONCAT(authors.id SEPARATOR ';') as author_id", "GROUP_CONCAT(authors.name SEPARATOR ';') as author_name"));
/*			// Return only fields which are required to be viewn
			foreach($this->fields as $f)
			{
				if(array_key_exists('ljtable', $f) && ((array_key_exists('show_result', $f) && $f['show_result'] == 1) || $bLoadAllTables))
				{
					$this->ldb->select("group_concat(".$f['name']."s.name) as ".$f['name']);
					$this->ldb->join($f['ljtable'], $f['ljtable'].".book_id = books.id", "left");
					//$this->ldb->join("languages", "languages.id = books_languages.language_id", "left");
					$this->ldb->join($f['name']."s", $f['ljtable'].".".$f['name']."_id = ".$f['name']."s.id", "left");
				}
			}
*/
			foreach($this->fields as $f)
			{
				if(array_key_exists('ljtable', $f) && ((array_key_exists('show_result', $f) && $f['show_result'] == 1) || $bLoadAllTables))
				{

 //(
  //select GROUP_CONCAT(authors.id SEPARATOR ';')
  //from authors_books
  //LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   //where `authors_books`.`book_id` = `books`.`id`
//)  as author_id,

					$strSelect = "(SELECT GROUP_CONCAT(".$f['name']."s.name)\n";
					$strSelect .= "FROM ".$f['ljtable']."\n";
					$strSelect .= "LEFT JOIN ".$f['name']."s ON ". $f['ljtable'].".".$f['name']."_id = ".$f['name']."s.id\n";
					$strSelect .= "WHERE ".$f['ljtable'].".".$this->classname."_id = ".$this->classname."s.id\n";
					$strSelect .= ") as ".$f['name'];
					$this->ldb->select($strSelect);
				}
			}
			$this->ldb->join("books_clients", "books_clients.book_id = books.id and books_clients.date_return = 0000-00-00", "left");
			$this->ldb->join("clients", "books_clients.client_id = clients.id", "left");
			$this->ldb->join("authors_books", "authors_books.book_id = books.id and authors_books.role_id = 1", "left");
			$this->ldb->join("authors", "authors.id = authors_books.author_id", "left");

			$this->ldb->where("books.serial_number", $needles['serial_number']['value']);
			$this->ldb->where($df);
			$this->ldb->from("books");
			$query = $this->ldb->get();
			$result = $query->result_array();
			return $result;
		}


		// Search author
		if(array_key_exists("author", $needles) && array_key_exists("value", $needles['author']) && $needles['author']['value'] != "")
		{
			$a['authors.name'] = explode(" ", $needles['author']['value']);
			$this->ldb->select("books.*");
			$this->ldb->from("authors");
			$this->ldb->join("authors_books", "authors_books.author_id = authors.id", "left");
			$this->ldb->join("books", "books.id = authors_books.book_id", "left");
			foreach($a['authors.name'] as $name)
				//$this->ldb->like("authors.name", addslashes($name));
				$this->ldb->where("authors.name regexp '[[:<:]]".addslashes($name)."'");
			$this->ldb->where("books.id is not null");
			$query = $this->ldb->get();
	//$result = $query->result_array();
	//print_r($result);
			$recursive_from = $this->ldb->last_query();
	//print($recursive_from);
//print("<pre>");
//print($this->ldb->last_query());
//print("</pre>");
		}



		// Search title & general
		$w = array();
		if($recursive_from)
			$table = "`books_rec`";
		else
			$table = "`books`";
		if(array_key_exists("title", $needles) && array_key_exists("value", $needles['title']) && $needles['title']['value'] != "")
		{
			$arrT = explode(" ", $needles['title']['value']);
			$arrTitles = array("title", "remainder_of_title", "parallel_title", "original_title", "series_name", "series_part_name");
			foreach($arrTitles as $fieldname)
			{
				$p = array();
				foreach($arrT as $titlepart)
					//$p[] = $table.".`".$fieldname."` like '%".$titlepart."%'";
					$p[] = $table.".`".$fieldname."` regexp '[[:<:]]".addslashes($titlepart)."'";
				$w[] = "(".implode(" and ", $p).")";
				//$t["books.".$fieldname][] = $titlepart;
			}
		}
/*		if(array_key_exists("general", $needles) && array_key_exists("value", $needles['general']) && $needles['general']['value'] != "")
		{
			$bLoadAllTables = true;
			foreach($this->fields as $f)
				if(array_key_exists('ljtable', $f))
					$w[] = "`".$f['name']."s`.`name` like '%".$needles['title']['value']."%'";
				else
					$w[] = "`books`.`".$f['name']."` like '%".$needles['title']['value']."%'";
		}
*/
		if(count($w))
			$strWhere = implode("\nOR ", $w);
		else
			$strWhere = "1";

//print("where:\n<br/>");
//print_r($strWhere);

		// There is an author to search book in recursively
		// Cannot use ActiveRecrod due to recursive query ; loses dynamic changing fields, but what to do...
		if($recursive_from)
		{
			$strQuery = "SELECT `books_rec`.`id`, `books_rec`.`serial_number`, `books_rec`.`title`, `books_rec`.`remainder_of_title`, `books_rec`.`parallel_title`, `books_rec`.`original_title`, `books_rec`.`part_x`, `books_rec`.`book_number`, `books_rec`.`delete_flag`, `books_rec`.`dewey_decimal_classification`, `books_clients`.`date_borrow`, `books_clients`.`date_due`, `clients`.`id` as clientid, CONCAT(clients.name, ' ', clients.surname, ' (', clients.address, ')' ) as client,";
			//GROUP_CONCAT(authors.id SEPARATOR ';') as author_id,
			//GROUP_CONCAT(authors.name SEPARATOR ';') as author_name,
			//GROUP_CONCAT(languages.name) as language  \n";

			$strQuery .= "\n(
  SELECT group_concat(`languages`.`name`)
  FROM books_languages
  LEFT JOIN `languages` ON `books_languages`.`language_id` = `languages`.`id`
   WHERE `books_languages`.`book_id` = `books_rec`.`id`
) AS language,
(
  SELECT GROUP_CONCAT(`authors`.`id` SEPARATOR ';')
  FROM authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   WHERE `authors_books`.`book_id` = `books_rec`.`id`
)  AS author_id,
(
  SELECT GROUP_CONCAT(`authors`.`name` SEPARATOR ';')
  FROM authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   WHERE `authors_books`.`book_id` = `books_rec`.`id`
)  AS author_name\n";

			$strQuery .= "FROM (".$recursive_from.") books_rec\n";
/*			$strQuery .= "LEFT JOIN `books_languages` ON `books_languages`.`book_id` = `books_rec`.`id`
										LEFT JOIN `languages` ON `books_languages`.`language_id` = `languages`.`id`
										LEFT JOIN `books_clients` ON `books_clients`.`book_id` = `books_rec`.`id` and books_clients.date_return = 0000-00-00
										LEFT JOIN `clients` ON `books_clients`.`client_id` = `clients`.`id`
										LEFT JOIN `authors_books` ON `authors_books`.`book_id` = `books_rec`.`id` and authors_books.role_id = 1
										LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
										WHERE ((".$strWhere.") AND ".$df.")
										GROUP BY `books_rec`.`id`";
*/
			$strQuery .= "LEFT JOIN `books_clients` ON `books_clients`.`book_id` = `books_rec`.`id` and books_clients.date_return = 0000-00-00
										LEFT JOIN `clients` ON `books_clients`.`client_id` = `clients`.`id`
										WHERE ((".$strWhere.") AND ".$df.")
										GROUP BY `books_rec`.`id`";

			$query = $this->ldb->query($strQuery);
			$result = $query->result_array();

//print_r($this->ldb->last_query());

		}


		// No authors found, don't use recursive querry
		else
		{
			$this->ldb->select(implode(',', $fieldlist));
			$this->ldb->select(array("books.delete_flag", "books_clients.date_borrow", "books_clients.date_due", "clients.id as clientid", "CONCAT(clients.name, ' ', clients.surname, ' (', clients.address, ')'	) as client", "GROUP_CONCAT(authors.id SEPARATOR ';') as author_id", "GROUP_CONCAT(authors.name SEPARATOR ';') as author_name"));

			// Old form
/*			foreach($this->fields as $f)
			{
				if(array_key_exists('ljtable', $f) && ((array_key_exists('show_result', $f) && $f['show_result'] == 1) || $bLoadAllTables))
				{
					$this->ldb->select("group_concat(".$f['name']."s.name) as ".$f['name']);
					$this->ldb->join($f['ljtable'], $f['ljtable'].".book_id = books.id", "left");
					//$this->ldb->join("languages", "languages.id = books_languages.language_id", "left");
					$this->ldb->join($f['name']."s", $f['ljtable'].".".$f['name']."_id = ".$f['name']."s.id", "left");
				}
			}
*/
			// Return only fields which are required to be viewn
			foreach($this->fields as $f)
			{
				if(array_key_exists('ljtable', $f) && ((array_key_exists('show_result', $f) && $f['show_result'] == 1) || $bLoadAllTables))
				{

 //(
  //select GROUP_CONCAT(authors.id SEPARATOR ';')
  //from authors_books
  //LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   //where `authors_books`.`book_id` = `books`.`id`
//)  as author_id,

					$strSelect = "(SELECT GROUP_CONCAT(".$f['name']."s.name)\n";
					$strSelect .= "FROM ".$f['ljtable']."\n";
					$strSelect .= "LEFT JOIN ".$f['name']."s ON ". $f['ljtable'].".".$f['name']."_id = ".$f['name']."s.id\n";
					$strSelect .= "WHERE ".$f['ljtable'].".".$this->classname."_id = ".$this->classname."s.id\n";
					$strSelect .= ") as ".$f['name'];
					$this->ldb->select($strSelect);
				}
			}

			$this->ldb->join("books_clients", "books_clients.book_id = books.id and books_clients.date_return = 0000-00-00", "left");
			$this->ldb->join("clients", "books_clients.client_id = clients.id", "left");
			$this->ldb->join("authors_books", "authors_books.book_id = books.id and authors_books.role_id = 1", "left");
			$this->ldb->join("authors", "authors.id = authors_books.author_id", "left");

			//$this->ldb->where("books.serial_number", $needles['serial_number']['value']);
			//$this->ldb->where("delete_flag = 0");
			$this->ldb->where("(".$strWhere.") AND ".$df);
			$this->ldb->from("books");
			$this->ldb->group_by("books.id");
			$query = $this->ldb->get();
			$result = $query->result_array();
		}

/*
SELECT `books`.`id`, `books`.`title`,
(
  select group_concat(languages.name)
  from books_languages
  LEFT JOIN `languages` ON `books_languages`.`language_id` = `languages`.`id`
   where `books_languages`.`book_id` = `books`.`id`
) as language ,
(
  select GROUP_CONCAT(authors.id SEPARATOR ';')
  from authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   where `authors_books`.`book_id` = `books`.`id`
)  as author_id,
(
  select GROUP_CONCAT(authors.name SEPARATOR ';')
  from authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   where `authors_books`.`book_id` = `books`.`id`
)  as author_name

FROM (`books`)

LEFT JOIN `authors_books` ON `authors_books`.`book_id` = `books`.`id` and authors_books.role_id = 1

WHERE ((`books`.`title` regexp '[[:<:]]warrior' and `books`.`title` regexp '[[:<:]]marks')
OR (`books`.`remainder_of_title` regexp '[[:<:]]warrior' and `books`.`remainder_of_title` regexp '[[:<:]]marks')
OR (`books`.`parallel_title` regexp '[[:<:]]warrior' and `books`.`parallel_title` regexp '[[:<:]]marks')
OR (`books`.`original_title` regexp '[[:<:]]warrior' and `books`.`original_title` regexp '[[:<:]]marks')
OR (`books`.`series_name` regexp '[[:<:]]warrior' and `books`.`series_name` regexp '[[:<:]]marks')
OR (`books`.`series_part_name` regexp '[[:<:]]warrior' and `books`.`series_part_name` regexp '[[:<:]]marks')) AND `delete_flag` = '0'
GROUP BY `books`.`id`
*/
//print("<pre>");
//print($this->ldb->last_query());
//print_r($result);
//print("</pre>");
//die();

	return $result;
	}


	function search_advanced($needles, $offset=0)
	{
		$r1 = $this->search_adv_individual($needles['f1']['value'], $needles['fv1']['value'], $needles['fc1']['value']);
		$r2 = $this->search_adv_individual($needles['f2']['value'], $needles['fv2']['value'], $needles['fc2']['value']);
		$r3 = $this->search_adv_individual($needles['f3']['value'], $needles['fv3']['value'], $needles['fc3']['value']);
		$o1 = $needles['bf1']['value'];
		$o2 = $needles['bf2']['value'];
		$o3 = $needles['bf3']['value'];

//print_r($r1);
//print_r($r2);
//print_r($r3);
//die();

		if(is_admin())
		{
			$df = "`delete_flag` >= '0'";
			//$df = "`delete_flag` = '1'";
			//unset($needles['search_deleted']);
		}
		else
			$df = "`delete_flag` = '0'";

		$arrRes = $this->bop($this->bop($r1, $r2, $o1), $r3, $o2);
//print_r($arrRes);
		if(is_array($arrRes) && count($arrRes) > 0)
		{
			$strWhere = "books.id IN(".implode(",", $arrRes).")";

			$strQuery = "SELECT `books`.`id`, `books`.`serial_number`, `books`.`title`, `books`.`remainder_of_title`, `books`.`parallel_title`, `books`.`original_title`, `books`.`part_x`, `books`.`book_number`, `books`.`dewey_decimal_classification`, `books`.`delete_flag`, `books_clients`.`date_borrow`, `books_clients`.`date_due`, `clients`.`id` as clientid, CONCAT(clients.name, ' ', clients.surname, ' (', clients.address, ')' ) as client,";
			$strQuery .= "\n(
  SELECT group_concat(`languages`.`name`)
  FROM books_languages
  LEFT JOIN `languages` ON `books_languages`.`language_id` = `languages`.`id`
   WHERE `books_languages`.`book_id` = `books`.`id`
) AS language,
(
  SELECT GROUP_CONCAT(`authors`.`id` SEPARATOR ';')
  FROM authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   WHERE `authors_books`.`book_id` = `books`.`id`
)  AS author_id,
(
  SELECT GROUP_CONCAT(`authors`.`name` SEPARATOR ';')
  FROM authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   WHERE `authors_books`.`book_id` = `books`.`id`
)  AS author_name\n";

			$strQuery .= "FROM books\n";
			$strQuery .= "LEFT JOIN `books_clients` ON `books_clients`.`book_id` = `books`.`id` and books_clients.date_return = 0000-00-00
										LEFT JOIN `clients` ON `books_clients`.`client_id` = `clients`.`id`
										WHERE ".$strWhere." AND ".$df;

			$q = $this->ldb->query($strQuery);
			$r = $q->result_array();
//print($this->ldb->last_query());
//print_r($r);
			return $r;
		}

	}

	function bop($r1, $r2, $o1)
	{
		if( ! is_array($r1))
			return array();
		if( ! is_array($r2) || count($r2) == 0)
			return $r1;
		if($o1 == "and")
		{
			//print("\ngoing for and\n");
			$arrRet = array();
			foreach($r1 as $r)
			{
				if(array_search($r, $r2))
					$arrRet[] = $r;
			}
			return $arrRet;
		}
		else{
			return array_merge($r1, $r2);
		}
	}

	private function write_where($term, $fields, $criteria)
	{
		$arrRet = array();
		switch($criteria)
		{
			// Equal (soft)
			case "like":
				foreach($fields as $f)
					$arrRet[] = $f." like '".$term."'";
			break;

			// Equal (hard)
			case "equals":
				foreach($fields as $f)
					$arrRet[] = $f." = '".$term."'";
			break;

			// Begins with
			case "like%":
				foreach($fields as $f)
					$arrRet[] = $f." like '".$term."%'";
			break;

			// Ends with
			case "%like":
				foreach($fields as $f)
					$arrRet[] = $f." like '%".$term."'";
			break;

			// Contains
			case "%like%":
			default:
				foreach($fields as $f)
					$arrRet[] = $f." like '%".$term."%'";
			break;
		}

		return implode(" OR ", $arrRet);
	}

	function search_adv_individual($fieldname, $term, $criteria)
	{
		$ret = array();
		// Search in linked table
		if(array_key_exists("ljtable", $this->fields[$fieldname]) && $this->fields[$fieldname]['ljtable'])
		{
			// Keep option of author.field searches...
			if(substr($fieldname, 0, strlen("author")) == "author")
			{
				if($fieldname && $term  !== false)
				{
					$this->ldb->select("authors_books.book_id as id");
					$this->ldb->from($fieldname."s");
					$this->ldb->join($fieldname."s_books", $fieldname."s_books.".$fieldname."_id = ".$fieldname."s.id");
					//$this->ldb->where($fieldname."s.name like '%".$term."%' OR ".$fieldname."s.title like '%".$term."%' OR ".$fieldname."s.name_fuller_form like '%".$term."%' OR ".$fieldname."s.notes like '%".$term."%' OR ".$fieldname."s.lifetime like '%".$term."%'");
					$this->ldb->where($this->write_where($term, array($fieldname."s.name", $fieldname."s.title", $fieldname."s.name_fuller_form", $fieldname."s.notes", $fieldname."s.lifetime"), $criteria));
					$q = $this->ldb->get();
					//print($this->ldb->last_query());
					$r = $q->result_array();
					foreach($r as $v)
						$ret[] = $v['id'];
				}
				return $ret;
			}
			else
			{
				if($fieldname && $term  !== false)
				{
					$this->ldb->select("books_".$fieldname."s.book_id as id");
					$this->ldb->from($fieldname."s");
					$this->ldb->join("books_".$fieldname."s", "books_".$fieldname."s.".$fieldname."_id = ".$fieldname."s.id");
					//$this->ldb->where($fieldname."s.name like '%".$term."%' OR ".$fieldname."s.notes like '%".$term."%'");
					$this->ldb->where($this->write_where($term, array($fieldname."s.name", $fieldname."s.notes"), $criteria));
					$q = $this->ldb->get();
					//print($this->ldb->last_query());
					$r = $q->result_array();
					foreach($r as $v)
						$ret[] = $v['id'];
				}
				return $ret;
			}
		}
		// Search in books table
		else
		{
			if($fieldname && $term  !== false)
			{
				$this->ldb->select("id");
				$this->ldb->from("books");
				//$this->ldb->where($fieldname." like '%".$term."%'");
				$this->ldb->where($this->write_where($term, array($fieldname), $criteria));
				$q = $this->ldb->get();
				//print($this->ldb->last_query());
				$r = $q->result_array();
				foreach($r as $v)
					$ret[] = $v['id'];
			}
			return $ret;
		}
	}

	function searchall($strLimit = 100)
	{
		// Return only fields which are required to be viewn
		foreach($this->fields as $f)
		{
			if(array_key_exists('show_result', $f) && $f['show_result'] == 1)
			{
				if(!array_key_exists('ljtable', $f))
					$fieldlist[] = $this->classname."s.".$f['name'];
			}
		}

		$this->ldb->select(implode(',', $fieldlist));
		$this->ldb->select(array("books_clients.date_borrow", "books_clients.date_due", "clients.id as clientid", "CONCAT(clients.name, ' ', clients.surname, ' (', clients.address, ')'	) as client", "authors.id as author_id", "authors.name as author_name"));
		$this->ldb->join("books_clients", "books_clients.book_id = books.id and books_clients.date_return = 0000-00-00", "left");
		$this->ldb->join("clients", "books_clients.client_id = clients.id", "left");
		$this->ldb->join("authors_books", "authors_books.book_id = books.id", "left");
		$this->ldb->join("authors", "authors.id = authors_books.author_id", "left");
		$this->ldb->where("delete_flag = 0");
		$this->ldb->limit($strLimit);
		$this->ldb->from($this->classname."s");
		$query = $this->ldb->get();
		$result = $query->result_array();
//print($this->ldb->last_query());
//print("<br/>\n");

		// Look up LJTables
		foreach($result as $i => $r)
		{
			// Return only fields which are required to be viewn
			foreach($this->fields as $f)
			{
				if(array_key_exists('ljtable', $f) && array_key_exists('show_result', $f) && $f['show_result'] == 1)
				{
					$this->ldb->select($f['name']."s.name as ".$f['name']);
					$this->ldb->from($f['ljtable']);
					$this->ldb->join($f['name']."s", $f['ljtable'].".".$f['name']."_id = ".$f['name']."s.id", "left");
					$this->ldb->where($f['ljtable'].".book_id", $r['id']);
					$query = $this->ldb->get();
		//print($this->ldb->last_query());
		//print("<br/>\n");
					$ljr = $query->result_array();
					if(is_array($ljr) && count($ljr) > 0)
						$result[$i][$f['name']] = $ljr;
					else
						$result[$i][$f['name']] = NULL;
				}
			}
		}

	return $result;
	}

	function searchentrydate($from, $to, $direction=FALSE)
	{
		// Return only fields which are required to be viewn
		foreach($this->fields as $f)
		{
			if(array_key_exists('show_result', $f) && $f['show_result'] == 1)
			{
				if(!array_key_exists('ljtable', $f))
					$fieldlist[] = $this->classname."s.".$f['name'];
			}
		}
		// If 'date_entry' field is not already part of the list, add it
		if($this->fields['date_created']['show_result'] != 1)
			$fieldlist[] = $this->classname."s.".$this->fields['date_created']['name'];

		$this->ldb->select(implode(',', $fieldlist));
		$this->ldb->select(array("books_clients.date_borrow", "books_clients.date_due", "clients.id as clientid", "CONCAT(clients.name, ' ', clients.surname, ' (', clients.address, ')'	) as client"));
		$this->ldb->select("
  (select GROUP_CONCAT(authors.id SEPARATOR ';')
  from authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   where `authors_books`.`book_id` = `books`.`id`
)  as author_id,
(
  select GROUP_CONCAT(authors.name SEPARATOR ';')
  from authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   where `authors_books`.`book_id` = `books`.`id`
)  as author_name");



		$this->ldb->join("books_clients", "books_clients.book_id = books.id and books_clients.date_return = 0000-00-00", "left");
		$this->ldb->join("clients", "books_clients.client_id = clients.id", "left");
		$this->ldb->where(array('delete_flag' => "0", "date_created >= " => $from, "date_created <= " => $to));
		$this->ldb->from($this->classname."s");
		if(!$direction || $direction <= 0)
			$this->ldb->order_by("date_created", "ASC");
		else
			$this->ldb->order_by("date_created", "DESC");
		$query = $this->ldb->get();
		$result = $query->result_array();
//print($this->ldb->last_query());
//print("<br/>\n");

		// Look up LJTables
		foreach($result as $i => $r)
		{
			// Return only fields which are required to be viewn
			foreach($this->fields as $f)
			{
				if(array_key_exists('ljtable', $f) && array_key_exists('show_result', $f) && $f['show_result'] == 1)
				{
					$this->ldb->select($f['name']."s.name as ".$f['name']);
					$this->ldb->from($f['ljtable']);
					$this->ldb->join($f['name']."s", $f['ljtable'].".".$f['name']."_id = ".$f['name']."s.id", "left");
					$this->ldb->where($f['ljtable'].".book_id", $r['id']);
					$query = $this->ldb->get();
		//print($this->ldb->last_query());
		//print("<br/>\n");
					$ljr = $query->result_array();
					if(is_array($ljr) && count($ljr) > 0)
						$result[$i][$f['name']] = $ljr;
					else
						$result[$i][$f['name']] = NULL;
				}
			}
		}

	return $result;
	}


	function searchborrowfrequency($ddc_from, $ddc_to, $direction=FALSE)
	{
		// Return only fields which are required to be viewn
		foreach($this->fields as $f)
		{
			if(array_key_exists('show_result', $f) && $f['show_result'] == 1)
			{
				if(!array_key_exists('ljtable', $f))
					$fieldlist[] = $this->classname."s.".$f['name'];
			}
		}

		$this->ldb->select(implode(',', $fieldlist));
		$this->ldb->select(array("books.id", "books_clients.id as borrowid", "COUNT(books_clients.client_id) as borrow_qty", "YEAR(MAX(date_borrow)) as last_borrowed", "YEAR(books.date_created) as created"));
		$this->ldb->select("
  (select GROUP_CONCAT(authors.id SEPARATOR ';')
  from authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   where `authors_books`.`book_id` = `books`.`id`
)  as author_id,
(
  select GROUP_CONCAT(authors.name SEPARATOR ';')
  from authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   where `authors_books`.`book_id` = `books`.`id`
)  as author_name");
		$this->ldb->join("books_clients", "books_clients.book_id = books.id", "left");
		$this->ldb->from("books");
		$this->ldb->where(array('books.delete_flag' => "0", "books.dewey_decimal_classification >= " => $ddc_from, "books.dewey_decimal_classification <= " => $ddc_to));
		$this->ldb->group_by("books.id");

		if(!$direction || $direction <= 0)
			$this->ldb->order_by("books.dewey_decimal_classification", "ASC");
		else
			$this->ldb->order_by("books.dewey_decimal_classification", "DESC");
		$query = $this->ldb->get();
		$result = $query->result_array();
//print_r($result);
//print($this->ldb->last_query());
//print("<br/>\n");

		// Look up LJTables
		foreach($result as $i => $r)
		{
			// Return only fields which are required to be viewn
			foreach($this->fields as $f)
			{
				if(array_key_exists('ljtable', $f) && array_key_exists('show_result', $f) && $f['show_result'] == 1)
				{
					$this->ldb->select($f['name']."s.name as ".$f['name']);
					$this->ldb->from($f['ljtable']);
					$this->ldb->join($f['name']."s", $f['ljtable'].".".$f['name']."_id = ".$f['name']."s.id", "left");
					$this->ldb->where($f['ljtable'].".book_id", $r['id']);
					$query = $this->ldb->get();
		//print($this->ldb->last_query());
		//print("<br/>\n");
					$ljr = $query->result_array();
					if(is_array($ljr) && count($ljr) > 0)
						$result[$i][$f['name']] = $ljr;
					else
						$result[$i][$f['name']] = NULL;
				}
			}
		}

	return $result;
	}

	function searchnotborrowed($from, $to, $direction=FALSE)
	{
		// Return only fields which are required to be viewn
		foreach($this->fields as $f)
		{
			if(array_key_exists('show_result', $f) && $f['show_result'] == 1)
			{
				if(!array_key_exists('ljtable', $f))
					$fieldlist[] = $this->classname."s.".$f['name'];
			}
		}

		$q = $this->ldb->query("SELECT DISTINCT id FROM books_clients WHERE date_return >=  '".$from."' AND date_return <  '".$to."'");
		$r = $q->result_array();
		foreach($r as $i => $v)
			$exclude[] = $v['id'];

/*
select books.id, books.title from books
where books.id not in(SELECT DISTINCT id FROM books_clients WHERE date_return >=  "2012-01-01" AND date_return <  "2013-01-01")
limit 0,30
*/

//print($from);
//print($to);
//die();

//`books`.`id`, `books`.`serial_number`, `books`.`title`, `books`.`remainder_of_title`, `books`.`parallel_title`, `books`.`original_title`, `books`.`part_x`, `books`.`book_number`, `books_clients`.`id` as borrowid, COUNT(books_clients.client_id) as borrow_qty, MAX(date_borrow) as last_borrowed FROM (`books_clients`) LEFT JOIN `books` ON `books_clients`.`book_id` = `books`.`id`


/*			$strQuery = "SELECT `books`.`id`, `books`.`serial_number`, `books`.`title`, `books`.`remainder_of_title`, `books`.`parallel_title`, `books`.`original_title`, books`.`part_x`, `books`.`book_number`,";
			//GROUP_CONCAT(authors.id SEPARATOR ';') as author_id,
			//GROUP_CONCAT(authors.name SEPARATOR ';') as author_name,
			//GROUP_CONCAT(languages.name) as language  \n";

			$strQuery .= "\n(
  SELECT group_concat(`languages`.`name`)
  FROM books_languages
  LEFT JOIN `languages` ON `books_languages`.`language_id` = `languages`.`id`
   WHERE `books_languages`.`book_id` = `books`.`id`
) AS language,
(
  SELECT GROUP_CONCAT(`authors`.`id` SEPARATOR ';')
  FROM authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   WHERE `authors_books`.`book_id` = `books`.`id`
)  AS author_id,
(
  SELECT GROUP_CONCAT(`authors`.`name` SEPARATOR ';')
  FROM authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   WHERE `authors_books`.`book_id` = `books`.`id`
)  AS author_name\n";

			$strQuery .= "FROM (".$recursive_from.") books\n";
*/
/*			$strQuery .= "LEFT JOIN `books_languages` ON `books_languages`.`book_id` = `books`.`id`
										LEFT JOIN `languages` ON `books_languages`.`language_id` = `languages`.`id`
										LEFT JOIN `books_clients` ON `books_clients`.`book_id` = `books`.`id` and books_clients.date_return = 0000-00-00
										LEFT JOIN `clients` ON `books_clients`.`client_id` = `clients`.`id`
										LEFT JOIN `authors_books` ON `authors_books`.`book_id` = `books`.`id` and authors_books.role_id = 1
										LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
										WHERE ((".$strWhere.") AND ".$df.")
										GROUP BY `books`.`id`";
*/

/*
 			$strQuery .= "LEFT JOIN `books_clients` ON `books_clients`.`book_id` = `books`.`id` and books_clients.date_return = 0000-00-00
										LEFT JOIN `clients` ON `books_clients`.`client_id` = `clients`.`id`
										WHERE ((".$strWhere.") AND ".$df.")
										GROUP BY `books`.`id`";

*/

		$this->ldb->select(implode(',', $fieldlist));
		$this->ldb->select("
  (select GROUP_CONCAT(authors.id SEPARATOR ';')
  from authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   where `authors_books`.`book_id` = `books`.`id`
)  as author_id,
(
  select GROUP_CONCAT(authors.name SEPARATOR ';')
  from authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   where `authors_books`.`book_id` = `books`.`id`
)  as author_name");
/*		$this->ldb->select("(
  SELECT group_concat(`languages`.`name`)
  FROM books_languages
  LEFT JOIN `languages` ON `books_languages`.`language_id` = `languages`.`id`
   WHERE `books_languages`.`book_id` = `books`.`id`
) AS language,
(
  SELECT GROUP_CONCAT(`authors`.`id` SEPARATOR ';')
  FROM authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   WHERE `authors_books`.`book_id` = `books`.`id`
)  AS author_id,
(
  SELECT GROUP_CONCAT(`authors`.`name` SEPARATOR ';')
  FROM authors_books
  LEFT JOIN `authors` ON `authors`.`id` = `authors_books`.`author_id`
   WHERE `authors_books`.`book_id` = `books`.`id`
)  AS author_name\n", false);
*/

		$this->ldb->from("books");
		//$this->ldb->join("books_clients", "books_clients.book_id = books.id", "left");
		//$this->ldb->where("books.id not in(SELECT DISTINCT id FROM books_clients WHERE date_return >=  \"".$from."\" AND date_return <  \"".$to."\")");
		$this->ldb->where("books.id not in (".implode(",", $exclude).")");
		//$this->ldb->where(array('books.delete_flag' => "0", "books_clients.date_borrow >= " => $from, "books_clients.date_borrow <= " => $to));

		if(!$direction || $direction <= 0)
			$this->ldb->order_by("books.id", "ASC");
		else
			$this->ldb->order_by("books.id", "DESC");

		$this->ldb->group_by("books.id");
		//$this->ldb->limit(100);

		$query = $this->ldb->get();
		$result = $query->result_array();
//print($this->ldb->last_query());
//print("<br/>\n");

	return $result;
	}


	// List of books overdue
	function overdue()
	{
		// Return only fields which are required to be viewn
		foreach($this->fields as $f)
		{
			if(array_key_exists('show_result', $f) && $f['show_result'] == 1)
			{
				if(!array_key_exists('ljtable', $f))
					$fieldlist[] = $this->classname."s.".$f['name'];
			}
		}

		$this->ldb->select(implode(',', $fieldlist));
		$this->ldb->select(array("books_clients.date_borrow", "books_clients.date_due", "books_clients.date_reminder", "clients.id as clientid", "CONCAT(clients.name, ' ', clients.surname, ' (', clients.address, ')'	) as client", "clients.email", "books_clients.id as overdueid", "books_clients.reminder_level"));
		$this->ldb->join("books_clients", "books_clients.book_id = books.id and books_clients.date_return = 0000-00-00", "left");
		$this->ldb->join("clients", "books_clients.client_id = clients.id", "left");
		$this->ldb->group_by("books_clients.book_id");
		$this->ldb->where("books_clients.date_due < current_date() AND books_clients.date_return = 0000-00-00");
		$this->ldb->order_by("books_clients.date_due","ASC");
		$this->ldb->from($this->classname."s");
		//$this->ldb->limit(2);
		$query = $this->ldb->get();
		$result = $query->result_array();
//print($this->ldb->last_query());
//print("<br/>\n");

		// Look up LJTables
		foreach($result as $i => $r)
		{
			// Return only fields which are required to be viewn
			foreach($this->fields as $f)
			{
				if(array_key_exists('ljtable', $f) && array_key_exists('show_result', $f) && $f['show_result'] == 1)
				{
					$this->ldb->select($f['name']."s.name as ".$f['name']);
					$this->ldb->from($f['ljtable']);
					$this->ldb->join($f['name']."s", $f['ljtable'].".".$f['name']."_id = ".$f['name']."s.id", "left");
					$this->ldb->where($f['ljtable'].".book_id", $r['id']);
					$query = $this->ldb->get();
//print($this->ldb->last_query());
//print("<br/>\n");
					$ljr = $query->result_array();
					if(is_array($ljr) && count($ljr) > 0)
						$result[$i][$f['name']] = $ljr;
					else
						$result[$i][$f['name']] = NULL;
				}
			}
		}

	return $result;
	}

	// Gets the overdue details for send email list
	// manual reminders are not part of the list as they don't get automatically send...
	function overdue_additem($item)
	{
		//print_r($item);
		//print("<br/>");
		if(!$item['email'] || !filter_var($item['email'], FILTER_VALIDATE_EMAIL))
		{
			//print("no email");
			return 0;
		}

		if($item['reminder_level'] >= 4)
		{
			//print("manual reminder");
			return 0;
		}

		$iTimeFirstReminder = strtotime($item['date_due']."+3 days", time());
		if(time() > $iTimeFirstReminder  && $item['reminder_level'] == 0)
		{
			//print("first remidner");
			//return array("reminder_level"=>1, "overdueid"=>$item['overdueid']);
			return $item['overdueid'];
		}

		$iTimeSecondReminder = strtotime($item['date_due']."+10 days", time());
		$iTimePreviousReminderSent = strtotime($item['date_reminder']."+10 days", time());
		if(time() > $iTimeSecondReminder && time() > $iTimePreviousReminderSent && $item['reminder_level'] >= 1 && $item['reminder_level'] < 4 )
		{
			//print("second remidner");
			//return array("reminder_level"=>1, "overdue"=>$item['overdueid']);
			return $item['overdueid'];
		}
	}

	// details of one overdue transaction
	function overdue_getdata($transactionid)
	{
		// Get all details of the transaction
		$this->ldb->select(array("books_clients.date_borrow", "books_clients.date_due", "books_clients.date_reminder", "clients.id as clientid", "clients.name", "clients.surname", "clients.email", "books.title", "authors.name as author_name", "books_clients.reminder_level"));
		$this->ldb->from("books_clients");
		$this->ldb->join("books", "books.id = books_clients.book_id", "left");
		$this->ldb->join("clients", "clients.id = books_clients.client_id", "left");
		$this->ldb->join("authors_books", "authors_books.book_id = books.id", "left");
		$this->ldb->join("authors", "authors.id = authors_books.author_id", "left");
		$this->ldb->where("books_clients.id = ".$transactionid);
		$query = $this->ldb->get();
		$transactiondata = $query->result_array();
//print($this->ldb->last_query());
//print("<br/>\n");
//print_r($transactiondata);
		return $transactiondata[0];
	}


	// Load template based on overdue category
	// manual reminders ARE part of the list !
	function overdue_getemailtemplate($item)
	{
		if(!$item['email'] || !filter_var($item['email'], FILTER_VALIDATE_EMAIL))
		{
			return 0;
		}

		// manual reminder
		if($item['reminder_level'] >= 4)
		{
			$query = $this->ldb->get_where('settings_reminders', array('id' => 5));
			$emailtemplatearray = $query->result_array();
			return $emailtemplatearray[0]['content'];
		}
		$iTimeFirstReminder = strtotime($item['date_due']."+3 days", time());
		if(time() > $iTimeFirstReminder  && $item['reminder_level'] == 0)
		{
			$query = $this->ldb->get_where('settings_reminders', array('id' => 1));
			$emailtemplatearray = $query->result_array();
			return $emailtemplatearray[0]['content'];
		}

		$iTimeSecondReminder = strtotime($item['date_due']."+10 days", time());
		if(time() > $iTimeSecondReminder  && $item['reminder_level'] >= 1)
		{
			$query = $this->ldb->get_where('settings_reminders', array('id' => $item['reminder_level']+1));
			$emailtemplatearray = $query->result_array();
			return $emailtemplatearray[0]['content'];
		}
	}

	function overdue_category($date)
	{
		$iLevelOne = strtotime($date."+10 days", time());
		$iLevelTwo = strtotime($date."+20 days", time());
		if(time() > $iLevelTwo)
			return 2;
		if(time() > $iLevelOne)
			return 1;
		return 0;
	}

	function overdue_label($overdueid, $reminder_level, $date_due, $date_reminder)
	{
		if($reminder_level == 0)
		{
		 if(strtotime($date_due."+3 days", time()) > time())
			return "Too early for reminder";
		 else
			return "Sending 1st reminder";
		}
		elseif($reminder_level == 1)
		{
		 if(strtotime($date_reminder." +10 days", time()) > time())
			return "Too early for 2nd reminder";
		 else
			return "Sending 2nd reminder";
		}
		elseif($reminder_level == 2)
		{
		 if(strtotime($date_reminder." +10 days", time()) > time())
			return "Too early for 3rd reminder";
		 else
			return "Sending 3rd reminder";
		}
		elseif($reminder_level == 3)
		{
		 if(strtotime($date_reminder." +10 days", time()) > time())
			return "Too early for 4th reminder";
		 else
			return "Sending 4th reminder";
		}
		elseif($reminder_level >= 4)
			return "<div id =\"".$overdueid."\"><a href=\"javascript:ajaxreminder(".$overdueid.");\" id=\"a_".$overdueid."\" class=\"reminderlink\">Manual reminder</a> <img src=\"/library/assets/img/spacer.gif\" id=\"img_".$overdueid."\" width=\"16\" height=\"16\"/></div>";

		else
			return "Unknown reminder error";
	}

	function send_reminder_email($to, $subject, $message)
	{
		$strFrom = "avlib@auroville.org.in";
		$strFromFulltext = "Auroville Library";

		// send an email
		$this->load->library('email');
		$this->email->from($strFrom, $strFromFulltext);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		if ( ! $this->email->send())
		{
			//die($this->email->print_debugger());
			return 0;
		}
		else
		{
			return 1;
		}
	}

	function overdue_updatereminderdate($transactionid)
	{
//		$this->ldb->update('books_clients', array("date_reminder" => "CURDATE()"), array('id' => $transactionid));
		$this->ldb->query("UPDATE `books_clients` SET `date_reminder` = CURDATE() WHERE `id` =  '".$transactionid."'");
		$this->ldb->query("UPDATE `books_clients` SET `reminder_level` = `reminder_level` + '1' WHERE `id` =  '".$transactionid."'");
//print($this->ldb->last_query());
//print("<br/>\n");
	}

	function get_labels_view()
	{
		$r = array();
		foreach($this->fields as $f)
			if(array_key_exists('show_result', $f) && $f['show_result'] == 1)
				$r[$f['name']] = $f['label'];
		return $r;
	}

	function get_book_borrowhistory($id=null)
	{
		if($id)
		{
			$this->ldb->select(array("books_clients.date_borrow", "books_clients.date_return", "clients.id as `clientid`", "CONCAT(`clients`.`name`, ' ', `clients`.`surname`, ' (', `clients`.`address`, ')'	) as `client`"));
			$this->ldb->join("clients", "books_clients.client_id = clients.id", "left");
			$this->ldb->from("books_clients");
			$this->ldb->where("books_clients.book_id = ".$id);
			$this->ldb->order_by("books_clients.date_borrow", "DESC");
			$query = $this->ldb->get();
			return $query->result_array();
		}
	}

	function get_checkoutcount_year()
	{
		$this->ldb->select(array("year(date_borrow) as Y", "count(id) as books_borrowed"));
		$this->ldb->from("books_clients");
		$this->ldb->group_by("year(date_borrow)");
		$this->ldb->order_by("Y", "DESC");
		$query = $this->ldb->get();
//print($this->ldb->last_query());
//print("<br/>\n");
		return $query->result_array();
	}

	function get_checkoutcount_month()
	{
		$this->ldb->select(array("year(date_borrow) as Y", "month(date_borrow) as M", "count(id) as books_borrowed"));
		$this->ldb->from("books_clients");
		$this->ldb->group_by("CONCAT(year(date_borrow),month(date_borrow))");
		$this->ldb->order_by("Y", "DESC");
		$this->ldb->order_by("M", "DESC");
		$query = $this->ldb->get();
//print($this->ldb->last_query());
//print("<br/>\n");
		return $query->result_array();
	}

	function get_checkoutcount_week()
	{
		$this->ldb->select(array("year(date_borrow) as Y", "week(date_borrow) as W", "count(id) as books_borrowed"));
		$this->ldb->from("books_clients");
		$this->ldb->group_by("yearweek(date_borrow)");
		$this->ldb->order_by("Y", "DESC");
		$this->ldb->order_by("W", "DESC");
		$query = $this->ldb->get();
//print($this->ldb->last_query());
//print("<br/>\n");
		return $query->result_array();
	}

	function get_checkoutcount_day()
	{
		$this->ldb->select(array("date_borrow", "count(id) as books_borrowed"));
		$this->ldb->from("books_clients");
		$this->ldb->group_by("date_borrow");
		$this->ldb->order_by("date_borrow", "DESC");
		$query = $this->ldb->get();
//print($this->ldb->last_query());
//print("<br/>\n");
		return $query->result_array();
	}

	function ajaxgeneratebooknumber($bookid)
	{
		$this->ldb->select(array("books.dewey_decimal_classification", "UCASE(SUBSTRING(`authors`.`name`,1,4)) as name", "SUBSTRING(books.title,1,1) as title", "books.part_x", "books.serial_number"));
		$this->ldb->join("authors_books", "authors_books.book_id = books.id AND authors_books.role_id = 1", "left");
		$this->ldb->join("authors", "authors.id = authors_books.author_id", "left");
		$this->ldb->where("books.id = ".$bookid);
		$this->ldb->from($this->classname."s");
		$this->ldb->limit(1);
		$query = $this->ldb->get();
		$r = $query->result_array();
//print($this->ldb->last_query());
//print_r($r);
//print("<br/>\n");
//die();
		return $r;

	}

	function ajaxlookupload($name, $bookid)
	{
		$this->ldb->select(array("books_".$name."s.id", $name."s.name", $name."s.notes", "books_".$name."s.".$name."_id"));
		$this->ldb->where(array("book_id" => $bookid));
		$this->ldb->from("books_".$name."s");
		$this->ldb->join($name."s", $name."s.id = books_".$name."s.".$name."_id", "left");
		//$this->ldb->limit(10);
		$query = $this->ldb->get();
		$r = $query->result_array();

		$data['labels'] = array($name => $this->fields[$name]['label'], "notes" => "Notes", "action"=> "");
		$data['items'] = $r;

		foreach($data['items'] as $i => $v)
		{
			$data['items'][$i][$name] = $v['name'];
			$data['items'][$i]['action'] = "<a href=\"javascript:deleteLookup('".$name."', '".$bookid."', '".$v['id']."');\">".img("assets/img/cross.png")."</a>";
		}
		return $data;
	}

	function ajaxlookupadd($name, $bookid, $value)
	{
		// not int ; create new entry
		if(!ctype_digit($value))
		{
			$this->ldb->insert($name."s", array("name" => $value));
			$value = $this->ldb->insert_id();
		}
		$this->ldb->insert("books_".$name."s", array("book_id" => $bookid, $name."_id" => $value));
		return true;
	}

	function ajaxlookupdelete($name, $lookupid)
	{
		$this->ldb->delete("books_".$name."s", array("id" => $lookupid));
		return true;
	}

	function ajaxlookupcomplete($name, $term)
	{
		$this->ldb->select(array("id", "name"));
		$this->ldb->where("(`id` like '".$term."%' OR `name` like '".$term."%')");
		$this->ldb->order_by("name", "asc");
		$this->ldb->from($name."s");
		$this->ldb->limit(200);
		$query = $this->ldb->get();
//print($this->ldb->last_query());
//print("<br/>\n");
		$r = $query->result_array();
		if(is_array($r) && count($r) > 0)
		{
			foreach($r as $res)
			{
				$ret[] = array('label' => $res['name']." (".$res['id'].")", 'value' => $res['id']);
			}
			return $ret;
		}
		else
			return null;
	}

	function ajaxsearch($needle, $haystack)
	{
		$this->ldb->distinct();
		$this->ldb->select($haystack);
		$this->ldb->where("(`".$haystack."` like '".$needle."%')");
		$this->ldb->order_by($haystack, "asc");
		$this->ldb->from($this->classname."s");
		$this->ldb->limit(20);
		$query = $this->ldb->get();
		$r = $query->result_array();

		if(is_array($r) && count($r) > 0)
		{
			foreach($r as $res)
			{
				$ret[] = array('label' => $res[$haystack], 'value' => $res[$haystack]);
			}
			return $ret;
		}
		else
			return null;
	}
}