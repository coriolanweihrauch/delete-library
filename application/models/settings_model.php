<?php
class Settings_model extends CI_Model {

	//public $lists = array("feature" => "Features", "group" => "Client Groups", "keyword" => "Keywords", "language" => "Languages", "physicaldetail" => "Binding", "role" => "Roles", "topicalterm" => "Topical terms");
	public $lists = array("group" => "Client Groups", "role" => "Author Roles");
	public $mergelists = array("author" => "Authors", "feature" => "Features", "group" => "Client Groups", "keyword" => "Keywords", "language" => "Languages", "physicaldetail" => "Binding", "role" => "Roles", "topicalterm" => "Topical terms");
	private $classname = 'settings';
	public $ldb;

	public function __construct()
	{
		$this->load->database();
		$this->load->helper('avlib');
		$this->load->helper('html');
		
		$this->ldb = $this->load->database(get_current_database(), TRUE);
		//$mergelists = $this->lists;
		//$mergelists['author'] = "Authors";
	}

	public function save_list($listname, $post)
	{
		if(array_key_exists("name", $post) && $post['name'] != "")
		{
			foreach($post['name'] as $i => $p)
			{
				$this->ldb->update($listname."s", array("name" => $p), array('id' => $i));
//print($this->ldb->last_query()."<br/>\n");
			}
		}
		if(array_key_exists("new", $post) && $post['new'] != "")
		{
			$this->ldb->insert($listname."s", array("name" => $post['new']));
//print($this->ldb->last_query()."<br/>\n");
		}
		return 1;
	}

	public function load_list($listname)
	{
		$this->ldb->order_by("name", "asc");
		$query = $this->ldb->get($listname."s");
		$r = $query->result_array();
//print($this->ldb->last_query()."<br/>\n");
//print_r($r);
		return $r;
	}

	public function delete_list_item($listname, $id)
	{
		$query = $this->ldb->delete($listname."s", array("id" => $id));
//print($this->ldb->last_query()."<br/>\n");
		if($listname == "role")
		{
			$query = $this->ldb->delete("authors_books", array($listname."_id" => $id));
		}
		else
			$query = $this->ldb->delete("books_".$listname."s", array($listname."_id" => $id));
//print($this->ldb->last_query()."<br/>\n");
	}

	public function save_blacklist($post)
	{
		if(array_key_exists("name", $post) && $post['name'] != "")
		{
			foreach($post['name'] as $i => $p)
			{
				$this->ldb->update("settings_blacklist", array("name" => $p), array('id' => $i));
//print($this->ldb->last_query()."<br/>\n");
			}
		}
		if(array_key_exists("new", $post) && $post['new'] != "")
		{
			$this->ldb->insert("settings_blacklist", array("name" => $post['new']));
//print($this->ldb->last_query()."<br/>\n");
		}
		return 1;
	}

	public function load_blacklist()
	{
		$this->ldb->order_by("name", "asc");
		$query = $this->ldb->get("settings_blacklist");
		$r = $query->result_array();
		return $r;
	}

	public function delete_blacklist_item($id)
	{
		$query = $this->ldb->delete("settings_blacklist", array("id" => $id));
	}

	public function merge($listname, $post)
	{
 		if(!array_key_exists("right", $post) || !array_key_exists("left", $post) || !isset($post['left']) || !isset($post['right']))
			return 0;

		if($listname == "author")
			$strTable = $listname."s_books";
		else
			$strTable = "books_".$listname."s";
		$this->ldb->update($strTable, array($listname."_id"=>$post['left']), array($listname."_id"=>$post['right']));

		$this->ldb->delete($listname."s", array("id" => $post['right']));

		return 1;
	}

	public function save_reminders($post)
	{
		foreach($post['name'] as $i => $p)
		{
			$this->ldb->update("settings_reminders", array("content" => $p), array('id' => $i));
//print($this->ldb->last_query()."<br/>\n");
		}
		return 1;
	}

	public function load_reminders()
	{
		$query = $this->ldb->get("settings_reminders");
		$r = $query->result_array();
//print($this->ldb->last_query()."<br/>\n");
//print_r($r);
		return $r;
	}

	public function printlabels($postdata)
	{
		//print_r($postdata);
		$this->ldb->select(array("books.serial_number", "books.dewey_decimal_classification", "books.book_number"));
		$this->ldb->from("books");
		
		switch($postdata['type'])
		{
			case "date":
				$this->ldb->where("books.date_created >=", $postdata['date_from']);
				$this->ldb->where("books.date_created <=", $postdata['date_to']);
				$this->ldb->order_by("serial_number", "ASC");
				$query = $this->ldb->get();
		$result = $query->result_array();
			break;
			case "number":
				$this->ldb->where("books.serial_number >=", $postdata['number_from']);
				$this->ldb->where("books.serial_number <=", $postdata['number_to']);
				$this->ldb->order_by("serial_number", "ASC");
				$query = $this->ldb->get();
				$result = $query->result_array();
			break;
			case "list":
				$query = $this->ldb->query("SELECT `books`.`serial_number`, `books`.`dewey_decimal_classification`, `books`.`book_number` FROM (`books`) WHERE `books`.`serial_number` IN ('".implode("', '", explode("\n", $postdata['list_from']))."') ORDER BY FIELD(`books`.`serial_number`, '".implode("', '", explode("\n", $postdata['list_from']))."')");
				$result = $query->result_array();

			break;
			default:
				$this->ldb->limit(100);
			break;
		}
		
		

//print($this->ldb->last_query());
//print("<br/><br/>\n");
//print_r($result);

		return $result;
	}

	public function exportlabels($data)
	{
		$books = $data['elements']['items'];
		// 1 = Spine (full info)
		// 2 = Barcode only (back of book)
		$format = $data['elements']['fields']['format']['value'];
		
//print('<pre>');
//print_r($books);
//print('</pre>');
//die();
//foreach($books as $i => $book)
//	print($book['dewey_decimal_classification']."\n".$book['book_number']."\n".$book['serial_number']."\n-\n");
//die();

		// Parameters
		//$book = array("dewey_decimal_classification" => "150.195", "book_number" => "150.195 ABRA 13", "serial_number" => "31262");
		$iMaxCol = 5;
		$arrAlphabet = array( 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' );
		// Excel metadata
		$this->load->library('excel');
		$this->excel->getActiveSheet()->setTitle('labels');
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getDefaultStyle()->getFont()->setName('Arial');
		$this->excel->getDefaultStyle()->getFont()->setSize(11);

		for($i=0; $i<$iMaxCol; $i++)
		{
			$this->excel->getActiveSheet()->getColumnDimension($arrAlphabet[$i])->setWidth(15.5);
		}

		// Actual excel
		// Spine label
		if($format == 1)
		{
			$this->excel->getDefaultStyle()->getFont()->setBold(true);
			$iRowIndex = 0;
			foreach($books as $i => $book)
			{
				if($i % $iMaxCol == 0)
					$iRowIndex++;
				$this->excel->getActiveSheet()->getRowDimension($iRowIndex+1)->setRowHeight(55);
				for($j = strlen($book['serial_number']); $j < 6; $j++)
					$book['serial_number'] = "0".$book['serial_number'];
				$this->excel->getActiveSheet()->setCellValue($arrAlphabet[$i%$iMaxCol].($iRowIndex), trim($book['dewey_decimal_classification']."\n".$book['book_number']."\n".$book['serial_number']));
				$this->excel->getActiveSheet()->getStyle($arrAlphabet[$i%$iMaxCol].($iRowIndex))->getAlignment()->setWrapText(true);
			}
		}
		// Spine label
		elseif($format == 2)
		{
			$iRowIndex = 0;
			foreach($books as $i => $book)
			{
				if($i % $iMaxCol == 0)
					$iRowIndex++;
				$this->excel->getActiveSheet()->getRowDimension($iRowIndex+1)->setRowHeight(55);
				for($j = strlen($book['serial_number']); $j < 6; $j++)
					$book['serial_number'] = "0".$book['serial_number'];
				$this->excel->getActiveSheet()->setCellValue($arrAlphabet[$i%$iMaxCol].($iRowIndex), "*".$book['serial_number']."*");
				$this->excel->getActiveSheet()->getStyle($arrAlphabet[$i%$iMaxCol].($iRowIndex))->getFont()->setName('IDAutomationHC39M');
			}
		}
		return PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
	}
}
?>