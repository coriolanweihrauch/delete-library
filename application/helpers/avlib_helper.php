<?php
function is_client()
{
	$CI =& get_instance();
	return !$CI->session->userdata('loggedin');
}

function is_staff()
{
	$CI =& get_instance();
	return $CI->session->userdata('loggedin') && $CI->session->userdata('credentials') == "staff";
}

function is_admin()
{
	$CI =& get_instance();
	return $CI->session->userdata('loggedin') && $CI->session->userdata('credentials') == "admin";
}

function login($redirect = null)
{
	$data['title'] = "Please authenticate";
	$data['selectedMenu'] = 'Books';
	if($redirect)
		$data['redirect'] = $redirect;
	// Focus on password field
	$data['focusfield'] = "password";
	
	$CI =& get_instance();
	
	$CI->load->view('templates/avlibheader', $data);
	//$this->load->view('books/menubar', $data);
	$CI->load->view('templates/loginform', $data);
	$CI->load->view('templates/avlibfooter', $data);
}

function get_current_database()
{
	$CI =& get_instance();
	$dbset = $CI->config->item('avlib_database_set');
	$dbdefault = $CI->config->item('avlib_database_default');
	$CI->load->library('session');
	$dbsession = $CI->session->userdata('avlib_database');
	if(isset($dbset) && is_array($dbset) && count($dbset) > 1 && array_key_exists($dbdefault, $dbset)){
		if(isset($dbsession) && $dbsession != "")
			$dbreturn = $dbsession;
		else
			$dbreturn = $dbdefault;
	}
	else{
		$dbreturn = "default";
	}
	return $dbreturn;
}

function generate_form_field($f)
{
	// Remove custom tags from field //
	if(array_key_exists('script', $f))
		unset($f['script']);
	if(array_key_exists('show_edit', $f))
		unset($f['show_edit']);
	if(array_key_exists('show_search', $f))
		unset($f['show_search']);
	if(array_key_exists('show_view', $f))
		unset($f['show_view']);

  if(!array_key_exists('ljtable', $f))
  {
		switch($f['type'])
		{
			case "textarea":
				print(form_textarea($f));
			break;

			case "checkbox":
				if($f['value'] == 1)
				{
					$f['checked'] = '1';
				}
				// Set 1 on checkbox value so submitted value is one ... horrble, I know. Blame HTML forms
				$f['value'] = 1;
				print(form_input($f)."\n");
				if(array_key_exists('label', $f) && $f['label'] != "")
					print(form_label($f['label'], $f['name']));
			break;

			case "hidden":
				print(form_input($f));
			break;

			case "text":
			default:
				print(form_input($f));
			break;
		}

  }
  else{
    switch($f['type'])
    {
			case "lookup":
				form_lookup($f);
			break;
      case "multiselect":
        print(form_multiselect($f['name']."[]", $f['options'], $f['value'], "id=\"".$f['id']."\""));
        break;
      case "select":
      default:
        print(form_dropdown($f['name'], $f['options'], $f['value'], "id=\"".$f['id']."\""));
      break;

    }
  }
}

function form_lookup($f)
{
	if($f['name'] != "author")
	{
		generate_form_table_no_submit($f['subfields']);
		show_values_table($f);
	}
	else
	{
		//print('<div class="author-quickadd">');
		//print('	<h3>Authors & other people</h3>');
		generate_form_table_no_submit($f['quickaddform']['fields']);
		print("<table id=\"lookupval_author\" class=\"tableview\"></table>");
		print("<script>loadPersons(".$f['quickaddform']['bookid'].");</script>");
		//print('</div>');
	}
}

function show_values_table($f)
{
	print("<table id=\"lookupval_".$f['name']."\" class=\"tableview\"></table>");
	print("<script>loadLookup(\"".$f['name']."\",$('#bookid').val());</script>");
	print("<script>$(\"#".$f['id']."\").autocomplete({
			source: function(request, response) {
				$.ajax({ url: \"/library/books/ajaxlookupcomplete/\",
					data: { name: \"".$f['name']."\", term: $(\"#".$f['id']."\").val()},
					dataType: \"json\",
					type: \"POST\",
					success: function(data){
							response(data);
					}
				});
			},
			minLength: 1,
		});</script>");
}

function generate_form_field_br($f)
{
	if($f['label'] != "" && $f['type'] != 'checkbox' && $f['type'] != 'hidden')
	{
		generate_form_field_label($f);
		print("<br/>\n");
	}
	generate_form_field($f);
	if($f['type'] != 'hidden')
		print("<br/>\n");
}

function generate_form_field_hz($f)
{
	if($f['label'] != "" && $f['type'] != 'checkbox' && $f['type'] != 'hidden')
	{
		generate_form_field_label($f);
	}
	print(" ");
	generate_form_field($f);
	if($f['type'] != 'hidden')
		print("<br/>\n");
}

function generate_form_field_label($f)
{
	if($f['type'] != "hidden")
		print(form_label($f['label'], $f['id']));
}

function generate_form_table($fields, $submitoptions=FALSE, $resetoptions = FALSE)
{
	if(isset($fields) && is_array($fields) && count($fields) > 0)
	{
		if($submitoptions === FALSE)
		{
			$submitoptions['label'] = "Save";
			$submitoptions['name'] = "submit";
		}
		print("<table class=\"formtable\">\n");
		print("  <tr>\n");
		foreach($fields as $f)
		{
			print("    <td>");
			generate_form_field_label($f);
			print("</td>\n");
		}
		// Spacer for submit button
		print("    <td/>");
		// Spacer for reset button
		if($resetoptions !== FALSE)
			print("    <td/>");
		print("  </tr>\n");
		print("  <tr>\n");
		foreach($fields as $f)
		{
			print("    <td>");
			generate_form_field($f);
			print("</td>\n");
		}
		print("    <td>");
		print(form_submit($submitoptions['name'], $submitoptions['label']));
		print("</td>\n");
		if($resetoptions !== FALSE)
		{
			print("    <td>");
			print(form_reset($resetoptions['name'], $resetoptions['label'], "onClick=\"javascript:resetForm('".$resetoptions['form']."')\""));
			print("</td>\n");
		}
		print("  </tr>\n");
		print("</table>\n");
	}
	foreach ($fields as $f)
		if(array_key_exists('script', $f))
			print("<script>\n".$f['script']."</script>\n");
}

function generate_form_table_horizontal($fields, $submitoptions=FALSE)
{
	generate_form_table($fields, $submitoptions=FALSE);
}

function generate_form_table_vertical($fields, $submitoptions=FALSE)
{
	if(isset($fields) && is_array($fields) && count($fields) > 0)
	{
		if($submitoptions === FALSE)
		{
			$submitoptions['label'] = "Save";
			$submitoptions['name'] = "submit";
		}
		print("<table class=\"formtable\">\n");
		foreach($fields as $f)
		{
			print("  <tr>\n");
			print("    <td class=\"tablelable\">");
			generate_form_field_label($f);
			print("</td>\n");
			print("    <td class=\"tablecell\">");
			generate_form_field($f);
			print("</td>\n");
			print("  </tr>\n");
		}
		print("</table>\n");
		print("<br/>");
		print(form_submit($submitoptions['name'], $submitoptions['label']));
	}
	foreach ($fields as $f)
		if(array_key_exists('script', $f))
			print("<script>\n".$f['script']."</script>\n");
}

// Generate a table for the lookup (suggestion popup) menu
function generate_form_table_no_submit($fields)
{

//print_r($fields);

	if(isset($fields) && is_array($fields) && count($fields) > 0)
	{
		print("<table class=\"formtable\">\n");
		print("  <tr>\n");
		foreach($fields as $f)
		{
			print("    <td>");
			generate_form_field_label($f);
			print("</td>\n");
		}
		print("  </tr>\n");
		print("  <tr>\n");
		foreach($fields as $f)
		{
			print("    <td>");
			generate_form_field($f);
			print("</td>\n");
		}
		print("  </tr>\n");
		print("</table>\n");
	}
	foreach ($fields as $f)
		if(array_key_exists('script', $f))
			print("<script>\n".$f['script']."</script>\n");
}

function load_ljtables($fields)
{
  foreach($fields as $i => $f)
    if(array_key_exists('ljtable', $f))
    {
      $ci=& get_instance();
      $ci->load->database();

      $q = $ci->db->get($f['name']."s");
      foreach($q->result_array() as $r)
        $fields[$i]['options'][$r['id']] = $r['name'];
      //$this->fields[$i]['values'] = $q->result_array();
//print_r($this->fields[$i]['options']);
    } 
  return $fields;
//print("hello");
}

function sort_labels($labels)
{
//print("Resorting<br/>Original:<br/>\n<pre>");
//print_r($labels);
//print("</pre><br/>\n");
  $t = array("title"=>"", "author_name"=>"", "language"=>"", "book_number"=>"");
	foreach($t as $i => $v)
	{
		if(array_key_exists($i, $labels))
		{
			$t[$i] = $labels[$i];
			unset($labels[$i]);
		}
		else
			unset($t[$i]);
	}
	foreach($labels as $i => $v)
		$t[$i] = $v;
//print("Modified:<br/>\n<pre>");
//print_r($t);
//print("</pre><br/>\n");
	return $t;
}
?>