#AVLib Installation Guide
You can run this software on any computer running PHP 5.2, MySQL 5 and GIT. This guide shows how to install the software under Debian / Ubuntu linux. The installation usually takes about one hour.

### Requirements: ###
* Debian Linux 6 or Ubuntu 12.04
* Root password
* Running internet connection

### Nano Commands ###
We'll be using the Nano text editor. Some useful commands:

* ctrl-w to search a string
* ctrl-o to save the file
* ctrl-x  to exit

## Setting up the server ##

### Installaing prerequisites ###
We start out with installing a few necessary programs.

Open a Terminal window and enter the following commands:

```
sudo apt-get install aptitude
sudo aptitude update 
sudo aptitude safe-upgrade 
```

In *Ubutu* (not Debian) run:

```
sudo aptitude install git-core git nano apache2 php5 phpmyadmin mysql-server php5-mysql
```

In *Debian* (not Ubuntu) run:
```
sudo aptitude install git nano apache2 php5 phpmyadmin mysql-server php5-mysql
```

* When asked, chose (and remember) a new MySQL Password - we will refer to this as [mysqlpassword]
* Chose 'Apache' as webserver for MySQL
* configure PHPMyAdmin with dbconfig.common

### Configuring Apache
Run this:
```
sudo a2enmod rewrite
```

Then this:
```
sudo nano /etc/apache2/apache2.conf
```

This will open Nano.
Add the following line:
```
Include /etc/phpmyadmin/apache.conf
```
Save and exit

Under Ubuntu 12.x and Debian run:
```
sudo nano /etc/apache2/sites-enabled/000-default

```
Under Ubuntu 13.10 and higher run this instead:
```
sudo nano /etc/apache2/sites-enabled/000-default.config
```

Add the following lines:
```
<Directory /var/www>
	AllowOverride All
	allow from all
</Directory>
```
Save and exit

### Configuring PHP

Run
```
sudo nano /etc/php5/apache2/php.ini
```

Search for 'memory_limit' and set it:
```
memory_limit = '256M'
```

Search for 'error_reporting' and set it: 
```
error_reporting = 'E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_NOTICE'
```

Search for ‘date.timezone’ and set it to your timezone. For india, use: 
```
date.timezone = "Asia/Kolkata"
```
Save and exit

**NOTE**: Some (older) Apache versions require 
```
date.timezone = "Asia/Calcutta"
```
If the AVLib software later reports an error message such as ```"Message: date(): …"``` come back to this point.

Restart apache:
```
sudo apachectl graceful
```

### Configuring MySQL
Next, we will create the mysql database and access credentials to it. Chose a new password. We will refer to this as [phppassword]. Run:

```
sudo mysql -u root -p -e "CREATE DATABASE avlib; GRANT ALL PRIVILEGES ON avlib.* to avlib@localhost identified by '[phppassword]'; flush privileges;" 
```

You will be asked for your [mysqlpassword] (as defined above)


### Installing the AVLib program
Create and go to the installation folder
```
sudo mkdir /var/www/library
cd /var/www/library
```

Download the program
```
sudo git init
sudo git pull ssh://git@bitbucket.org:coriolanweihrauch/asynctoapi.git 
```

Install the basic dataset:
```
sudo mysql -u root -p avlib < /var/www/library/database/avlib_blank.sql
```
You will need to enter your [mysqlpassword]

Configure general settings:
```
cd /var/www/library/application/config/
sudo cp config.php.sample.php config.php
sudo nano config.php
```

Enter the following details
* your institution's name
* set an encrytpion key - enter random characters (no ' symbol)
Save and exit

Configure database settings:
```
cd /var/www/library/application/config/
sudo cp database.config.php.sample.php database.php
sudo nano database.php
```

Set the following details:
```
username = 'avlib'
password = '[phppassword]'
database = 'avlib'
dbdriver = 'mysqli'
```
[phppassword] as defined above.

In case of migration from NewGenLib, set username & password in the corresponding section.
Save and exit.

Configure email settings:
```
cd /var/www/library/application/config/
sudo cp email.config.php.sample.php email.php
sudo nano email.php
```
Set the details as applicable, save and exit.

The installation is no completed and we are ready to use AVLIB.
Open a browser to ```http://localhost/library```
The Standard Admin password is 123.
It can be changed under Settings > Password