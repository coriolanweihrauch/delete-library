git rm --cached ./application/config/config.php
git rm --cached ./application/config/email.php
git rm --cached ./application/config/database.php
git rm --cached ./application/views/help/help.php
git rm --cached ./application/views/help/help-staff.php
git add .
git commit -am "Removed settings files"